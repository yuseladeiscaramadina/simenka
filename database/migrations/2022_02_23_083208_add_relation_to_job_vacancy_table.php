<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationToJobVacancyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_vacancy', function (Blueprint $table) {
            $table->foreign('hrd_id')->references('id')->on('hrd')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_vacancy', function (Blueprint $table) {
            $table->dropForeign('job_vacancy_hrd_id_foreign');
        });
    }
}
