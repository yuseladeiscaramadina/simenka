<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationToApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application', function (Blueprint $table) {
            $table->foreign('applicant_id')->references('id')->on('applicant')->onDelete('cascade');
            $table->foreign('job_vacancy_id')->references('id')->on('job_vacancy')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application', function (Blueprint $table) {
            $table->dropForeign('application_applicant_id_foreign');
            $table->dropForeign('application_job_vacancy_id_foreign');
        });
    }
}
