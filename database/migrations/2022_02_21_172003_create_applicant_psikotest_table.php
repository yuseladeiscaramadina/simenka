<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantPsikotestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_psikotest', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('application_id');
            $table->unsignedBigInteger('psikotest_id');
            $table->integer('score')->nullable();
            $table->enum('status', ['not_started', 'started', 'done', 'fail', 'late']);
            $table->integer('progress_question')->nullable();
            $table->enum('progress_pk', ['p', 'k']);
            $table->integer('p_d')->nullable();
            $table->integer('p_i')->nullable();
            $table->integer('p_s')->nullable();
            $table->integer('p_c')->nullable();
            $table->integer('p_star')->nullable();
            $table->integer('k_d')->nullable();
            $table->integer('k_i')->nullable();
            $table->integer('k_s')->nullable();
            $table->integer('k_c')->nullable();
            $table->integer('k_star')->nullable();
            $table->integer('pk_d')->nullable();
            $table->integer('pk_i')->nullable();
            $table->integer('pk_s')->nullable();
            $table->integer('pk_c')->nullable();
            $table->integer('result_p')->nullable();
            $table->integer('result_k')->nullable();
            $table->integer('result_pk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_psikotest');
    }
}
