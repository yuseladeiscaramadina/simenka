<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationToApplicantTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applicant_test', function (Blueprint $table) {
            $table->foreign('application_id')->references('id')->on('application')->onDelete('cascade');
            $table->foreign('test_id')->references('id')->on('test')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applicant_test', function (Blueprint $table) {
            $table->dropForeign('applicant_test_application_id_foreign');
            $table->dropForeign('applicant_test_test_id_foreign');
        });
    }
}
