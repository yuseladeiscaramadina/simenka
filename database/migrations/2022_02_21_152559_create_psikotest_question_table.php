<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsikotestQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psikotest_question', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('psikotest_id');
            $table->string('question');
            $table->string('option_a');
            $table->string('option_type_a_me');
            $table->string('option_type_a_not_me');
            $table->string('option_b');
            $table->string('option_type_b_me');
            $table->string('option_type_b_not_me');
            $table->string('option_c');
            $table->string('option_type_c_me');
            $table->string('option_type_c_not_me');
            $table->string('option_d');
            $table->string('option_type_d_me');
            $table->string('option_type_d_not_me');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psikotest_question');
    }
}
