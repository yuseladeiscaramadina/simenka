<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationToApplicantPsikotestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applicant_psikotest', function (Blueprint $table) {
            $table->foreign('application_id')->references('id')->on('application')->onDelete('cascade');
            $table->foreign('psikotest_id')->references('id')->on('psikotest')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applicant_psikotest', function (Blueprint $table) {
            $table->dropForeign('applicant_psikotest_application_id_foreign');
            $table->dropForeign('applicant_psikotest_psikotest_id_foreign');
        });
    }
}
