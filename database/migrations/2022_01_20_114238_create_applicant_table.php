<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('id_card_number')->nullable();
            $table->string('family_card_number')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('ktp_address')->nullable();
            $table->text('current_address')->nullable();
            $table->string('religion')->nullable();
            $table->integer('age')->nullable();
            $table->enum('gender', ['pria', 'wanita'])->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('last_education')->nullable();
            $table->string('major')->nullable();
            $table->string('last_place_education')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('bank')->nullable();
            $table->string('token');
            $table->text('profile_picture')->nullable();
            $table->text('doc_id_card')->nullable();
            $table->text('doc_family_card')->nullable();
            $table->text('doc_cv')->nullable();
            $table->text('doc_certificate_of_education')->nullable();
            $table->text('doc_transcript')->nullable();
            $table->text('doc_taxpayer')->nullable();
            $table->text('doc_bpjs')->nullable();
            $table->date('date_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant');
    }
}
