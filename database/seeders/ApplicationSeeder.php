<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('application')->insert([
            'applicant_id' => '1',
            'job_vacancy_id' => '1',
            'status' => 'pendaftaran',
            'deadline' => Carbon::now()
        ]);
    }
}
