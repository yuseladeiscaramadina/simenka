<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PsikotestPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('psikotest_p')->insert([
            'key' => '0',
            'd' => '-6.00',
            'i' => '-7.00',
            's' => '-5.70',
            'c' => '-6.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '1',
            'd' => '-5.30',
            'i' => '-4.60',
            's' => '-4.30',
            'c' => '-4.70'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '2',
            'd' => '-4.00',
            'i' => '-2.50',
            's' => '-3.50',
            'c' => '-3.50'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '3',
            'd' => '-2.50',
            'i' => '-1.30',
            's' => '-1.50',
            'c' => '-1.50'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '4',
            'd' => '-1.70',
            'i' => '-1.00',
            's' => '-0.70',
            'c' => '-0.50'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '5',
            'd' => '-1.30',
            'i' => '-3.00',
            's' => '-0.50',
            'c' => '-2.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '6',
            'd' => '0.00',
            'i' => '3.50',
            's' => '1.00',
            'c' => '3.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '7',
            'd' => '0.50',
            'i' => '5.30',
            's' => '2.50',
            'c' => '5.30'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '8',
            'd' => '1.00',
            'i' => '5.70',
            's' => '3.00',
            'c' => '5.70'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '9',
            'd' => '2.00',
            'i' => '6.00',
            's' => '4.00',
            'c' => '6.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '10',
            'd' => '3.00',
            'i' => '6.50',
            's' => '4.60',
            'c' => '6.30'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '11',
            'd' => '3.50',
            'i' => '7.00',
            's' => '5.00',
            'c' => '6.50'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '12',
            'd' => '4.00',
            'i' => '7.00',
            's' => '5.70',
            'c' => '6.70'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '13',
            'd' => '4.70',
            'i' => '7.00',
            's' => '6.00',
            'c' => '7.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '14',
            'd' => '5.30',
            'i' => '7.00',
            's' => '6.50',
            'c' => '7.30'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '15',
            'd' => '6.50',
            'i' => '7.00',
            's' => '6.50',
            'c' => '7.30'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '16',
            'd' => '7.00',
            'i' => '7.50',
            's' => '7.00',
            'c' => '7.30'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '17',
            'd' => '7.00',
            'i' => '7.50',
            's' => '7.00',
            'c' => '7.50'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '18',
            'd' => '7.00',
            'i' => '7.50',
            's' => '7.00',
            'c' => '8.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '19',
            'd' => '7.50',
            'i' => '7.50',
            's' => '7.50',
            'c' => '8.00'
        ]);

        DB::table('psikotest_p')->insert([
            'key' => '20',
            'd' => '7.50',
            'i' => '8.00',
            's' => '7.50',
            'c' => '8.00'
        ]);
    }
}
