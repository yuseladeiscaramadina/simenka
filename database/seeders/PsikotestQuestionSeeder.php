<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PsikotestQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Gampangan, Mudah Setuju',
            'option_b' => 'Percaya, Mudah percaya pada orang',
            'option_c' => 'Petualang, Mengambil resiko',
            'option_d' => 'Toleran, Menghormati',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => 'I',
            'option_type_c_me' => '*',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => 'D',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Lembut suara, Pendiam',
            'option_b' => 'Optimistik, Visioner',
            'option_c' => 'Pusat Perhatian, Suka gaul',
            'option_d' => 'Pendamai, Membawa Harmoni',
            // D,I,S,C,*
            'option_type_a_me' => 'C',
            'option_type_b_me' => 'D',
            'option_type_c_me' => '*',
            'option_type_d_me' => 'S',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'S',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Menyemangati orang',
            'option_b' => 'Berusaha sempurna',
            'option_c' => 'Bagian dari kelompok',
            'option_d' => 'Ingin membuat tujuan',
            // D,I,S,C,*
            'option_type_a_me' => 'I',
            'option_type_b_me' => '*',
            'option_type_c_me' => '*',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'I',
            'option_type_b_not_me' => 'C',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => '*',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Menjadi frustrasi',
            'option_b' => 'Menyimpan perasaan saya',
            'option_c' => 'Menceritakan sisi saya',
            'option_d' => 'Siap beroposisi',
            // D,I,S,C,*
            'option_type_a_me' => 'C',
            'option_type_b_me' => 'S',
            'option_type_c_me' => '*',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'C',
            'option_type_b_not_me' => 'S',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Hidup, Suka bicara',
            'option_b' => 'Gerak cepat, Tekun',
            'option_c' => 'Usaha menjaga keseimbangan',
            'option_d' => 'Usaha mengikuti aturan',
            // D,I,S,C,*
            'option_type_a_me' => 'I',
            'option_type_b_me' => 'D',
            'option_type_c_me' => 'S',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Kelola waktu secara efisien',
            'option_b' => 'Sering terburu-buru, Merasa tertekan',
            'option_c' => 'Masalah sosial itu penting',
            'option_d' => 'Suka selesaikan apa yang saya mulai',
            // D,I,S,C,*
            'option_type_a_me' => 'C',
            'option_type_b_me' => 'D',
            'option_type_c_me' => 'I',
            'option_type_d_me' => 'S',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'S',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Tolak perubahan mendadak',
            'option_b' => 'Cenderung janji berlebihan',
            'option_c' => 'Tarik diri di tengah tekanan',
            'option_d' => 'Tidak takut bertempur',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => 'I',
            'option_type_c_me' => '*',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => 'C',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Penyemangat yang baik',
            'option_b' => 'Pendengar yang baik',
            'option_c' => 'Penganalisa yang baik',
            'option_d' => 'Delegator yang baik',
            // D,I,S,C,*
            'option_type_a_me' => 'I',
            'option_type_b_me' => 'S',
            'option_type_c_me' => 'C',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'I',
            'option_type_b_not_me' => 'S',
            'option_type_c_not_me' => 'C',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Hasil adalah penting',
            'option_b' => 'Lakukan dengan benar, Akurasi penting',
            'option_c' => 'Dibuat menyenangkan',
            'option_d' => 'Mari kerjakan bersama',
            // D,I,S,C,*
            'option_type_a_me' => 'D',
            'option_type_b_me' => 'C',
            'option_type_c_me' => '*',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => 'C',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'S',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Akan berjalan terus tanpa kontrol diri',
            'option_b' => 'Akan membeli sesuai dorongan hati',
            'option_c' => 'Akan menunggu, Tanpa tekanan',
            'option_d' => 'Akan mengusahakan  yang kuinginkan',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'D',
            'option_type_c_me' => 'S',
            'option_type_d_me' => 'I',
            'option_type_a_not_me' => 'C',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => '*',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Ramah, Mudah bergabung',
            'option_b' => 'Unik, Bosan rutinitas',
            'option_c' => 'Aktif mengubah sesuatu',
            'option_d' => 'Ingin hal-hal yang pasti',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => '*',
            'option_type_c_me' => 'D',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => 'D',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Non-konfrontasi, Menyerah',
            'option_b' => 'Dipenuhi hal detail',
            'option_c' => 'Perubahan pada menit terakhir',
            'option_d' => 'Menuntut, Kasar',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'C',
            'option_type_c_me' => 'I',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Ingin kemajuan',
            'option_b' => 'Puas dengan segalanya',
            'option_c' => 'Terbuka memperlihatkan perasaan',
            'option_d' => 'Rendah hati, Sederhana',
            // D,I,S,C,*
            'option_type_a_me' => 'D',
            'option_type_b_me' => 'S',
            'option_type_c_me' => 'I',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => '*',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Tenang, Pendiam',
            'option_b' => 'Bahagia, Tanpa beban',
            'option_c' => 'Menyenangkan, Baik hati',
            'option_d' => 'Tak gentar, Berani',
            // D,I,S,C,*
            'option_type_a_me' => 'C',
            'option_type_b_me' => 'I',
            'option_type_c_me' => 'S',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'C',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => '*',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Menggunakan waktu berkualitas dgn teman',
            'option_b' => 'Rencanakan masa depan, Bersiap',
            'option_c' => 'Bepergian demi petualangan baru',
            'option_d' => 'Menerima ganjaran atas tujuan yg dicapai',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => 'C',
            'option_type_c_me' => 'I',
            'option_type_d_me' => 'D',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'D',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Aturan perlu dipertanyakan',
            'option_b' => 'Aturan membuat adil',
            'option_c' => 'Aturan membuat bosan',
            'option_d' => 'Aturan membuat aman',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'C',
            'option_type_c_me' => 'I',
            'option_type_d_me' => 'S',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'S',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Pendidikan, Kebudayaan',
            'option_b' => 'Prestasi, Ganjaran',
            'option_c' => 'Keselamatan, keamanan',
            'option_d' => 'Sosial, Perkumpulan kelompok',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'D',
            'option_type_c_me' => 'S',
            'option_type_d_me' => 'I',
            'option_type_a_not_me' => 'C',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => '*',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Memimpin, Pendekatan langsung',
            'option_b' => 'Suka bergaul, Antusias',
            'option_c' => 'Dapat diramal, Konsisten',
            'option_d' => 'Waspada, Hati-hati',
            // D,I,S,C,*
            'option_type_a_me' => 'D',
            'option_type_b_me' => '*',
            'option_type_c_me' => '*',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => '*',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Tidak mudah dikalahkan',
            'option_b' => 'Kerjakan sesuai perintah, Ikut pimpinan',
            'option_c' => 'Mudah terangsang, Riang',
            'option_d' => 'Ingin segalanya teratur, Rapi',
            // D,I,S,C,*
            'option_type_a_me' => 'D',
            'option_type_b_me' => 'S',
            'option_type_c_me' => 'I',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Saya akan pimpin mereka',
            'option_b' => 'Saya akan melaksanakan',
            'option_c' => 'Saya akan meyakinkan mereka',
            'option_d' => 'Saya dapatkan fakta',
            // D,I,S,C,*
            'option_type_a_me' => 'D',
            'option_type_b_me' => 'S',
            'option_type_c_me' => 'I',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => '*',
            'option_type_b_not_me' => 'S',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => '*',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Memikirkan orang dahulu',
            'option_b' => 'Kompetitif, Suka tantangan',
            'option_c' => 'Optimis, Positif',
            'option_d' => 'Pemikir logis, Sistematik',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => 'D',
            'option_type_c_me' => 'I',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => 'D',
            'option_type_c_not_me' => 'I',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Menyenangkan orang, Mudah setuju',
            'option_b' => 'Tertawa lepas, Hidup',
            'option_c' => 'Berani, Tak gentar',
            'option_d' => 'Tenang, Pendiam',
            // D,I,S,C,*
            'option_type_a_me' => 'S',
            'option_type_b_me' => '*',
            'option_type_c_me' => 'D',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => 'D',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Ingin otoritas lebih',
            'option_b' => 'Ingin kesempatan baru',
            'option_c' => 'Menghindari konflik',
            'option_d' => 'Ingin petunjuk yang jelas',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'I',
            'option_type_c_me' => 'S',
            'option_type_d_me' => '*',
            'option_type_a_not_me' => 'D',
            'option_type_b_not_me' => '*',
            'option_type_c_not_me' => 'S',
            'option_type_d_not_me' => 'C',
        ]);

        DB::table('psikotest_question')->insert([
            'psikotest_id' => '1',
            'question' => 'Gambaran Diri',
            'option_a' => 'Dapat diandalkan, Dapata dipercaya',
            'option_b' => 'Kreatif, Unik',
            'option_c' => 'Garis dasar, Orientasi hasil',
            'option_d' => 'Jalankan standar yang tinggi, Akurat',
            // D,I,S,C,*
            'option_type_a_me' => '*',
            'option_type_b_me' => 'I',
            'option_type_c_me' => 'D',
            'option_type_d_me' => 'C',
            'option_type_a_not_me' => 'S',
            'option_type_b_not_me' => 'I',
            'option_type_c_not_me' => '*',
            'option_type_d_not_me' => '*',
        ]);
    }
}
