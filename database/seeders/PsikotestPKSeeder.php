<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PsikotestPKSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('psikotest_pk')->insert([
            'key' => '-22',
            'd' => '-8.00',
            'i' => '-8.00',
            's' => '-8.00',
            'c' => '-7.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-21',
            'd' => '-7.50',
            'i' => '-8.00',
            's' => '-8.00',
            'c' => '-7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-20',
            'd' => '-7.00',
            'i' => '-8.00',
            's' => '-8.00',
            'c' => '-7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-19',
            'd' => '-6.80',
            'i' => '-8.00',
            's' => '-8.00',
            'c' => '-7.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-18',
            'd' => '-6.75',
            'i' => '-7.00',
            's' => '-7.50',
            'c' => '-6.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-17',
            'd' => '-6.70',
            'i' => '-6.70',
            's' => '-7.30',
            'c' => '-6.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-16',
            'd' => '-6.50',
            'i' => '-6.70',
            's' => '-7.30',
            'c' => '-6.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-15',
            'd' => '-6.30',
            'i' => '-6.70',
            's' => '-7.00',
            'c' => '-6.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-14',
            'd' => '-6.10',
            'i' => '-6.70',
            's' => '-6.50',
            'c' => '-6.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-13',
            'd' => '-5.90',
            'i' => '-6.70',
            's' => '-6.50',
            'c' => '-6.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-12',
            'd' => '-5.70',
            'i' => '-6.70',
            's' => '-6.50',
            'c' => '-5.85'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-11',
            'd' => '-5.30',
            'i' => '-6.70',
            's' => '-6.50',
            'c' => '-5.85'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-10',
            'd' => '-4.30',
            'i' => '-6.50',
            's' => '-6.00',
            'c' => '-5.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-9',
            'd' => '-3.50',
            'i' => '-6.00',
            's' => '-4.70',
            'c' => '-4.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-8',
            'd' => '-3.25',
            'i' => '-5.70',
            's' => '-4.30',
            'c' => '-4.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-7',
            'd' => '-3.00',
            'i' => '-4.70',
            's' => '-3.50',
            'c' => '-3.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-6',
            'd' => '-2.75',
            'i' => '-4.30',
            's' => '-3.00',
            'c' => '-3.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-5',
            'd' => '-2.50',
            'i' => '-3.50',
            's' => '-2.00',
            'c' => '-2.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-4',
            'd' => '-1.50',
            'i' => '-3.00',
            's' => '-1.50',
            'c' => '-0.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-3',
            'd' => '-1.00',
            'i' => '-2.00',
            's' => '-1.00',
            'c' => '0.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-2',
            'd' => '-0.50',
            'i' => '-1.50',
            's' => '-0.50',
            'c' => '0.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '-1',
            'd' => '-0.25',
            'i' => '0.00',
            's' => '0.00',
            'c' => '0.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '0',
            'd' => '0.00',
            'i' => '0.50',
            's' => '1.00',
            'c' => '1.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '1',
            'd' => '0.50',
            'i' => '1.00',
            's' => '1.50',
            'c' => '3.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '2',
            'd' => '0.70',
            'i' => '1.50',
            's' => '2.00',
            'c' => '4.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '3',
            'd' => '1.00',
            'i' => '3.00',
            's' => '3.00',
            'c' => '4.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '4',
            'd' => '1.30',
            'i' => '4.00',
            's' => '3.50',
            'c' => '5.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '5',
            'd' => '1.50',
            'i' => '4.30',
            's' => '4.00',
            'c' => '5.70'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '6',
            'd' => '2.00',
            'i' => '5.00',
            's' => '0.00',
            'c' => '6.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '7',
            'd' => '2.50',
            'i' => '5.50',
            's' => '4.70',
            'c' => '6.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '8',
            'd' => '3.50',
            'i' => '6.50',
            's' => '5.00',
            'c' => '6.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '9',
            'd' => '4.00',
            'i' => '6.70',
            's' => '5.50',
            'c' => '6.70'
        ]);
        
        DB::table('psikotest_pk')->insert([
            'key' => '10',
            'd' => '4.70',
            'i' => '7.00',
            's' => '6.00',
            'c' => '7.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '11',
            'd' => '4.85',
            'i' => '7.30',
            's' => '6.20',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '12',
            'd' => '5.00',
            'i' => '7.30',
            's' => '6.30',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '13',
            'd' => '5.50',
            'i' => '7.30',
            's' => '6.50',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '14',
            'd' => '6.00',
            'i' => '7.30',
            's' => '6.70',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '15',
            'd' => '6.30',
            'i' => '7.30',
            's' => '7.00',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '16',
            'd' => '6.50',
            'i' => '7.30',
            's' => '7.30',
            'c' => '7.30'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '17',
            'd' => '6.70',
            'i' => '7.30',
            's' => '7.30',
            'c' => '7.50'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '18',
            'd' => '7.00',
            'i' => '7.50',
            's' => '7.30',
            'c' => '8.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '19',
            'd' => '7.30',
            'i' => '8.00',
            's' => '7.30',
            'c' => '8.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '20',
            'd' => '7.30',
            'i' => '8.00',
            's' => '7.50',
            'c' => '8.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '21',
            'd' => '7.50',
            'i' => '8.00',
            's' => '8.00',
            'c' => '8.00'
        ]);

        DB::table('psikotest_pk')->insert([
            'key' => '22',
            'd' => '8.00',
            'i' => '8.00',
            's' => '8.00',
            'c' => '8.00'
        ]);

    }   
}
