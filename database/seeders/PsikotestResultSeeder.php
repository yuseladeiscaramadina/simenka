<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PsikotestResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('psikotest_result')->insert([
            'key' => '1',
            'title' => 'Logical Thinker',
            'definition' => '<li>Pendiam</li><li>Anti Kritik</li><li>Perfeksionis</li><li>Cenderung Santi</li><li>Detail</li><li>Empati</li><li>Rapi</li><li>Organized</li><li>Kaku pada Metode & Prosedur</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '2',
            'title' => 'Establisher',
            'definition' => '<li>Individualis</li><li>Ego Tinggi, Kurang Sensitif</li><li>Kurang Pertimbangan</li><li>Efektif</li><li>High Motivation</li><li>Bersemangat Tinggi</li><li>Percaya Diri, Cenderung Nekat</li><li>Kreatif</li><li>Terlalu Dominan</li><li>Agresif</li><li>Terlalu Dinamis</li><li>Penuh Ambisi</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '3',
            'title' => 'Designer',
            'definition' => '<li>Sensitif</li><li>Kurang Cepat</li><li>Anti Tekanan</li><li>Terlalu Mandiri</li><li>Kurang Percaya Orang Lain</li><li>Anti Kritik</li><li>Dingin</li><li>Kreatif</li><li>Result Oriented</li><li>Suka Tantangan</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '4',
            'title' => 'Negotiator',
            'definition' => '<li>Suka Bergaul</li><li>Anti Rutin</li><li>Aktif</li><li>Terlalu Percaya Diri</li><li>Agresif</li><li>Optimis</li><li>Kurang Detail</li><li>Result Oriented</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '5',
            'title' => 'Confident & Determined',
            'definition' => '<li>Pandai Memilih Orang</li><li>Leader</li><li>Good Interpersonal Skill</li><li>Dominan</li><li>Agresif</li><li>Perfeksionis</li><li>Good Communication Skill</li><li>Aktif</li><li>Need Recognition Reward</li><li>Kurang Peduli pada Aturan</li><li>Terburu - buru</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '6',
            'title' => 'Reformer',
            'definition' => '<li>Mudah Bergaul</li><li>Leader</li><li>Sadar Diri</li><li>Butuh Pujian & Penghargaan</li><li>Cepat Percaya Orang</li><li>Mudah Simpati & Empati</li><li>Motivator</li><li>Optimis & Positif</li><li>Anti Aturan</li><li>Kurang Detail</li><li>Terlalu Selektif</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '7',
            'title' => 'Motivator',
            'definition' => '<li>Leader</li><li>Supporter</li><li>Sosialisasi Baik</li><li>Butuh Ketegatasan</li><li>Butuh Pujian & Penghargaan</li><li>Kurang Detail</li><li>Agak Kaku</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '8',
            'title' => 'Inquirer',
            'definition' => '<li>Full Self Control</li><li>Sabar<li><li>Penuh Pertimbangan<li><li>Good Interpersonal<li><li>Selektif<li><li>Lambat Adaptasi<li><li>Inisiatif Kurang<li><li>Result Oriented<li><li>Kaku dan Keras Kepala<li><li>Good Service<li><li>Kurang dalam hal Managerial<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '9',
            'title' => 'Pengambilan Keputusan',
            'definition' => '<li>Pekerja Keras</li><li>Leader<li><li>Banyak Minat<li><li>Dingin / Task Oriented<li><li>Kurang Pergaulan<li><li>Kontrol Emosi Kurang<li><li>Suka Tantangan<li><li>Cepat Bosan<li><li>Anti Aturan<li><li>Kurang Detail<li><li>Kurang Peduli Wewenang<li><li>Argumentatif<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '10',
            'title' => 'Director',
            'definition' => '<li>Pengelola</li><li>Enerjik<li><li>Kurang Detail<li><li>Mudah Bosan<li><li>Agresif<li><li>Arogan<li><li>Kurang Focus<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '11',
            'title' => 'Self-Motivated',
            'definition' => '<li>Objektif dan Analitis</li><li>Mandiri<li><li>Good Planner<li><li>Komitmen terhadap Target<li><li>Menghindari Konflik<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '12',
            'title' => 'Mediator',
            'definition' => '<li>Loyal</li><li>Tight Scheduled<li><li>Curious<li><li>Sensitif<li><li>Good Communication Skill<li><li>Good Analitical Think<li><li>Good Interpersonal Skill<li><li>Cepat Beradaptasi<li><li>Anti Kritik<li><li>Not Leader<li><li>Work / Play Conflict<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '13',
            'title' => 'Practitioner',
            'definition' => '<li>Perfeksionis</li><li>Quality Oriented<li><li>Scheduled<li><li>Anti Kejutan<li><li>Good Interpersonal Skill<li><li>Terlalu Detail<li><li>Sistematis<li><li>Kaku / Tidak Fleksibel<li><li>Monoton<li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '14',
            'title' => 'Responsive & Thoughtful',
            'definition' => '<li>High Energy</li><li>Good Communication Skill</li><li>To The Point</li><li>Sensitif</li><li>Banyak Bicara</li><li>Need Recognation</li><li>Need Socialism</li><li>Anti Terhadap Kritik</li><li>Terlalu Banyak Bersosialisasi</li><li>Leadership Kurang</li><li>Kurang Fokus</li><li>Anti Deadline</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '15',
            'title' => 'Specialist',
            'definition' => '<li>Stabil & Konsisten</li><li>Terkendali</li><li>Nyaman di Belakang Layar</li><li>Sabar</li><li>Loyal</li><li>Sulit Adaptasi</li><li>Process Oriented</li><li>Teguh</li><li>Need for Peace</li><li>Anti Perubahan</li><li>Sulit Menentukan Prioritas</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '16',
            'title' => 'Perfectionist',
            'definition' => '<li>Detail & Teliti</li><li>Butuh Situasi Stabil</li><li>Sistematik & Prosedural</li><li>Menghindari Konflik</li><li>Anti Kritik</li><li>Lambat Memutuskan</li><li>Sulit Adaptasi</li><li>Pendendam</li><li>Anti Perubahan</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '17',
            'title' => 'Peacemaker, Respectfull & Accurat',
            'definition' => '<li>Sulit Beradaptasi</li><li>Anti Kritik</li><li>Pendendam</li><li>Sukar Berubah</li><li>Detail</li><li>Empati</li><li>Memikirkan Dampak ke Orang Lain</li><li>Terlalu Mendalam dalam Berpikir</li><li>Concern ke Data dan Fakta</li><li>Intovert</li><li>Loyal</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '18',
            'title' => 'Challenger',
            'definition' => '<li>Seorang yang tekun</li><li>Sensitif Terhadap permasalahan</li><li>Mempunyai Keputusan yang Kuat</li><li>Kreatif dalam memecahkan masalah</li><li>Memiliki reaksi yang cepat</li><li>Mampu mencari solusi permasalahan</li><li>Banyak Memberikan Ide - Ide</li><li>Usaha yang Keras pada Ketepatan</li><li>Cenderung Perfeksionis</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '19',
            'title' => 'Chancellor',
            'definition' => '<li>Seorang yang ramah secara alami</li><li>Menggabungkan kesenangan dengan pekerjaan</li><li>Menyukai hubungan dengan sesama</li><li>Menikmati interaksi dengan sesama</li><li>Dapat mengerjakan hal-hal detil</li><li>Ingin melakukan segala sesuatu dengan tepat</li><li>Menilai orang dan tugas secara hati-hati</li><li>Sering melalaikan perencanaan yang seksama</li><li>Mudah beralih kepada proyek-proyek baru</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '20',
            'title' => 'Director',
            'definition' => '<li>Seorang yang obyektif dan analitis</li><li>Ingin terlibat dalam situasi</li><li>Ingin memberikan bantuan dan dukungan</li><li>Termotivasi oleh target pribadi</li><li>Berorientasi terhadap pekerjaannya</li><li>Menyukai hubungan dengan sesama</li><li>Mempunyai determinasi yang kuat</li><li>Karakternya tenang</li><li>Stabil dan daya tahannya tinggi</li><li>Ulet dalam memulai pekerjaan</li><li>Berusaha keras mencapai sasarannya</li><li>Mandiri dan cermat</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '21',
            'title' => 'Communicator',
            'definition' => '<li>Antusias</li><li>Percaya</li><li>Optimis</li><li>Persuasif</li><li>Bicara aktif</li><li>Impulsif</li><li>Emosional</li><li>Ramah</li>><li>Inspirasional</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '22',
            'title' => 'Advisor',
            'definition' => '<li>Hangat</li><li>Simpati</li><li>Tenang dalam situasi sosial</li><li>Pendengar yang baik</li><li>Demonstratif</li><li>Tidak memaksakan idenya pada orang lain</li><li>Kurang tegas dalam memberi perintah</li><li>Menerima kritik</li><li>Toleran dan sabar</li><li>Penjaga damai</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '23',
            'title' => 'Assessor',
            'definition' => '<li>Ramah</li><li>Suka berteman</li><li>Nyaman walapun dengan orang asing</li><li>Mudah mengembangkan hubungan baru</li><li>Dapat mengendalikan diri</li><li>Sangat sosial</li><li>Cenderung perfeksionis alamiah</li><li>Mempromosikan tugas-tugas orang lain</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '24',
            'title' => 'Advocate',
            'definition' => '<li>Stabil</li><li>Ramah</li><li>Detail ketika situasi membutuhkan</li><li>Cenderung individualis</li><li>Teguh pendirian</li><li>Menyukai hubungan dengan orang</li><li>Mendukung pihak yang lemah</li><li>Ingin diterima sebagai anggota tim</li><li>Ingin orang lain menyukainya</li><li>Sulit membuat keputusan</li><li>Moderat</li><li>Cermat dan dapat diandalkan</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '25',
            'title' => 'Contemplator',
            'definition' => '<li>Berorientasi pada hal-hal detil</li><li>Mempunyai standar tinggi untuk dirinya</li><li>Logis dan analitis</li><li>Ingin berbuat yang terbaik</li><li>Selalu berpikir ada ruang untuk kemajuan</li><li>Kompetitif</li><li>Ingin menghasilkan mutu yang terbaik</li><li>Mampu mencapai sasarannya</li><li>Sangat memusatkan perhatian pada tugas</li><li>Mantap dan dapat diandalkan</li>',
        ]);

        DB::table('psikotest_result')->insert([
            'key' => '26',
            'title' => 'Precisionist',
            'definition' => '<li>Sistematis dan Prosedural</li><li>Teratur & memiliki perencanaan yang baik</li><li>Teliti</li><li>Fokus pada detil</li><li>Bijaksana</li><li>Diplomatis</li><li>Jarang menentang rekan kerjanya</li><li>Ia sangat berhati-hati</li><li>Mengharapkan akurasi dan standard tinggi</li><li>Menginginkan adanya petunjuk standard</li><li>Tidak menginginkan perubahan mendadak</li>',
        ]);
    }
}
