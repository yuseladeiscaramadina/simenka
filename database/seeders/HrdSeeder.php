<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HrdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hrd')->insert([
            'name' => 'Tjatur Agustianawati',
            'email' => 'agustianawati@gmail.com',
            'password' => Hash::make('12345678'),
            'token' => Str::random(16),
            'profile_picture' => 'dashboard/images/user/default_avatar.png'
        ]);
    }
}
