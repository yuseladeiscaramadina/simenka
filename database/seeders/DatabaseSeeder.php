<?php

namespace Database\Seeders;

use App\Models\PsikotestK;
use App\Models\PsikotestP;
use App\Models\TestQuestion;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            ApplicantSeeder::class,
            HrdSeeder::class,
            JobVacancySeeder::class,
            ApplicationSeeder::class,
            PsikotestSeeder::class,
            PsikotestQuestionSeeder::class,
            TestSeeder::class,
            PsikotestResultSeeder::class,
            PsikotestPSeeder::class,
            PsikotestKSeeder::class,
            PsikotestPKSeeder::class,
            TestQuestionSeeder::class
        ]);
    }
}
