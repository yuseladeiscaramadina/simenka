<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test_question')->insert([
            'test_id' => 1,
            'question' => 'Bahasa Apakah ini?',
            'image' => 'https://i.pinimg.com/474x/dc/97/a0/dc97a030f433177e0d0275e513fa705a.jpg',
            'option_a' => 'CSS',
            'option_b' => 'HTML',
            'option_c' => 'C',
            'option_d' => 'Python',
            'answer' => 'c'
        ]);

        DB::table('test_question')->insert([
            'test_id' => 1,
            'question' => 'Apakah kepanjangan dari HTML?',
            'image' => '',
            'option_a' => 'HTEmEL',
            'option_b' => 'HTML',
            'option_c' => 'Hateemel',
            'option_d' => 'HTM',
            'answer' => 'b'
        ]);

        DB::table('test_question')->insert([
            'test_id' => 1,
            'question' => 'Kapan indonesia Merdeka?',
            'image' => '',
            'option_a' => '17 Agustus 1945',
            'option_b' => '16 Agustus 1945',
            'option_c' => '25 Januari 1999',
            'option_d' => '10 Desember 1999',
            'answer' => 'a'
        ]);
    }
}
