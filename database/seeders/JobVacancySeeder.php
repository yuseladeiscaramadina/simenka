<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobVacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_vacancy')->insert([
            'hrd_id' => 1,
            'title' => 'Backend Developer',
            'description' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Esse hic distinctio quod itaque ullam fugiat, magni, iure magnam, exercitationem in obcaecati? Eveniet labore eum delectus pariatur provident et necessitatibus adipisci.',
            'requirement' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Esse hic distinctio quod itaque ullam fugiat, magni, iure magnam, exercitationem in obcaecati? Eveniet labore eum delectus pariatur provident et necessitatibus adipisci.',
            'image' => '1',
            'is_active' => false
        ]);
    }
}
