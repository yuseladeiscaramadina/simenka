<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ApplicantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applicant')->insert([
            'name' => 'Yusela Deisca Ramadina',
            'email' => 'yuselaramadina@gmail.com',
            'password' => Hash::make('12345678'),
            'id_card_number' => '23647823492432',
            'family_card_number' => '87192311987322',
            'phone_number' => '081277057373',
            'ktp_address' => 'Gg Flamboyan pokoknya',
            'current_address' => 'Gg Flamboyan pokoknya',
            'religion' => 'Islam',
            'age' => '22',
            'gender' => 'wanita',
            'place_of_birth' => 'Balikpapan',
            'date_of_birth' => '1999-12-10',
            'last_education' => 'S1',
            'major' => 'Sistem Informasi',
            'last_place_education' => 'Institut Teknologi Kalimantan',
            'mother_name' => 'Yulisa',
            'account_number' => '0928091238',
            'bank' => 'BNI',
            'doc_id_card' => '',
            'doc_family_card' => '',
            'doc_cv' => '',
            'doc_certificate_of_education' => '',
            'doc_transcript' => '',
            'doc_taxpayer' => '',
            'doc_bpjs' => '',
            'token' => Str::random(16),
            'profile_picture' => 'dashboard/images/user/default_avatar.png'
        ]);
    }
}
