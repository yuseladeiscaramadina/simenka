<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsikotestPK extends Model
{
    use HasFactory;

    protected $table = 'psikotest_pk';

    protected $fillable = [
        'key',
        'd',
        'i',
        's',
        'c'
    ];
}
