<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsikotestQuestion extends Model
{
    use HasFactory;

    protected $table = 'psikotest_question';

    protected $fillable = [
        'question',
        'option_a',
        'option_type_a',
        'option_b',
        'option_type_b',
        'option_c',
        'option_type_c',
        'option_d',
        'option_type_d',
    ];
    public function psikotest()
    {
        return $this->belongsTo('App\Models\Psikotest');
    }
}
