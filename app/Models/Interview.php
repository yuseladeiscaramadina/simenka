<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    use HasFactory;

    protected $table = 'interview';

    protected $fillable = [
        'application_id',
        'many_interview',
        'event_date',
        'link',
        'meeting_id',
        'password',
    ];

    public function application()
    {
        return $this->belongsTo('App\Models\Application');
    }
}
