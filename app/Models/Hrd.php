<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Hrd extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'hrd';

    protected $fillable = [
        'name',
        'email',
        'password',
        'profile_picture',
        'token',
        'date_login'
    ];

    public function jobVacancy()
    {
        return $this->hasMany('App\Models\JobVacancy', 'hrd_id');
    }
}
