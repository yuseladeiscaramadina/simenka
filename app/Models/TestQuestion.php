<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    use HasFactory;

    protected $table = 'test_question';

    protected $fillable = [
        'test_id',
        'question',
        'image',
        'option_a',
        'option_b',
        'option_c',
        'option_d',
        'answer',
    ];

    public function test()
    {
        return $this->belongsTo('App\Models\Test');
    }
}
