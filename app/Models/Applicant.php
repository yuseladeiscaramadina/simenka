<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Applicant extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'applicant';

    protected $fillable = [
        'name',
        'email',
        'password',
        'id_card_number',
        'family_card_number',
        'phone_number',
        'ktp_address',
        'current_address',
        'religion',
        'age',
        'gender',
        'place_of_birth',
        'date_of_birth',
        'last_education',
        'major',
        'last_place_education',
        'mother_name',
        'account_number',
        'bank',
        'access',
        'token',
        'profile_picture',
        'doc_id_card',
        'doc_family_card',
        'doc_cv',
        'doc_certificate_of_education',
        'doc_transcript',
        'doc_taxpayer',
        'doc_bpjs',
        'date_login'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function application()
    {
        return $this->hasMany('App\Models\Application', 'applicant_id');
    }
}
