<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Psikotest extends Model
{
    use HasFactory;

    protected $table = 'psikotest';

    protected $fillable = [
        'name',
    ];

    public function psikotestQuestion()
    {
        return $this->hasMany('App\Models\PsikotestQuestion', 'psikotest_id');
    }

    public function applicantPsikotest()
    {
        return $this->hasMany('App\Models\ApplicantPsikotest', 'psikotest_id');
    }
}
