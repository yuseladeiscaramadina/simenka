<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantPsikotest extends Model
{
    use HasFactory;

    protected $table = 'applicant_psikotest';

    protected $fillable = [
        'application_id',
        'psikotest_id',
        'status',
        'score',
        'progress_question',
        'progress_pk',
        'p_d',
        'p_i',
        'p_s',
        'p_c',
        'p_star',
        'k_d',
        'k_i',
        'k_s',
        'k_c',
        'k_star',
        'pk_d',
        'pk_i',
        'pk_s',
        'pk_c',
        'result_p',
        'result_k',
        'result_pk'
    ];

    public function psikotest()
    {
        return $this->belongsTo('App\Models\Psikotest');
    }

    public function application()
    {
        return $this->belongsTo('App\Models\Application');
    }
}
