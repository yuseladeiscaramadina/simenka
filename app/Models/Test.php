<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $table = 'test';

    protected $fillable = [
        'name',
        'kkm',
    ];

    public function testQuestion()
    {
        return $this->hasMany('App\Models\TestQuestion', 'test_id');
    }

    public function applicantTest()
    {
        return $this->hasMany('App\Models\ApplicantPsikotest', 'test_id');
    }
}
