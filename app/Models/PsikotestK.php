<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsikotestK extends Model
{
    use HasFactory;

    protected $table = 'psikotest_k';

    protected $fillable = [
        'key',
        'd',
        'i',
        's',
        'c'
    ];
}
