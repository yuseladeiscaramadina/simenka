<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $table = 'application';

    protected $fillable = [
        'user_id',
        'job_vacancy_id',
        'status',
        'deadline'
    ];

    public function job_vacancy()
    {
        return $this->belongsTo('App\Models\JobVacancy');
    }

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }

    public function interview()
    {
        return $this->hasMany('App\Models\Interview', 'application_id');
    }

    public function applicant_psikotest()
    {
        return $this->hasOne('App\Models\ApplicantPsikotest', 'application_id');
    }

    public function applicant_test()
    {
        return $this->hasOne('App\Models\ApplicantTest', 'application_id');
    }
}
