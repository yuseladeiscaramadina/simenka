<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantTest extends Model
{
    use HasFactory;

    protected $table = 'applicant_test';

    protected $fillable = [
        'application_id',
        'test_id',
        'status',
        'score',
    ];

    public function test()
    {
        return $this->belongsTo('App\Models\Test');
    }

    public function application()
    {
        return $this->belongsTo('App\Models\Application');
    }
}
