<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsikotestResult extends Model
{
    use HasFactory;

    protected $table = 'psikotest_result';

    protected $fillable = [
        'key',
        'title',
        'definition'
    ];
}
