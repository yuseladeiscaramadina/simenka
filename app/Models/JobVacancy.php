<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobVacancy extends Model
{
    use HasFactory;

    protected $table = 'job_vacancy';

    protected $fillable = [
        'title',
        'description',
        'requirement',
        'image',
        'is_active'
    ];

    public function hrd()
    {
        return $this->belongsTo('App\Models\Hrd');
    }

    public function application()
    {
        return $this->hasMany('App\Models\Application', 'job_vacancy_id');
    }

    public function interview()
    {
        return $this->hasOne('App\Models\Interview', 'application_id');
    }
}
