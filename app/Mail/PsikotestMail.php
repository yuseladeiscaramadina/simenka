<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PsikotestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $eventDate;
    public function __construct($name, $eventDate)
    {
        $this->name = $name;
        $this->eventDate = $eventDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('halosimenka@gmail.com')
            ->view('extra/psikotestMail')
            ->subject('Recruitment - MKA')
            ->with([
                'name' => $this->name,
                'eventDate' => $this->eventDate,
            ]);
    }
}
