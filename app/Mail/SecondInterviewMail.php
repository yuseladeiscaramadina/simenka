<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SecondInterviewMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $eventDate;
    private $link;
    private $roomId;
    private $roomPassword;
    public function __construct($name, $eventDate, $link, $roomId, $roomPassword)
    {
        $this->name = $name;
        $this->eventDate = $eventDate;
        $this->link = $link;
        $this->roomId = $roomId;
        $this->roomPassword = $roomPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('halosimenka@gmail.com')
            ->view('extra/secondInterviewMail')
            ->subject('Recruitment - MKA')
            ->with([
                'name' => $this->name,
                'eventDate' => $this->eventDate,
                'link' => $this->link,
                'roomId' => $this->roomId,
                'roomPassword' => $this->roomPassword,
            ]);
    }
}
