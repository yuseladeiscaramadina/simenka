<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\Application;
use App\Models\Diki;
use App\Models\JobVacancy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::guest()) {
                if (Auth::user()->date_login != Carbon::now()->toDateString()) {
                    Auth::logout();
                    Redirect::to('/')->send();
                }
            }
            return $next($request);
        });
    }

    public function index()
    {
        $newestJob = JobVacancy::latest('created_at')->where('is_active', true)->first();
        if ($newestJob == null) {
            $job = null;
        } else {
            $job = JobVacancy::where('id', '!=', $newestJob->id)->where('is_active', true)->latest('created_at')->take(3)->get();
        }
        return view('beranda/beranda', ['newestJob' => $newestJob, 'jobs' => $job]);
    }

    public function allJobVacancy()
    {
        $job = JobVacancy::where('is_active', true)->get();
        return view('beranda/all-job-vacancy', ['jobs' => $job]);
    }

    public function jobVacancy($id)
    {
        $job = JobVacancy::find($id);
        $isApplied = false;

        if (Auth::id() != null) {
            $application = Application::with('job_vacancy')->where(['applicant_id' => Auth::id(), 'job_vacancy_id' => $id])->where('status', '!=', 'diterima')->where('status', '!=', 'ditolak')->first();

            if ($application != null) {
                $isApplied = true;
            }
        }

        return view('beranda/detail-job-vacancy', ['job' => $job, 'isApplied' => $isApplied]);
    }

    public function registerVacancy($id)
    {
        if (Auth::id() == null) {
            return redirect()->back()->with(['msg' => 'noauth']);
        }

        if (Auth::user()->profile_picture == 'dashboard/images/user/default_avatar.png') {
            return redirect()->back()->with(['msg' => 'nodata']);
        }

        foreach (Auth::user()->getAttributes() as $key => $value) {
            if ($key != 'date_login' && $key != 'remember_token' && $key != 'created_at' && $key != 'updated_at') {
                if ($value == null) {
                    return redirect()->back()->with(['msg' => 'nodata']);
                }
            }
        }

        $application = Application::with('job_vacancy')->where(['applicant_id' => Auth::id()])->where('status', '!=', 'diterima')->where('status', '!=', 'ditolak')->first();

        if ($application != null) {
            return redirect()->back()->with(['msg' => 'hasactive', 'job_active' => $application->job_vacancy]);
        }

        $application = new Application();

        $application->applicant_id = Auth::id();
        $application->job_vacancy_id = $id;
        $application->status = 'pendaftaran';

        $application->save();
        return redirect()->back()->with(['msg' => 'success']);
    }
}
