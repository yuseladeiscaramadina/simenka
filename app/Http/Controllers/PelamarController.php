<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\ApplicantPsikotest;
use App\Models\ApplicantTest;
use App\Models\Application;
use App\Models\Psikotest;
use App\Models\PsikotestK;
use App\Models\PsikotestP;
use App\Models\PsikotestPK;
use App\Models\Test;
use App\Models\TestQuestion;
use Carbon\Carbon;
use Database\Seeders\PsikotestSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;

class PelamarController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::guest()) {
                if (Auth::user()->date_login != Carbon::now()->toDateString()) {
                    Auth::logout();
                    Redirect::to('/')->send();
                }
            }
            return $next($request);
        });
    }

    public function dashboard(Request $request)
    {
        $request->session()->forget('time_psikotest');
        $request->session()->forget('time_test');
        $request->session()->forget('answer_test');
        $request->session()->put('answer_test', []);
        $application = Application::with(['job_vacancy', 'interview'])->where('applicant_id', Auth::user()->id)->where('status', '!=', 'diterima')->where('status', '!=', 'ditolak')->first();

        $test = null;
        if ($application != null) {
            if ($application->status == 'psikotest') {
                $applicantPsikotest = ApplicantPsikotest::with('psikotest')->where('application_id', $application->id)->first();
                $now = Carbon::now();

                if ($now > $application->deadline) {
                    $applicantPsikotest->status = 'late';
                    $test = $applicantPsikotest;
                }
                $test = $applicantPsikotest;
            } else if ($application->status == 'keterampilan') {
                $applicantTest = ApplicantTest::with('test')->where('application_id', $application->id)->first();
                $now = Carbon::now();

                if ($now > $application->deadline) {
                    $applicantTest->status = 'late';
                    $test = $applicantTest;
                }
                $test = $applicantTest;
            }
        }

        return view('pelamar/dashboard', ['application' => $application, 'test' => $test]);
    }

    public function changePassword(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/change-password-2');
        } else {
            $user = Applicant::find(Auth::id());
            if (Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();

                Auth::logout();
                return redirect('/masuk')->with(['msg' => 'Password akun anda berhasil diubah! Silahkan masuk dengan password baru anda.', 'type' => 'success']);
            } else {
                return Redirect::back()->with(['msg' => 'Password lama yang anda masukkan salah', 'type' => 'warning']);
            }
        }
    }

    public function application()
    {
        $data = Application::with(['job_vacancy', 'applicant'])->where('status', 'diterima')->orWhere('status', 'ditolak')->where('applicant_id', Auth::id())->orderBy('status')->get();
        return view('pelamar/my-application', ['datas' => $data]);
    }

    public function profile(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('pelamar/my-profile');
        } else {
            $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->id_card_number = $request->id_card_number;
            $user->family_card_number = $request->family_card_number;
            $user->phone_number = $request->phone_number;
            $user->ktp_address = $request->ktp_address;
            $user->current_address = $request->current_address;
            $user->religion = $request->religion;
            $user->age = $request->age;
            $user->gender = $request->gender;
            $user->place_of_birth = $request->place_of_birth;
            $user->date_of_birth = $request->date_of_birth;
            $user->last_education = $request->last_education;
            $user->major = $request->major;
            $user->last_place_education = $request->last_place_education;
            $user->mother_name = $request->mother_name;
            $user->account_number = $request->account_number;
            $user->bank = $request->bank;

            $user->save();

            return redirect()->back()->with(['msg' => 'Profil Anda Berhasil Diperbaharui!', 'type' => 'success']);
        }
    }

    public function changeProfilePicture(Request $request)
    {
        if (Auth::user()->profile_picture != 'dashboard/images/user/default_avatar.png') {
            File::delete(Auth::user()->profile_picture);
        }
        $img1 = explode(';', $request->image);
        $img2 = explode(',', $img1[1]);
        $img3 = base64_decode($img2[1]);
        $name = 'dashboard/images/user/' . time() . "_" . $this->randString() . '.jpg';

        $user = Auth::user();
        $user->profile_picture = $name;
        $user->save();

        if (file_put_contents($name, $img3)) {
            return response()->json(['msg' => 'success']);
        } else {
            return response()->json(['msg' => 'failed']);
        }
    }

    function docKtp(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_id_card != null) {
                $path = public_path(Auth::user()->doc_id_card);
                $exp = explode('/', Auth::user()->doc_id_card);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_id_card;
            File::delete(Auth::user()->doc_id_card);
            $user = Auth::user();
            $user->doc_id_card = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/ktp";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_id_card = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docKk(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_family_card != null) {
                $path = public_path(Auth::user()->doc_family_card);
                $exp = explode('/', Auth::user()->doc_family_card);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_family_card;
            File::delete(Auth::user()->doc_family_card);
            $user = Auth::user();
            $user->doc_family_card = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/kk";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_family_card = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docCv(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_cv != null) {
                $path = public_path(Auth::user()->doc_cv);
                $exp = explode('/', Auth::user()->doc_cv);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_cv;
            File::delete(Auth::user()->doc_cv);
            $user = Auth::user();
            $user->doc_cv = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/cv";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_cv = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docIjazah(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_certificate_of_education != null) {
                $path = public_path(Auth::user()->doc_certificate_of_education);
                $exp = explode('/', Auth::user()->doc_certificate_of_education);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_certificate_of_education;
            File::delete(Auth::user()->doc_certificate_of_education);
            $user = Auth::user();
            $user->doc_certificate_of_education = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/ijazah";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_certificate_of_education = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docTranskrip(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_transcript != null) {
                $path = public_path(Auth::user()->doc_transcript);
                $exp = explode('/', Auth::user()->doc_transcript);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_transcript;
            File::delete(Auth::user()->doc_transcript);
            $user = Auth::user();
            $user->doc_transcript = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/transkrip";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_transcript = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docNpwp(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_taxpayer != null) {
                $path = public_path(Auth::user()->doc_taxpayer);
                $exp = explode('/', Auth::user()->doc_taxpayer);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_taxpayer;
            File::delete(Auth::user()->doc_taxpayer);
            $user = Auth::user();
            $user->doc_taxpayer = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/npwp";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_taxpayer = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function docBpjs(Request $request)
    {
        if ($request->method() == 'GET') {
            $data = [];

            if (Auth::user()->doc_bpjs != null) {
                $path = public_path(Auth::user()->doc_bpjs);
                $exp = explode('/', Auth::user()->doc_bpjs);
                $obj['name'] = end($exp);
                $obj['size'] = filesize($path);
                $obj['path'] = $path;
                $data[0] = $obj;
            }

            return response()->json($data);
        } else if ($request->method() == 'DELETE') {
            $nameFile = Auth::user()->doc_bpjs;
            File::delete(Auth::user()->doc_bpjs);
            $user = Auth::user();
            $user->doc_bpjs = null;
            $user->save();

            return response()->json(['success' => $nameFile]);
        } else if ($request->method() == 'POST') {
            if ($request->hasFile('file')) {
                $user = Auth::user();
                $img = $request->file('file');
                $dir = "dokumen/bpjs";
                $name = time() . "_" . $this->randString() . '.pdf';

                $img->move($dir, $name);
                $user->doc_bpjs = $dir . '/' . $name;
                $user->save();

                return response()->json(['success' => $name]);
            }
        }
    }

    function doPsikotest(Request $request)
    {
        $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'psikotest'])->first();
        $applicantPsikotest = ApplicantPsikotest::with('psikotest')->where('application_id', $application->id)->first();
        $psikotest = Psikotest::with('psikotestQuestion')->find($applicantPsikotest->psikotest_id);

        if ($request->method() == 'GET') {
            $psikotestQuestionNow = $psikotest->psikotestQuestion[0];
            $applicantPsikotest->status = 'started';
            $applicantPsikotest->progress_question = 0;
            $applicantPsikotest->progress_pk = 'p';
            $applicantPsikotest->p_d = 0;
            $applicantPsikotest->p_i = 0;
            $applicantPsikotest->p_s = 0;
            $applicantPsikotest->p_c = 0;
            $applicantPsikotest->p_star = 0;
            $applicantPsikotest->k_d = 0;
            $applicantPsikotest->k_i = 0;
            $applicantPsikotest->k_s = 0;
            $applicantPsikotest->k_c = 0;
            $applicantPsikotest->k_star = 0;
            $applicantPsikotest->save();

            return view('pelamar/do-psikotest', ['psikotest' => $psikotest, 'psikotestQuestionNow' => $psikotestQuestionNow, 'progressPK' => 'p', 'lastAnswer' => null, 'randString' => $this->randString()]);
        } else {
            if ($request->is_refreshed == $request->session()->get('sess_key_psikotest')) {
                return redirect('/failed-psikotest');
            } else {
                $request->session()->put('sess_key_psikotest', $request->is_refreshed);
                if ($applicantPsikotest->progress_pk == 'p') {
                    if (explode('|', $request->psikotestRadio)[1] == 'D') {
                        $applicantPsikotest->p_d = $applicantPsikotest->p_d + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'I') {
                        $applicantPsikotest->p_i = $applicantPsikotest->p_i + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'S') {
                        $applicantPsikotest->p_s = $applicantPsikotest->p_s + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'C') {
                        $applicantPsikotest->p_c = $applicantPsikotest->p_c + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == '*') {
                        $applicantPsikotest->p_star = $applicantPsikotest->p_star + 1;
                    }

                    $applicantPsikotest->progress_pk = 'k';
                    $applicantPsikotest->save();
                } elseif ($applicantPsikotest->progress_pk == 'k') {
                    if (explode('|', $request->psikotestRadio)[1] == 'D') {
                        $applicantPsikotest->k_d = $applicantPsikotest->k_d + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'I') {
                        $applicantPsikotest->k_i = $applicantPsikotest->k_i + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'S') {
                        $applicantPsikotest->k_s = $applicantPsikotest->k_s + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == 'C') {
                        $applicantPsikotest->k_c = $applicantPsikotest->k_c + 1;
                    } elseif (explode('|', $request->psikotestRadio)[1] == '*') {
                        $applicantPsikotest->k_star = $applicantPsikotest->k_star + 1;
                    }

                    $applicantPsikotest->progress_question = $applicantPsikotest->progress_question + 1;
                    $applicantPsikotest->progress_pk = 'p';
                    $applicantPsikotest->save();

                    if ($applicantPsikotest->progress_question == count($psikotest->psikotestQuestion)) {
                        $applicantPsikotest->pk_d = $applicantPsikotest->p_d - $applicantPsikotest->k_d;
                        $applicantPsikotest->pk_i = $applicantPsikotest->p_i - $applicantPsikotest->k_i;
                        $applicantPsikotest->pk_s = $applicantPsikotest->p_s - $applicantPsikotest->k_s;
                        $applicantPsikotest->pk_c = $applicantPsikotest->p_c - $applicantPsikotest->k_c;
                        $applicantPsikotest->status = 'done';
                        $applicantPsikotest->save();

                        // Logika Perhitungan DISC
                        $this->calculateDISC($applicantPsikotest);

                        $request->session()->forget('sess_key_psikotest');
                        return redirect('/my-dashboard')->with(['msg' => 'done-psikotest']);
                    }
                }

                $psikotestQuestionNow = $psikotest->psikotestQuestion[$applicantPsikotest->progress_question];

                return view('pelamar/do-psikotest', ['psikotest' => $psikotest, 'psikotestQuestionNow' => $psikotestQuestionNow, 'progressPK' => $applicantPsikotest->progress_pk, 'lastAnswer' => explode('|', $request->psikotestRadio)[0], 'randString' => $this->randString()]);
            }
        }
    }

    function doTest(Request $request)
    {
        $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'keterampilan'])->first();
        $applicantTest = ApplicantTest::with('test')->where('application_id', $application->id)->first();
        $test = Test::with('testQuestion')->find($applicantTest->test_id);

        if ($request->method() == 'GET') {
            $testQuestionNow = $test->testQuestion[0];
            $applicantTest->status = 'started';
            $applicantTest->progress_question = '';
            $applicantTest->score = 0;
            $applicantTest->save();

            return view('pelamar/do-test', ['tests' => $test, 'testQuestionNow' => $testQuestionNow]);
        } else {
            if ($request->has('change_question')) {
                $question = TestQuestion::find($request->question_id);

                return view('pelamar/do-test', ['tests' => $test, 'testQuestionNow' => $question]);
            }

            //$answer = $request->session()->get('answer_test');
            //$answer[$request->question_id] = $request->testRadio;
            //$request->session()->put('answer_test', $answer);
            $applicantTest->progress_question = $applicantTest->progress_question . $request->question_id . "=" . $request->testRadio . ",";
            $applicantTest->save();
            $answer = [];
            $answerDB = substr($applicantTest->progress_question, 0, -1);
            foreach(explode(",", $answerDB) as $key => $value) {
                $i = explode("=", $value);
                $answer[$i[0]] = $i[1];
            }
            if (count($answer) == count($test->testQuestion)) {
                $totalQuestion = count($test->testQuestion);
                $rightAnswer = 0;

                foreach ($answer as $key => $value) {
                    $question = TestQuestion::find($key);
                    if ($value == $question->answer) {
                        $rightAnswer += 1;
                    }
                }

                $score = (int)(($rightAnswer / $totalQuestion) * 100);
                $applicantTest->score = $score;
                if ($test->kkm <= $score) {
                    $applicantTest->status = 'done';
                } else {
                    $applicantTest->status = 'fail';
                }
                $applicantTest->save();

                return redirect('/my-dashboard')->with(['msg' => 'done-test']);
            }

            $nextQuestion = null;
            foreach ($test->testQuestion as $index => $testQuestion) {
                if ($testQuestion->id == $request->question_id) {
                    $nextQuestion = $test->testQuestion[++$index];
                }
            }

            return view('pelamar/do-test', ['tests' => $test, 'testQuestionNow' => $nextQuestion]);
        }
    }

    function timePsikotest(Request $request)
    {
        $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'psikotest'])->first();
        $applicantPsikotest = ApplicantPsikotest::with('psikotest')->where('application_id', $application->id)->first();

        $time = $applicantPsikotest->psikotest->time * 60;
        if ($request->session()->get('time_psikotest') == null) {
            $request->session()->put('time_psikotest', time());
        } else {
            $diff = time() - $request->session()->get('time_psikotest');
            $diff = $time - $diff;

            // return response()->json(['time' => ]);
            $hours = (floor($diff / 3600) < 10) ? '0' . floor($diff / 3600) : floor($diff / 3600);
            $minutes = ((int)(($diff % 3600) / 60) < 10) ? '0' . (int)(($diff % 3600) / 60) : (int)(($diff % 3600) / 60);
            $seconds = (($diff % 60) < 10) ? '0' . ($diff % 60) : ($diff % 60);

            $dateDiff = $hours . ":" . $minutes . ":" . $seconds;

            if ($diff == 0 || $diff < 0) {
                return response()->json(['time' => 'timeout']);
            } else {
                return response()->json(['time' => $dateDiff]);
            }
        }
    }

    function timeTest(Request $request)
    {
        $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'keterampilan'])->first();
        $applicantTest = ApplicantTest::with('test')->where('application_id', $application->id)->first();

        $time = $applicantTest->test->time * 60;

        if ($request->session()->get('time_test') == null) {
            $request->session()->put('time_test', time());
        } else {
            $diff = time() - $request->session()->get('time_test');
            $diff = $time - $diff;

            // return response()->json(['time' => ]);
            $hours = (floor($diff / 3600) < 10) ? '0' . floor($diff / 3600) : floor($diff / 3600);
            $minutes = ((int)(($diff % 3600) / 60) < 10) ? '0' . (int)(($diff % 3600) / 60) : (int)(($diff % 3600) / 60);
            $seconds = (($diff % 60) < 10) ? '0' . ($diff % 60) : ($diff % 60);

            $dateDiff = $hours . ":" . $minutes . ":" . $seconds;

            if ($diff == 0 || $diff < 0) {
                return response()->json(['time' => 'timeout']);
            } else {
                return response()->json(['time' => $dateDiff]);
            }
        }
    }

    function failedPsikotest(Request $request)
    {
        if ($request->session()->get('time_psikotest') != null) {
            $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'psikotest'])->first();
            $applicantPsikotest = ApplicantPsikotest::with('psikotest')->where('application_id', $application->id)->first();

            $applicantPsikotest->status = 'fail';
            $applicantPsikotest->save();

            return redirect('/my-dashboard')->with(['msg' => 'cancel-test']);
        } else {
            return redirect('/my-dashboard');
        }
    }

    function failedTest(Request $request)
    {
        if ($request->session()->get('time_test') != null) {
            $application = Application::where(['applicant_id' => Auth::id(), 'status' => 'keterampilan'])->first();
            $applicantTest = ApplicantTest::with('test')->where('application_id', $application->id)->first();

            $applicantTest->status = 'fail';
            $applicantTest->save();

            return redirect('/my-dashboard')->with(['msg' => 'cancel-psikotest']);
        } else {
            return redirect('/my-dashboard');
        }
    }

    function calculateDISC(ApplicantPsikotest $applicantPsikotest)
    {
        // Mask Public Self (luar)
        $pd = PsikotestP::where('key', $applicantPsikotest->p_d)->first()->d;
        $pi = PsikotestP::where('key', $applicantPsikotest->p_i)->first()->i;
        $ps = PsikotestP::where('key', $applicantPsikotest->p_s)->first()->s;
        $pc = PsikotestP::where('key', $applicantPsikotest->p_c)->first()->c;

        // Core Private Self (Dalam)
        $kd = PsikotestK::where('key', $applicantPsikotest->k_d)->first()->d;
        $ki = PsikotestK::where('key', $applicantPsikotest->k_i)->first()->i;
        $ks = PsikotestK::where('key', $applicantPsikotest->k_s)->first()->s;
        $kc = PsikotestK::where('key', $applicantPsikotest->k_c)->first()->c;

        // Mirror Perceived Self
        $pkd = PsikotestPK::where('key', $applicantPsikotest->pk_d)->first()->d;
        $pki = PsikotestPK::where('key', $applicantPsikotest->pk_i)->first()->i;
        $pks = PsikotestPK::where('key', $applicantPsikotest->pk_s)->first()->s;
        $pkc = PsikotestPK::where('key', $applicantPsikotest->pk_c)->first()->c;

        // Pencarian Mask Public Self (luar)
        if ($pd <= 0 && $pi <= 0 && $ps <= 0 && $pc > 0) {
            // Logical Thinker (C)
            $applicantPsikotest->result_p = 1;
        } elseif ($pd > 0 && $pi <= 0 && $ps <= 0 && $pc <= 0) {
            // Establisher (D)
            $applicantPsikotest->result_p = 2;
        } elseif ($pd > 0 && $pi <= 0 && $ps <= 0 && $pc > 0 && $pc >= $pd) {
            // Designer (C-D)
            $applicantPsikotest->result_p = 3;
        } elseif ($pd > 0 && $pi > 0 && $ps <= 0 && $pc <= 0 && $pi >= $pd) {
            // Negotiator (I-D)
            $applicantPsikotest->result_p = 4;
        } elseif ($pd > 0 && $pi > 0 && $ps <= 0 && $pc > 0 && $pi >= $pd && $pd >= $pc) {
            // Confident & Determined (I-D-C)
            $applicantPsikotest->result_p = 5;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $pi >= $pd && $pd >= $ps) {
            // Reformer (I-D-S)
            $applicantPsikotest->result_p = 6;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $pi >= $ps && $ps >= $pd) {
            // Motivator (I-S-D)
            $applicantPsikotest->result_p = 7;
        } elseif ($pd > 0 && $pi <= 0 && $ps > 0 && $pc >= 0 && $pi >= $ps && $ps >= $pd) {
            // Inquirer (S-D-C)
            $applicantPsikotest->result_p = 8;
        } elseif ($pd > 0 && $pi > 0 && $ps <= 0 && $pc <= 0 && $pd >= $pi) {
            // Pengambilan Keputusan (D-I)
            $applicantPsikotest->result_p = 9;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $pd >= $pi && $pi >= $ps) {
            // Director (D-I-S)
            $applicantPsikotest->result_p = 20;
        } elseif ($pd > 0 && $pi <= 0 && $ps > 0 && $pc <= 0 && $pd >= $ps) {
            // Self-Motivated (D-S)
            $applicantPsikotest->result_p = 11;
        } elseif ($pd <= 0 && $pi > 0 && $ps > 0 && $pc > 0 && $pc >= $pi && $pi >= $ps) {
            // Mediator (C-I-S)
            $applicantPsikotest->result_p = 12;
        } elseif ($pd <= 0 && $pi > 0 && $ps > 0 && $pc > 0 && $pc >= $pi && $pi >= $ps) {
            // Practitioner (C-S-I)
            $applicantPsikotest->result_p = 13;
        } elseif ($pd <= 0 && $pi > 0 && $ps > 0 && $pc > 0 && $ps >= $pi && $ps >= $pc) {
            // Responsive & Thoughtful (I-S-C / I-C-S)
            $applicantPsikotest->result_p = 14;
        } elseif ($pd <= 0 && $pi <= 0 && $ps > 0 && $pc <= 0) {
            // Specialist (S)
            $applicantPsikotest->result_p = 15;
        } elseif ($pd <= 0 && $pi <= 0 && $ps > 0 && $pc > 0 && $pc >= $ps) {
            // Perfectionist (C-S)
            $applicantPsikotest->result_p = 16;
        } elseif ($pd <= 0 && $pi <= 0 && $ps > 0 && $pc > 0 && $ps >= $pc) {
            // Peacemaker, Respectfull & Accurat (S-C)
            $applicantPsikotest->result_p = 17;
        } elseif ($pi <= 0 && $ps <= 0 && $pd > 0 && $pc > 0 && $pd >= $pc) {
            // Challenger (D-C)
            $applicantPsikotest->result_p = 18;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $pd >= $pi && $pi >= $pc) {
            // Chancelor (D-I-C)
            $applicantPsikotest->result_p = 19;
        } elseif ($pd > 0 && $ps > 0 && $pi > 0 && $pc <= 0 && $pd >= $ps && $ps >= $pi) {
            // Director (D-S-I)
            $applicantPsikotest->result_p = 20;
        } elseif ($pd > 0 && $ps > 0 && $pc > 0 && $pi <= 0 && $pd >= $ps && $ps >= $pc) {
            // Director (D-S-C)
            $applicantPsikotest->result_p = 20;
        } elseif ($pd > 0 && $pi > 0 && $pc > 0 && $ps <= 0 && $pd >= $pc && $pc >= $pi) {
            // Challenger (D-C-I)
            $applicantPsikotest->result_p = 18;
        } elseif ($pd > 0 && $ps > 0 && $pc > 0 && $pi <= 0 && $pd >= $pc && $pc >= $ps) {
            // Challenger (D-C-S)
            $applicantPsikotest->result_p = 18;
        } elseif ($pd <= 0 && $ps <= 0 && $pc <= 0 && $pi > 0) {
            // Communicator (I)
            $applicantPsikotest->result_p = 21;
        } elseif ($pi > 0 && $ps > 0 && $pd <= 0 && $pc <= 0 && $pi >= $ps) {
            // Advisor (I-S)
            $applicantPsikotest->result_p = 22;
        } elseif ($pi > 0 && $pc > 0 && $pd <= 0 && $ps <= 0 && $pi >= $pc) {
            // Assessor (I-C)
            $applicantPsikotest->result_p = 23;
        } elseif ($pd > 0 && $pi > 0 && $pc > 0 && $ps <= 0 && $pi >= $pc && $pc >= $pd) {
            // Assessor (I-C-D)
            $applicantPsikotest->result_p = 23;
        } elseif ($pd <= 0 && $pi > 0 && $pc > 0 && $ps > 0 && $pi >= $pc && $pc >= $ps) {
            // Responsive & Thoughtful (I-C-S)
            $applicantPsikotest->result_p = 14;
        } elseif ($pd > 0 && $pi <= 0 && $ps > 0 && $pc <= 0 && $ps >= $pd) {
            // Self-Motivated (S-D)
            $applicantPsikotest->result_p = 11;
        } elseif ($pi > 0 && $ps > 0 && $pd <= 0 && $pc <= 0 && $ps >= $pi) {
            // Advisor (S-I)
            $applicantPsikotest->result_p = 22;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $ps >= $pd && $pd >= $pi) {
            // Director (S-D-I)
            $applicantPsikotest->result_p = 20;
        } elseif ($pd > 0 && $pi > 0 && $ps > 0 && $pc <= 0 && $ps >= $pi && $pi >= $pd) {
            // Advisor (S-I-D)
            $applicantPsikotest->result_p = 22;
        } elseif ($pi > 0 && $ps > 0 && $pc > 0 && $pd <= 0 && $ps >= $pi && $pi >= $pc) {
            // Advocate (S-I-C)
            $applicantPsikotest->result_p = 24;
        } elseif ($pd > 0 && $pi <= 0 && $ps > 0 && $pc > 0 && $ps >= $pc && $pc >= $pd) {
            // Inquirer (S-C-D)
            $applicantPsikotest->result_p = 8;
        } elseif ($pi > 0 && $ps > 0 && $pc > 0 && $pd <= 0 && $ps >= $pc && $pc >= $pi) {
            // Advocate (S-C-I)
            $applicantPsikotest->result_p = 24;
        } elseif ($pi > 0 && $pc > 0 && $pd <= 0 && $ps <= 0 && $pc >= $pi) {
            // Assessor (C-I)
            $applicantPsikotest->result_p = 23;
        } elseif ($pd > 0 && $pi > 0 && $pc > 0 && $ps <= 0 && $pc >= $pd && $pd >= $pi) {
            // Challenger (C-D-I)
            $applicantPsikotest->result_p = 18;
        } elseif ($pd > 0 && $ps > 0 && $pc > 0 && $pi <= 0 && $pc >= $pd && $pd >= $ps) {
            // Contemplator (C-D-S)
            $applicantPsikotest->result_p = 25;
        } elseif ($pd > 0 && $pi > 0 && $pc > 0 && $ps <= 0 && $pc >= $pi && $pi >= $pd) {
            // Assessor (C-I-D)
            $applicantPsikotest->result_p = 23;
        } elseif ($pd > 0 && $ps > 0 && $pc > 0 && $pi <= 0 && $pc >= $ps && $ps >= $pd) {
            // Precisionist (C-S-D)
            $applicantPsikotest->result_p = 26;
        }

        // Pencarian Core Private Self (Dalam)
        if ($kd <= 0 && $ki <= 0 && $ks <= 0 && $kc > 0) {
            // Logical Thinker (C)
            $applicantPsikotest->result_k = 1;
        } elseif ($kd > 0 && $ki <= 0 && $ks <= 0 && $kc <= 0) {
            // Establisher (D)
            $applicantPsikotest->result_k = 2;
        } elseif ($kd > 0 && $ki <= 0 && $ks <= 0 && $kc > 0 && $kc >= $kd) {
            // Designer (C-D)
            $applicantPsikotest->result_k = 3;
        } elseif ($kd > 0 && $ki > 0 && $ks <= 0 && $kc <= 0 && $ki >= $kd) {
            // Negotiator (I-D)
            $applicantPsikotest->result_k = 4;
        } elseif ($kd > 0 && $ki > 0 && $ks <= 0 && $kc > 0 && $ki >= $kd && $kd >= $kc) {
            // Confident & Determined (I-D-C)
            $applicantPsikotest->result_k = 5;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $ki >= $kd && $kd >= $ks) {
            // Reformer (I-D-S)
            $applicantPsikotest->result_k = 6;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $ki >= $ks && $ks >= $kd) {
            // Motivator (I-S-D)
            $applicantPsikotest->result_k = 7;
        } elseif ($kd > 0 && $ki <= 0 && $ks > 0 && $kc >= 0 && $ki >= $ks && $ks >= $kd) {
            // Inquirer (S-D-C)
            $applicantPsikotest->result_k = 8;
        } elseif ($kd > 0 && $ki > 0 && $ks <= 0 && $kc <= 0 && $kd >= $ki) {
            // Pengambilan Keputusan (D-I)
            $applicantPsikotest->result_k = 9;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $kd >= $ki && $ki >= $ks) {
            // Director (D-I-S)
            $applicantPsikotest->result_k = 20;
        } elseif ($kd > 0 && $ki <= 0 && $ks > 0 && $kc <= 0 && $kd >= $ks) {
            // Self-Motivated (D-S)
            $applicantPsikotest->result_k = 11;
        } elseif ($kd <= 0 && $ki > 0 && $ks > 0 && $kc > 0 && $kc >= $ki && $ki >= $ks) {
            // Mediator (C-I-S)
            $applicantPsikotest->result_k = 12;
        } elseif ($kd <= 0 && $ki > 0 && $ks > 0 && $kc > 0 && $kc >= $ki && $ki >= $ks) {
            // Practitioner (C-S-I)
            $applicantPsikotest->result_k = 13;
        } elseif ($kd <= 0 && $ki > 0 && $ks > 0 && $kc > 0 && $ks >= $ki && $ks >= $kc) {
            // Responsive & Thoughtful (I-S-C / I-C-S)
            $applicantPsikotest->result_k = 14;
        } elseif ($kd <= 0 && $ki <= 0 && $ks > 0 && $kc <= 0) {
            // Specialist (S)
            $applicantPsikotest->result_k = 15;
        } elseif ($kd <= 0 && $ki <= 0 && $ks > 0 && $kc > 0 && $kc >= $ks) {
            // Perfectionist (C-S)
            $applicantPsikotest->result_k = 16;
        } elseif ($kd <= 0 && $ki <= 0 && $ks > 0 && $kc > 0 && $ks >= $kc) {
            // Peacemaker, Respectfull & Accurat (S-C)
            $applicantPsikotest->result_k = 17;
        } elseif ($ki <= 0 && $ks <= 0 && $kd > 0 && $kc > 0 && $kd >= $kc) {
            // Challenger (D-C)
            $applicantPsikotest->result_k = 18;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $kd >= $ki && $ki >= $kc) {
            // Chancelor (D-I-C)
            $applicantPsikotest->result_k = 19;
        } elseif ($kd > 0 && $ks > 0 && $ki > 0 && $kc <= 0 && $kd >= $ks && $ks >= $ki) {
            // Director (D-S-I)
            $applicantPsikotest->result_k = 20;
        } elseif ($kd > 0 && $ks > 0 && $kc > 0 && $ki <= 0 && $kd >= $ks && $ks >= $kc) {
            // Director (D-S-C)
            $applicantPsikotest->result_k = 20;
        } elseif ($kd > 0 && $ki > 0 && $kc > 0 && $ks <= 0 && $kd >= $kc && $kc >= $ki) {
            // Challenger (D-C-I)
            $applicantPsikotest->result_k = 18;
        } elseif ($kd > 0 && $ks > 0 && $kc > 0 && $ki <= 0 && $kd >= $kc && $kc >= $ks) {
            // Challenger (D-C-S)
            $applicantPsikotest->result_k = 18;
        } elseif ($kd <= 0 && $ks <= 0 && $kc <= 0 && $ki > 0) {
            // Communicator (I)
            $applicantPsikotest->result_k = 21;
        } elseif ($ki > 0 && $ks > 0 && $kd <= 0 && $kc <= 0 && $ki >= $ks) {
            // Advisor (I-S)
            $applicantPsikotest->result_k = 22;
        } elseif ($ki > 0 && $kc > 0 && $kd <= 0 && $ks <= 0 && $ki >= $kc) {
            // Assessor (I-C)
            $applicantPsikotest->result_k = 23;
        } elseif ($kd > 0 && $ki > 0 && $kc > 0 && $ks <= 0 && $ki >= $kc && $kc >= $kd) {
            // Assessor (I-C-D)
            $applicantPsikotest->result_k = 23;
        } elseif ($kd <= 0 && $ki > 0 && $kc > 0 && $ks > 0 && $ki >= $kc && $kc >= $ks) {
            // Responsive & Thoughtful (I-C-S)
            $applicantPsikotest->result_k = 14;
        } elseif ($kd > 0 && $ki <= 0 && $ks > 0 && $kc <= 0 && $ks >= $kd) {
            // Self-Motivated (S-D)
            $applicantPsikotest->result_k = 11;
        } elseif ($ki > 0 && $ks > 0 && $kd <= 0 && $kc <= 0 && $ks >= $ki) {
            // Advisor (S-I)
            $applicantPsikotest->result_k = 22;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $ks >= $kd && $kd >= $ki) {
            // Director (S-D-I)
            $applicantPsikotest->result_k = 20;
        } elseif ($kd > 0 && $ki > 0 && $ks > 0 && $kc <= 0 && $ks >= $ki && $ki >= $kd) {
            // Advisor (S-I-D)
            $applicantPsikotest->result_k = 22;
        } elseif ($ki > 0 && $ks > 0 && $kc > 0 && $kd <= 0 && $ks >= $ki && $ki >= $kc) {
            // Advocate (S-I-C)
            $applicantPsikotest->result_k = 24;
        } elseif ($kd > 0 && $ki <= 0 && $ks > 0 && $kc > 0 && $ks >= $kc && $kc >= $kd) {
            // Inquirer (S-C-D)
            $applicantPsikotest->result_k = 8;
        } elseif ($ki > 0 && $ks > 0 && $kc > 0 && $kd <= 0 && $ks >= $kc && $kc >= $ki) {
            // Advocate (S-C-I)
            $applicantPsikotest->result_k = 24;
        } elseif ($ki > 0 && $kc > 0 && $kd <= 0 && $ks <= 0 && $kc >= $ki) {
            // Assessor (C-I)
            $applicantPsikotest->result_k = 23;
        } elseif ($kd > 0 && $ki > 0 && $kc > 0 && $ks <= 0 && $kc >= $kd && $kd >= $ki) {
            // Challenger (C-D-I)
            $applicantPsikotest->result_k = 18;
        } elseif ($kd > 0 && $ks > 0 && $kc > 0 && $ki <= 0 && $kc >= $kd && $kd >= $ks) {
            // Contemplator (C-D-S)
            $applicantPsikotest->result_k = 25;
        } elseif ($kd > 0 && $ki > 0 && $kc > 0 && $ks <= 0 && $kc >= $ki && $ki >= $kd) {
            // Assessor (C-I-D)
            $applicantPsikotest->result_k = 23;
        } elseif ($kd > 0 && $ks > 0 && $kc > 0 && $ki <= 0 && $kc >= $ks && $ks >= $kd) {
            // Precisionist (C-S-D)
            $applicantPsikotest->result_k = 26;
        }

        // Pencarian Mirror Perceived Self
        if ($pkd <= 0 && $pki <= 0 && $pks <= 0 && $pkc > 0) {
            // Logical Thinker (C)
            $applicantPsikotest->result_pk = 1;
        } elseif ($pkd > 0 && $pki <= 0 && $pks <= 0 && $pkc <= 0) {
            // Establisher (D)
            $applicantPsikotest->result_pk = 2;
        } elseif ($pkd > 0 && $pki <= 0 && $pks <= 0 && $pkc > 0 && $pkc >= $pkd) {
            // Designer (C-D)
            $applicantPsikotest->result_pk = 3;
        } elseif ($pkd > 0 && $pki > 0 && $pks <= 0 && $pkc <= 0 && $pki >= $pkd) {
            // Negotiator (I-D)
            $applicantPsikotest->result_pk = 4;
        } elseif ($pkd > 0 && $pki > 0 && $pks <= 0 && $pkc > 0 && $pki >= $pkd && $pkd >= $pkc) {
            // Confident & Determined (I-D-C)
            $applicantPsikotest->result_pk = 5;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pki >= $pkd && $pkd >= $pks) {
            // Reformer (I-D-S)
            $applicantPsikotest->result_pk = 6;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pki >= $pks && $pks >= $pkd) {
            // Motivator (I-S-D)
            $applicantPsikotest->result_pk = 7;
        } elseif ($pkd > 0 && $pki <= 0 && $pks > 0 && $pkc >= 0 && $pki >= $pks && $pks >= $pkd) {
            // Inquirer (S-D-C)
            $applicantPsikotest->result_pk = 8;
        } elseif ($pkd > 0 && $pki > 0 && $pks <= 0 && $pkc <= 0 && $pkd >= $pki) {
            // Pengambilan Keputusan (D-I)
            $applicantPsikotest->result_pk = 9;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pkd >= $pki && $pki >= $pks) {
            // Director (D-I-S)
            $applicantPsikotest->result_pk = 20;
        } elseif ($pkd > 0 && $pki <= 0 && $pks > 0 && $pkc <= 0 && $pkd >= $pks) {
            // Self-Motivated (D-S)
            $applicantPsikotest->result_pk = 11;
        } elseif ($pkd <= 0 && $pki > 0 && $pks > 0 && $pkc > 0 && $pkc >= $pki && $pki >= $pks) {
            // Mediator (C-I-S)
            $applicantPsikotest->result_pk = 12;
        } elseif ($pkd <= 0 && $pki > 0 && $pks > 0 && $pkc > 0 && $pkc >= $pki && $pki >= $pks) {
            // Practitioner (C-S-I)
            $applicantPsikotest->result_pk = 13;
        } elseif ($pkd <= 0 && $pki > 0 && $pks > 0 && $pkc > 0 && $pks >= $pki && $pks >= $pkc) {
            // Responsive & Thoughtful (I-S-C / I-C-S)
            $applicantPsikotest->result_pk = 14;
        } elseif ($pkd <= 0 && $pki <= 0 && $pks > 0 && $pkc <= 0) {
            // Specialist (S)
            $applicantPsikotest->result_pk = 15;
        } elseif ($pkd <= 0 && $pki <= 0 && $pks > 0 && $pkc > 0 && $pkc >= $pks) {
            // Perfectionist (C-S)
            $applicantPsikotest->result_pk = 16;
        } elseif ($pkd <= 0 && $pki <= 0 && $pks > 0 && $pkc > 0 && $pks >= $pkc) {
            // Peacemaker, Respectfull & Accurat (S-C)
            $applicantPsikotest->result_pk = 17;
        } elseif ($pki <= 0 && $pks <= 0 && $pkd > 0 && $pkc > 0 && $pkd >= $pkc) {
            // Challenger (D-C)
            $applicantPsikotest->result_pk = 18;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pkd >= $pki && $pki >= $pkc) {
            // Chancelor (D-I-C)
            $applicantPsikotest->result_pk = 19;
        } elseif ($pkd > 0 && $pks > 0 && $pki > 0 && $pkc <= 0 && $pkd >= $pks && $pks >= $pki) {
            // Director (D-S-I)
            $applicantPsikotest->result_pk = 20;
        } elseif ($pkd > 0 && $pks > 0 && $pkc > 0 && $pki <= 0 && $pkd >= $pks && $pks >= $pkc) {
            // Director (D-S-C)
            $applicantPsikotest->result_pk = 20;
        } elseif ($pkd > 0 && $pki > 0 && $pkc > 0 && $pks <= 0 && $pkd >= $pkc && $pkc >= $pki) {
            // Challenger (D-C-I)
            $applicantPsikotest->result_pk = 18;
        } elseif ($pkd > 0 && $pks > 0 && $pkc > 0 && $pki <= 0 && $pkd >= $pkc && $pkc >= $pks) {
            // Challenger (D-C-S)
            $applicantPsikotest->result_pk = 18;
        } elseif ($pkd <= 0 && $pks <= 0 && $pkc <= 0 && $pki > 0) {
            // Communicator (I)
            $applicantPsikotest->result_pk = 21;
        } elseif ($pki > 0 && $pks > 0 && $pkd <= 0 && $pkc <= 0 && $pki >= $pks) {
            // Advisor (I-S)
            $applicantPsikotest->result_pk = 22;
        } elseif ($pki > 0 && $pkc > 0 && $pkd <= 0 && $pks <= 0 && $pki >= $pkc) {
            // Assessor (I-C)
            $applicantPsikotest->result_pk = 23;
        } elseif ($pkd > 0 && $pki > 0 && $pkc > 0 && $pks <= 0 && $pki >= $pkc && $pkc >= $pkd) {
            // Assessor (I-C-D)
            $applicantPsikotest->result_pk = 23;
        } elseif ($pkd <= 0 && $pki > 0 && $pkc > 0 && $pks > 0 && $pki >= $pkc && $pkc >= $pks) {
            // Responsive & Thoughtful (I-C-S)
            $applicantPsikotest->result_pk = 14;
        } elseif ($pkd > 0 && $pki <= 0 && $pks > 0 && $pkc <= 0 && $pks >= $pkd) {
            // Self-Motivated (S-D)
            $applicantPsikotest->result_pk = 11;
        } elseif ($pki > 0 && $pks > 0 && $pkd <= 0 && $pkc <= 0 && $pks >= $pki) {
            // Advisor (S-I)
            $applicantPsikotest->result_pk = 22;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pks >= $pkd && $pkd >= $pki) {
            // Director (S-D-I)
            $applicantPsikotest->result_pk = 20;
        } elseif ($pkd > 0 && $pki > 0 && $pks > 0 && $pkc <= 0 && $pks >= $pki && $pki >= $pkd) {
            // Advisor (S-I-D)
            $applicantPsikotest->result_pk = 22;
        } elseif ($pki > 0 && $pks > 0 && $pkc > 0 && $pkd <= 0 && $pks >= $pki && $pki >= $pkc) {
            // Advocate (S-I-C)
            $applicantPsikotest->result_pk = 24;
        } elseif ($pkd > 0 && $pki <= 0 && $pks > 0 && $pkc > 0 && $pks >= $pkc && $pkc >= $pkd) {
            // Inquirer (S-C-D)
            $applicantPsikotest->result_pk = 8;
        } elseif ($pki > 0 && $pks > 0 && $pkc > 0 && $pkd <= 0 && $pks >= $pkc && $pkc >= $pki) {
            // Advocate (S-C-I)
            $applicantPsikotest->result_pk = 24;
        } elseif ($pki > 0 && $pkc > 0 && $pkd <= 0 && $pks <= 0 && $pkc >= $pki) {
            // Assessor (C-I)
            $applicantPsikotest->result_pk = 23;
        } elseif ($pkd > 0 && $pki > 0 && $pkc > 0 && $pks <= 0 && $pkc >= $pkd && $pkd >= $pki) {
            // Challenger (C-D-I)
            $applicantPsikotest->result_pk = 18;
        } elseif ($pkd > 0 && $pks > 0 && $pkc > 0 && $pki <= 0 && $pkc >= $pkd && $pkd >= $pks) {
            // Contemplator (C-D-S)
            $applicantPsikotest->result_pk = 25;
        } elseif ($pkd > 0 && $pki > 0 && $pkc > 0 && $pks <= 0 && $pkc >= $pki && $pki >= $pkd) {
            // Assessor (C-I-D)
            $applicantPsikotest->result_pk = 23;
        } elseif ($pkd > 0 && $pks > 0 && $pkc > 0 && $pki <= 0 && $pkc >= $pks && $pks >= $pkd) {
            // Precisionist (C-S-D)
            $applicantPsikotest->result_pk = 26;
        }

        $applicantPsikotest->save();
    }

    function randString()
    {
        $characters = '0123456789abcdefghijkmnopqrstuvwxyABCDEFGHIJKMNOPQRSTUVWXY';
        $randstring = '';
        for ($i = 0; $i < 8; $i++) {
            $randstring = $randstring . $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }
}
