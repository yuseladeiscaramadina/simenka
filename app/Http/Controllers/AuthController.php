<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPasswordEmail;
use App\Models\Applicant;
use App\Models\Hrd;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::guest()) {
                Redirect::to('/')->send();
            }
            return $next($request);
        });
    }
    public function login(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/login');
        } else {
            $pelamar = Applicant::where('email', $request->email)->first();
            $hrd = Hrd::where('email', $request->email)->first();
            if ($pelamar != null) {
                if ($pelamar->password == '0') {
                    return Redirect::back()->with(['msg' => 'Anda belum membuat Password! Silahkan login melalui Akun Google', 'type' => 'danger']);
                }

                if (Hash::check($request->password, $pelamar->password)) {
                    $pelamar->date_login = Carbon::now()->toDateString();
                    $pelamar->save();

                    Auth::login($pelamar, true);
                    return redirect('/');
                } else {
                    return Redirect::back()->with(['msg' => 'Password yang anda masukkan salah', 'type' => 'warning']);
                }
            }

            if ($hrd != null) {

                if (Hash::check($request->password, $hrd->password)) {
                    $hrd->date_login = Carbon::now()->toDateString();
                    $hrd->save();

                    $request->session()->put('hrd_id', $hrd->id);
                    $request->session()->put('name', $hrd->name);
                    $request->session()->put('email', $hrd->email);
                    $request->session()->put('password', $hrd->password);
                    $request->session()->put('profile_picture', $hrd->profile_picture);
                    $request->session()->put('token', $hrd->token);
                    $request->session()->put('date_login', $hrd->date_login);

                    return redirect('/hrd/dashboard');
                } else {
                    return Redirect::back()->with(['msg' => 'Password yang anda masukkan salah', 'type' => 'warning']);
                }
            }

            return Redirect::back()->with(['msg' => 'Email anda belum terdaftar, silahkan daftar terlebih dahulu', 'type' => 'warning']);
        }
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
    public function loginFromGoogle()
    {
        $user_google = Socialite::driver('google')->user();
        try {
            $user = Applicant::where('email', $user_google->getEmail())->first();

            if ($user != null) {
                $user->date_login = Carbon::now()->toDateString();
                $user->save();
                Auth::login($user, true);
                return redirect()->route('beranda');
            } else {
                $token = $this->generateRandomToken();
                $user = Applicant::create([
                    'name' => $user_google->getName(),
                    'email' => $user_google->getEmail(),
                    'password' => 0,
                    'profile_picture' =>  "dashboard/images/user/default_avatar.png",
                    'token' => $token,
                    'date_login' => Carbon::now()->toDateString()
                ]);

                Auth::login($user, true);
                return redirect()->route('beranda');
            }
        } catch (Exception $e) {
            return redirect('/masuk')->with(['msg' => 'Terjadi kesalahan saat login, silahkan menunggu sesaat lagi.', 'type' => 'warning']);
        }
    }

    public function signup(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/signup');
        } else {
            $token = $this->generateRandomToken();

            $user = Applicant::where('email', $request->email)->first();

            if ($user == null) {
                $user = Applicant::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'profile_picture' =>  "dashboard/images/user/default_avatar.png",
                    'token' => $token,
                    'date_login' => Carbon::now()->toDateString(),
                ]);

                Auth::login($user, true);
                return redirect()->route('beranda');
            } else {
                return Redirect::back()->with(['msg' => 'Email anda telah terdaftar, silahkan login menggunakan akun anda', 'type' => 'warning']);
            }
        }
    }

    public function forgotPassword(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/forgot-password');
        } else {
            $user = Applicant::where('email', $request->email)->first();

            if ($user == null) {
                $user = Hrd::where('email', $request->email)->first();
            }

            if ($user == null) {
                return Redirect::back()->with(['msg' => 'Email anda tidak terdaftar!', 'type' => 'warning']);
            } else {
                Mail::to($request->email)->send(new ForgotPasswordEmail($user->name, $user->token));

                if (Mail::failures()) {
                    return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
                }
                return redirect()->back()->with(['msg' => 'Email pemulihan password berhasil dikirim, silahkan periksa email anda!', 'type' => 'success']);
            }
        }
    }

    public function resetPassword($code, Request $request)
    {
        $user = Applicant::where('token', $code)->first();
        if ($user == null) {
            $user = Hrd::where('token', $code)->first();
        }

        if ($user == null) {
            return redirect('/lupa-password')->with(['msg' => 'Terjadi kesalahan saat melakukan reset password, silahkan diulangi lagi.', 'type' => 'warning']);
        } else {
            if ($request->method() == 'GET') {
                return view('auth/reset-password', ['code' => $code]);
            } else {
                $user->password = Hash::make($request->password);
                $user->remember_token = $this->generateRandomToken();
                $user->save();
                return redirect('/masuk')->with(['msg' => 'Password anda berhasil diubah.', 'type' => 'success']);
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('beranda');
    }

    public function generateRandomToken($length = 16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring = $randstring . $characters[rand(0, (strlen($characters) - 1))];
        }
        return $randstring;
    }
}
