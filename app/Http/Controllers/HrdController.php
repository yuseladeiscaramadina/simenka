<?php

namespace App\Http\Controllers;

use App\Mail\AcceptedMail;
use App\Mail\DeclinedMail;
use App\Mail\FirstInterviewMail;
use App\Mail\PsikotestMail;
use App\Mail\SecondInterviewMail;
use App\Mail\TestMail;
use App\Models\ApplicantPsikotest;
use App\Models\ApplicantTest;
use App\Models\Application;
use App\Models\Interview;
use App\Models\JobVacancy;
use App\Models\Psikotest;
use App\Models\Test;
use App\Models\TestQuestion;
use App\Models\Applicant;
use App\Models\Hrd;
use App\Models\PsikotestQuestion;
use App\Models\PsikotestResult;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class HrdController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if ($request->session()->has('hrd_id')) {
                if ($request->session()->get('date_login') != Carbon::now()->toDateString()) {
                    Session::flush();
                    Redirect::to('/')->send();
                }
            }

            if (!$request->session()->has('hrd_id')) {
                Redirect::to('/')->send();
            }
            return $next($request);
        });
    }


    public function dashboard()
    {
        $datas = Application::all();
        $active = 0;
        $accepted = 0;
        $declined = 0;

        foreach ($datas as $data) {
            if ($data->status == 'diterima') {
                $accepted++;
            } elseif ($data->status == 'ditolak') {
                $declined++;
            } else {
                $active++;
            }
        }

        $queryAccepted = "SELECT COUNT(job_vacancy_id) as total FROM application WHERE MONTH(updated_at) = 1 AND YEAR(updated_at) = " . date('Y') . " AND `status` = 'diterima'";
        $queryDeclined = "SELECT COUNT(job_vacancy_id) as total FROM application WHERE MONTH(updated_at) = 1 AND YEAR(updated_at) = " . date('Y') . " AND `status` = 'ditolak'";
        for ($i = 2; $i <= 12; $i++) {
            $queryAccepted = $queryAccepted . " UNION ALL SELECT COUNT(job_vacancy_id) as total FROM application WHERE MONTH(updated_at) = " . $i . " AND YEAR(updated_at) = " . date('Y') . " AND `status` = 'diterima'";
            $queryDeclined = $queryDeclined . " UNION ALL SELECT COUNT(job_vacancy_id) as total FROM application WHERE MONTH(updated_at) = " . $i . " AND YEAR(updated_at) = " . date('Y') . " AND `status` = 'ditolak'";
        }

        $dataAccepted = DB::select($queryAccepted);
        $dataDeclined = DB::select($queryDeclined);

        return view('hrd/dashboard', ['pendaftarAktif' => $active, 'diterima' => $accepted, 'ditolak' => $declined, 'dataDiterima' => $dataAccepted, 'dataDitolak' => $dataDeclined]);
    }

    public function jobVacancy()
    {
        $data = JobVacancy::all();
        return view('hrd/view-job-vacancy', ['datas' => $data]);
    }

    public function addJobVacancy(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('hrd/add-job-vacancy');
        } else {
            $data = new JobVacancy();

            if ($request->hasFile('image')) {
                $img = $request->file('image');
                $dir = "dashboard/images/job_vacancy";
                $name = time() . "_" . $this->randString() . '.jpg';

                $img->move($dir, $name);
                $data->image = $dir . '/' . $name;
            }

            $data->title = $request->title;
            $data->description = $request->description;
            $data->requirement = $request->requirement;
            $data->hrd_id = $request->session()->get('hrd_id');
            $data->is_active = false;

            $data->save();

            return redirect('/hrd/kelola-lowongan-pekerjaan')->with(['msg' => 'Lowongan pekerjaan <b>' . $data->title . '</b> berhasil ditambahkan!', 'type' => 'success']);
        }
    }

    public function editJobVacancy(Request $request, $id)
    {
        if ($request->method() == 'GET') {
            $data = JobVacancy::find($id);
            return view('hrd/edit-job-vacancy', ['datas' => $data]);
        } else {
            $data = JobVacancy::find($id);
            if ($request->hasFile('image')) {

                // $oldImage = $data->image;
                // File::delete($oldImage);

                $img = $request->file('image');
                $dir = "dashboard/images/job_vacancy";
                $name = time() . "_" . $this->randString() . '.jpg';

                $img->move($dir, $name);
                $data->image = $dir . '/' . $name;
            }

            $data->title = $request->title;
            $data->description = $request->description;
            $data->requirement = $request->requirement;
            $data->is_active = false;

            $data->save();

            return redirect('/hrd/kelola-lowongan-pekerjaan')->with(['msg' => 'Lowongan pekerjaan <b>' . $data->title . '</b> berhasil diubah!', 'type' => 'success']);
        }
    }

    public function deleteJobVacancy($id)
    {
        $data = JobVacancy::find($id);
        $data->delete();

        return redirect('/hrd/kelola-lowongan-pekerjaan')->with(['msg' => 'Lowongan pekerjaan <b>' . $data->title . '</b> berhasil dihapus!', 'type' => 'warning']);
    }

    public function statusJobVacancy($id)
    {
        $data = JobVacancy::find($id);
        $data->is_active = !$data->is_active;
        $data->save();

        return redirect('/hrd/kelola-lowongan-pekerjaan')->with(['msg' => 'Status Lowongan pekerjaan <b>' . $data->title . '</b> berhasil diubah!', 'type' => 'warning']);
    }

    public function applicationNew()
    {
        $data = Application::with(['job_vacancy', 'applicant'])->where('status', 'pendaftaran')->orderBy('job_vacancy_id')->get();
        return view('hrd/view-applicant-new', ['datas' => $data]);
    }

    public function applicationActive()
    {
        $psikotest = Psikotest::all();
        $test = Test::all();
        $data = Application::with(['job_vacancy', 'applicant', 'applicant_psikotest', 'applicant_test'])->where('status', '!=', 'diterima')->where('status', '!=', 'ditolak')->where('status', '!=', 'pendaftaran')->orderBy('job_vacancy_id')->get();
        $resultPsikotest = PsikotestResult::all();

        foreach ($data as $application) {
            $applicantPsikotest = ApplicantPsikotest::with('psikotest')->where('application_id', $application->id)->first();
            $applicantTest = ApplicantTest::with('test')->where('application_id', $application->id)->first();
            $now = Carbon::now();

            if ($applicantPsikotest != null) {
                if ($now > $application->deadline) {
                    $applicantPsikotest->status = 'late';
                    $applicantPsikotest->save();
                }
            }

            if ($applicantTest != null) {
                if ($now > $application->deadline) {
                    $applicantTest->status = 'late';
                    $applicantTest->save();
                }
            }
        }

        return view('hrd/view-applicant-active', ['datas' => $data, 'psikotests' => $psikotest, 'tests' => $test, 'resultPsikotests' => $resultPsikotest]);
    }

    public function applicationAccepted()
    {
        $data = Application::with(['job_vacancy', 'applicant'])->where('status', 'diterima')->orderBy('job_vacancy_id')->get();
        return view('hrd/view-applicant-accepted', ['datas' => $data]);
    }

    public function applicationDeclined()
    {
        $data = Application::with(['job_vacancy', 'applicant'])->where('status', 'ditolak')->orderBy('job_vacancy_id')->get();
        return view('hrd/view-applicant-declined', ['datas' => $data]);
    }

    public function acceptApplication($id)
    {
        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new AcceptedMail($name));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $data = Application::find($id);
        $data->status = 'diterima';
        $data->save();
        // Email
        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil diterima!', 'type' => 'success']);
    }

    public function declineApplication($id)
    {
        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new DeclinedMail($name));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $data = Application::find($id);
        $data->status = 'ditolak';
        $data->save();
        // Email 
        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil ditolak!', 'type' => 'danger']);
    }

    public function processToFirstInterview(Request $request, $id)
    {
        $date = date_create($request->date);
        $dateFormat = $this->changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));

        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new FirstInterviewMail($name, $dateFormat, $request->link, $request->room_id, $request->room_password));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $interview = new Interview();
        $interview->application_id = $id;
        $interview->many_interview = '1';
        $interview->event_date = $request->date;
        $interview->link = $request->link;
        $interview->meeting_id = $request->room_id;
        $interview->password = $request->room_password;

        $interview->save();

        $data = Application::find($id);
        $data->status = 'wawancara_1';
        $data->save();
        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil maju ke tahap Wawancara pertama!', 'type' => 'success']);
    }

    public function processToPsikotest(Request $request, $id)
    {
        $date = date_create($request->date);
        $dateFormat = $this->changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));

        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new PsikotestMail($name, $dateFormat));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $applicantPsikotest = new ApplicantPsikotest();
        $applicantPsikotest->application_id = $id;
        $applicantPsikotest->psikotest_id = $request->psikotest;
        $applicantPsikotest->status = 'not_started';
        $applicantPsikotest->save();

        $data = Application::find($id);
        $data->status = 'psikotest';
        $data->deadline = $request->date;
        $data->save();

        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil maju ke tahap Psikotest!', 'type' => 'success']);
    }

    public function processToTest(Request $request, $id)
    {
        $date = date_create($request->date);
        $dateFormat = $this->changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));

        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new TestMail($name, $dateFormat));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $applicantTest = new ApplicantTest();
        $applicantTest->application_id = $id;
        $applicantTest->test_id = $request->test;
        $applicantTest->status = 'not_started';
        $applicantTest->save();

        $data = Application::find($id);
        $data->status = 'keterampilan';
        $data->deadline = $request->date;
        $data->save();
        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil maju ke tahap Uji Keterampilan!', 'type' => 'success']);
    }

    public function processToSecondInterview(Request $request, $id)
    {
        $date = date_create($request->date);
        $dateFormat = $this->changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));

        $name = Application::with('applicant')->where('id', $id)->first()->applicant->name;
        $email = Application::with('applicant')->where('id', $id)->first()->applicant->email;

        Mail::to($email)->send(new SecondInterviewMail($name, $dateFormat, $request->link, $request->room_id, $request->room_password));

        if (Mail::failures()) {
            return redirect()->back()->with(['msg' => 'Terjadi kesalahan saat mengirimkan email, silahkan tunggu beberapa saat!', 'type' => 'warning']);
        }

        $interview = new Interview();
        $interview->application_id = $id;
        $interview->many_interview = '2';
        $interview->event_date = $request->date;
        $interview->link = $request->link;
        $interview->meeting_id = $request->room_id;
        $interview->password = $request->room_password;

        $interview->save();

        $data = Application::find($id);
        $data->status = 'wawancara_2';
        $data->save();
        return redirect('/hrd/kelola-pelamar')->with(['msg' => 'Pelamar berhasil maju ke tahap Wawancara kedua!', 'type' => 'success']);
    }

    function skillTest()
    {
        $test = Test::all();
        return view('hrd/view-test-skill', ['tests' => $test]);
    }

    function addSkillTest(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('hrd/add-test-skill');
        } else {
            $data = new Test();
            $data->name = $request->name;
            $data->kkm = $request->kkm;
            $data->time = $request->time;
            $data->save();

            return redirect('/hrd/kelola-uji-keterampilan')->with(['msg' => 'Uji Keterampilan <b>' . $data->name . '</b> berhasil ditambahkan!', 'type' => 'success']);
        }
    }

    function editSkillTest(Request $request, $id)
    {
        if ($request->method() == 'GET') {
            $test = Test::find($id);
            return view('hrd/edit-test-skill', ['test' => $test]);
        } else {
            $test = Test::find($id);
            $test->name = $request->name;
            $test->kkm = $request->kkm;
            $test->time = $request->time;
            $test->save();

            return redirect('/hrd/kelola-uji-keterampilan')->with(['msg' => 'Uji Keterampilan <b>' . $test->name . '</b> berhasil diubah!', 'type' => 'success']);
        }
    }

    function deleteSkillTest($id)
    {
        $data = Test::find($id);
        $data->delete();

        return redirect('/hrd/kelola-uji-keterampilan')->with(['msg' => 'Uji Keterampilan <b>' . $data->name . '</b> berhasil dihapus!', 'type' => 'warning']);
    }

    function questionSkillTest($id)
    {
        $test = Test::find($id);
        $data = TestQuestion::where('test_id', $id)->get();

        return view('hrd/view-question-test-skill', ['test' => $test, 'datas' => $data]);
    }

    function addQuestionSkillTest(Request $request, $testId)
    {
        if ($request->method() == 'GET') {
            $test = Test::find($testId);
            return view('hrd/add-question-test-skill', ['test' => $test]);
        } else {
            $testQuestion = new TestQuestion();

            if ($request->hasFile('image')) {
                $img = $request->file('image');
                $dir = "dashboard/images/soal_tes";
                $name = time() . "_" . $this->randString() . '.jpg';

                $img->move($dir, $name);
                $testQuestion->image = $dir . '/' . $name;
            }

            $testQuestion->test_id = $testId;
            $testQuestion->question = $request->question;
            $testQuestion->option_a = $request->option_a;
            $testQuestion->option_b = $request->option_b;
            $testQuestion->option_c = $request->option_c;
            $testQuestion->option_d = $request->option_d;
            $testQuestion->answer = $request->answer;

            $testQuestion->save();
            $data = Test::find($testId);

            return redirect('/hrd/kelola-soal-uji-keterampilan/' . $testId)->with(['msg' => 'Soal <b>' . $data->name . '</b> berhasil ditambahkan!', 'type' => 'success']);
        }
    }

    function editQuestionSkillTest(Request $request, $questionId)
    {
        if ($request->method() == 'GET') {
            $data = TestQuestion::find($questionId);
            $test = Test::find($data->test_id);
            return view('hrd/edit-question-test-skill', ['test' => $test, 'data' => $data]);
        } else {
            $testQuestion = TestQuestion::find($questionId);

            if ($request->hasFile('image')) {
                $oldImage = $testQuestion->image;
                File::delete($oldImage);

                $img = $request->file('image');
                $dir = "dashboard/images/soal_tes";
                $name = time() . "_" . $this->randString() . '.jpg';

                $img->move($dir, $name);
                $testQuestion->image = $dir . '/' . $name;
            }

            $testQuestion->question = $request->question;
            $testQuestion->option_a = $request->option_a;
            $testQuestion->option_b = $request->option_b;
            $testQuestion->option_c = $request->option_c;
            $testQuestion->option_d = $request->option_d;
            $testQuestion->answer = $request->answer;

            $testQuestion->save();
            $data = Test::find($testQuestion->test_id);

            return redirect('/hrd/kelola-soal-uji-keterampilan/' . $testQuestion->test_id)->with(['msg' => 'Soal <b>' . $data->name . '</b> berhasil diubah!', 'type' => 'success']);
        }
    }

    function deleteQuestionSkillTest($questionId)
    {
        $data = TestQuestion::find($questionId);
        $test = Test::find($data->test_id);

        File::delete($data->image);
        $data->delete();

        return redirect('/hrd/kelola-soal-uji-keterampilan/' . $data->test_id)->with(['msg' => 'Soal <b>' . $test->name . '</b> berhasil dihapus!', 'type' => 'warning']);
    }

    function psikotest()
    {
        $data = Psikotest::all();
        return view('hrd/view-psikotest', ['datas' => $data]);
    }

    function addPsikotest(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('hrd/add-psikotest');
        } else {
            $data = new Psikotest();
            $data->name = $request->name;
            $data->time = $request->time;
            $data->save();

            return redirect('/hrd/kelola-psikotest')->with(['msg' => 'Psikotest <b>' . $data->name . '</b> berhasil ditambahkan!', 'type' => 'success']);
        }
    }

    function editPsikotest(Request $request, $id)
    {
        $data = Psikotest::find($id);

        if ($request->method() == 'GET') {
            return view('hrd/edit-psikotest', ['data' => $data]);
        } else {
            $data->name = $request->name;
            $data->time = $request->time;
            $data->save();

            return redirect('/hrd/kelola-psikotest')->with(['msg' => 'Psikotest <b>' . $data->name . '</b> berhasil diubah!', 'type' => 'success']);
        }
    }

    function deletePsikotest($id)
    {
        $data = Psikotest::find($id);
        $data->delete();

        return redirect('/hrd/kelola-psikotest')->with(['msg' => 'Psikotest <b>' . $data->name . '</b> berhasil dihapus!', 'type' => 'warning']);
    }

    function questionPsikotest($id)
    {
        $psikotest = Psikotest::find($id);
        $data = PsikotestQuestion::with('psikotest')->where('psikotest_id', $id)->get();

        return view('hrd/view-question-psikotest', ['datas' => $data, 'psikotest' => $psikotest]);
    }

    function addQuestionPsikotest(Request $request, $id)
    {
        $psikotest = Psikotest::find($id);

        if ($request->method() == 'GET') {
            return view('hrd/add-question-psikotest', ['psikotest' => $psikotest]);
        } else {
            $data = new PsikotestQuestion();
            $data->psikotest_id = $id;
            $data->question = $request->question;
            $data->option_a = $request->option_a;
            $data->option_b = $request->option_b;
            $data->option_c = $request->option_c;
            $data->option_d = $request->option_d;
            $data->option_type_a_me = $request->question_type_a_me;
            $data->option_type_b_me = $request->question_type_b_me;
            $data->option_type_c_me = $request->question_type_c_me;
            $data->option_type_d_me = $request->question_type_d_me;
            $data->option_type_a_not_me = $request->question_type_a_not_me;
            $data->option_type_b_not_me = $request->question_type_b_not_me;
            $data->option_type_c_not_me = $request->question_type_c_not_me;
            $data->option_type_d_not_me = $request->question_type_d_not_me;
            $data->save();

            return redirect('/hrd/kelola-soal-psikotest/' . $id)->with(['msg' => 'Soal Psikotest <b>' . $data->question . '</b> berhasil ditambah!', 'type' => 'success']);
        }
    }

    function editQuestionPsikotest(Request $request, $id)
    {
        $data = PsikotestQuestion::find($id);

        if ($request->method() == 'GET') {
            return view('hrd/edit-question-psikotest', ['data' => $data]);
        } else {
            $data->question = $request->question;
            $data->option_a = $request->option_a;
            $data->option_b = $request->option_b;
            $data->option_c = $request->option_c;
            $data->option_d = $request->option_d;
            $data->option_type_a_me = $request->question_type_a_me;
            $data->option_type_b_me = $request->question_type_b_me;
            $data->option_type_c_me = $request->question_type_c_me;
            $data->option_type_d_me = $request->question_type_d_me;
            $data->option_type_a_not_me = $request->question_type_a_not_me;
            $data->option_type_b_not_me = $request->question_type_b_not_me;
            $data->option_type_c_not_me = $request->question_type_c_not_me;
            $data->option_type_d_not_me = $request->question_type_d_not_me;
            $data->save();

            return redirect('/hrd/kelola-soal-psikotest/' . $data->psikotest_id)->with(['msg' => 'Soal Psikotest <b>' . $data->question . '</b> berhasil diubah!', 'type' => 'success']);
        }
    }

    function deleteQuestionPsikotest($id)
    {
        $data = PsikotestQuestion::find($id);
        $data->delete();

        return redirect('/hrd/kelola-soal-psikotest/' . $data->psikotest_id)->with(['msg' => 'Soal Psikotest <b>' . $data->question . '</b> berhasil dihapus!', 'type' => 'warning']);
    }


    function changePassword(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/change-password');
        } else {
            if (Hash::check($request->old_password, $request->session()->get('password'))) {
                $user = Hrd::find($request->session()->get('hrd_id'));
                $user->password = Hash::make($request->new_password);
                $user->save();

                Session::flush();
                return redirect('/masuk')->with(['msg' => 'Password akun anda berhasil diubah! Silahkan masuk dengan password baru anda.', 'type' => 'success']);
            } else {
                return Redirect::back()->with(['msg' => 'Password lama yang anda masukkan salah', 'type' => 'warning']);
            }
        }
    }

    function changeFormatDateIndonesia($date)
    {
        $bulan = array(
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $exp = explode(' ', $date);
        $resDate = $exp[0];

        $expResDate = explode('/', $resDate);
        return $expResDate[2] . ' ' . $bulan[(int)$expResDate[1]] . ' ' . $expResDate[0] . ', jam ' . $exp[1] . ' ' . $exp[2];
    }

    function randString()
    {
        $characters = '0123456789abcdefghijkmnopqrstuvwxyABCDEFGHIJKMNOPQRSTUVWXY';
        $randstring = '';
        for ($i = 0; $i < 8; $i++) {
            $randstring = $randstring . $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }
}
