Dropzone.options.dropzoneKtp = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/ktp',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/ktp',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneKk = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/kk',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/kk',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneCv = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/cv',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/cv',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneIjazah = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/ijazah',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/ijazah',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneTranskrip = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/transkrip',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/transkrip',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneNpwp = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/npwp',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/npwp',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

Dropzone.options.dropzoneBpjs = {
  maxFiles: 1,
  maxFileSize: 2,
  acceptedFiles: ".pdf",
  accept: function(file, done) {
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    switch (file.type) {
      case 'application/pdf':
        thumbnail.css('background', '/dashboard/assets/images/pdf-mini.png');
        break;
    }

    done();
  },
  addRemoveLinks: true,
  timeout: 50000,
  init: function() {
    var myDropzone = this;
    $.ajax({
      url: 'doc/bpjs',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, value) {
          var file = {
            name: value.name,
            size: value.size
          };
          myDropzone.options.addedfile.call(myDropzone, file);
          myDropzone.options.thumbnail.call(myDropzone, file, '/dashboard/assets/images/pdf-mini.png');
          myDropzone.emit("complete", file);
          myDropzone.disable();

        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    });
  },
  removedfile: function(file) {
    if (this.options.dictRemoveFile) {
      return Dropzone.confirm("Apakah anda yakin ingin menghapus file ini?", function() {
        if (file.previewElement.id != "") {
          var name = file.previewElement.id;
        } else {
          var name = file.name;
        }
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'DELETE',
          url: 'doc/bpjs',
          data: {
            filename: name,
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            alert(data.success + " Berhasil dihapus!");
            // window.location.reload();
          },
          error: function(e) {
            console.log(e);
          }
        });
        var fileRef;
        return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 1;
      });
    }
  },

  success: function(file, response) {
    file.previewElement.id = response.success;
    var olddatadzname = file.previewElement.querySelector("[data-dz-name]");
    file.previewElement.querySelector("img").alt = response.success;
    olddatadzname.innerHTML = response.success;
  },
  error: function(file, response) {
    if ($.type(response) === "string")
      var message = response; //dropzone sends it's own error messages in string
    else
      var message = response.message;
    file.previewElement.classList.add("dz-error");
    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      _results.push(node.textContent = message);
    }
    return _results;
  }
};

