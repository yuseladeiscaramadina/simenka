<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\BerandaController::class, 'index'])->name('beranda');

Route::get('/lihat-lowongan', [App\Http\Controllers\BerandaController::class, 'allJobVacancy']);
Route::get('/lihat-lowongan/{id}', [App\Http\Controllers\BerandaController::class, 'jobVacancy']);
Route::get('/daftar-lowongan/{id}', [App\Http\Controllers\BerandaController::class, 'registerVacancy']);

// Login
Route::get('/masuk', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::post('/masuk', [App\Http\Controllers\AuthController::class, 'login']);

// Login Google
Route::get('/auth/redirect', [App\Http\Controllers\AuthController::class, 'redirectToProvider']);
Route::get('/auth/callback', [App\Http\Controllers\AuthController::class, 'loginFromGoogle']);

// Daftar
Route::get('/daftar', [App\Http\Controllers\AuthController::class, 'signup']);
Route::post('/daftar', [App\Http\Controllers\AuthController::class, 'signup']);

// Lupa Password
Route::get('/lupa-password', [App\Http\Controllers\AuthController::class, 'forgotPassword']);
Route::post('/lupa-password', [App\Http\Controllers\AuthController::class, 'forgotPassword']);

Route::get('/reset-password/{code}', [App\Http\Controllers\AuthController::class, 'resetPassword']);
Route::post('/reset-password/{code}', [App\Http\Controllers\AuthController::class, 'resetPassword']);

// Ganti Password
Route::get('/ganti-password', [App\Http\Controllers\HrdController::class, 'changePassword']);
Route::post('/ganti-password', [App\Http\Controllers\HrdController::class, 'changePassword']);

// Ganti Password
Route::get('/pelamar/ganti-password', [App\Http\Controllers\PelamarController::class, 'changePassword']);
Route::post('/pelamar/ganti-password', [App\Http\Controllers\PelamarController::class, 'changePassword']);

// Logout
Route::get('/logout', [App\Http\Controllers\AuthController::class, 'logout']);

// Pelamar
Route::get('/my-dashboard', [App\Http\Controllers\PelamarController::class, 'dashboard']);
Route::get('/lamaranku', [App\Http\Controllers\PelamarController::class, 'application']);
Route::get('/profilku', [App\Http\Controllers\PelamarController::class, 'profile']);
Route::get('/ikuti-psikotest', [App\Http\Controllers\PelamarController::class, 'doPsikotest']);
Route::get('/failed-psikotest', [App\Http\Controllers\PelamarController::class, 'failedPsikotest']);
Route::get('/waktu-psikotest', [App\Http\Controllers\PelamarController::class, 'timePsikotest']);
Route::get('/ikuti-uji-keterampilan', [App\Http\Controllers\PelamarController::class, 'doTest']);
Route::get('/waktu-test', [App\Http\Controllers\PelamarController::class, 'timeTest']);
Route::get('/failed-test', [App\Http\Controllers\PelamarController::class, 'failedTest']);

Route::post('/ubah-foto-profil', [App\Http\Controllers\PelamarController::class, 'changeProfilePicture']);
Route::post('/profilku', [App\Http\Controllers\PelamarController::class, 'profile']);
Route::post('/ikuti-psikotest', [App\Http\Controllers\PelamarController::class, 'doPsikotest']);
Route::post('/ikuti-uji-keterampilan', [App\Http\Controllers\PelamarController::class, 'doTest']);

// Dokumen
Route::get('/doc/ktp', [App\Http\Controllers\PelamarController::class, 'docKtp']);
Route::get('/doc/kk', [App\Http\Controllers\PelamarController::class, 'docKk']);
Route::get('/doc/cv', [App\Http\Controllers\PelamarController::class, 'docCv']);
Route::get('/doc/ijazah', [App\Http\Controllers\PelamarController::class, 'docIjazah']);
Route::get('/doc/transkrip', [App\Http\Controllers\PelamarController::class, 'docTranskrip']);
Route::get('/doc/npwp', [App\Http\Controllers\PelamarController::class, 'docNpwp']);
Route::get('/doc/bpjs', [App\Http\Controllers\PelamarController::class, 'docBpjs']);

Route::post('/doc/ktp', [App\Http\Controllers\PelamarController::class, 'docKtp']);
Route::post('/doc/kk', [App\Http\Controllers\PelamarController::class, 'docKk']);
Route::post('/doc/cv', [App\Http\Controllers\PelamarController::class, 'docCv']);
Route::post('/doc/ijazah', [App\Http\Controllers\PelamarController::class, 'docIjazah']);
Route::post('/doc/transkrip', [App\Http\Controllers\PelamarController::class, 'docTranskrip']);
Route::post('/doc/npwp', [App\Http\Controllers\PelamarController::class, 'docNpwp']);
Route::post('/doc/bpjs', [App\Http\Controllers\PelamarController::class, 'docBpjs']);

Route::delete('/doc/ktp', [App\Http\Controllers\PelamarController::class, 'docKtp']);

// HRD
Route::get('/hrd/dashboard', [App\Http\Controllers\HrdController::class, 'dashboard']);
Route::get('/hrd/kelola-lowongan-pekerjaan', [App\Http\Controllers\HrdController::class, 'jobVacancy']);
Route::get('/hrd/tambah-lowongan-pekerjaan', [App\Http\Controllers\HrdController::class, 'addJobVacancy']);
Route::get('/hrd/ubah-lowongan-pekerjaan/{id}', [App\Http\Controllers\HrdController::class, 'editJobVacancy']);
Route::get('/hrd/hapus-lowongan-pekerjaan/{id}', [App\Http\Controllers\HrdController::class, 'deleteJobVacancy']);
Route::get('/hrd/status-lowongan-pekerjaan/{id}', [App\Http\Controllers\HrdController::class, 'statusJobVacancy']);
Route::get('/hrd/kelola-pelamar-baru', [App\Http\Controllers\HrdController::class, 'applicationNew']);
Route::get('/hrd/kelola-pelamar', [App\Http\Controllers\HrdController::class, 'applicationActive']);
Route::get('/hrd/lihat-pelamar-diterima', [App\Http\Controllers\HrdController::class, 'applicationAccepted']);
Route::get('/hrd/lihat-pelamar-ditolak', [App\Http\Controllers\HrdController::class, 'applicationDeclined']);
Route::get('/hrd/terima-lamaran/{id}', [App\Http\Controllers\HrdController::class, 'acceptApplication']);
Route::get('/hrd/tolak-lamaran/{id}', [App\Http\Controllers\HrdController::class, 'declineApplication']);
Route::get('/hrd/kelola-uji-keterampilan', [App\Http\Controllers\HrdController::class, 'skillTest']);
Route::get('/hrd/tambah-uji-keterampilan', [App\Http\Controllers\HrdController::class, 'addSkillTest']);
Route::get('/hrd/ubah-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'editSkillTest']);
Route::get('/hrd/hapus-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'deleteSkillTest']);
Route::get('/hrd/kelola-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'questionSkillTest']);
Route::get('/hrd/tambah-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'addQuestionSkillTest']);
Route::get('/hrd/ubah-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'editQuestionSkillTest']);
Route::get('/hrd/hapus-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'deleteQuestionSkillTest']);
Route::get('/hrd/kelola-psikotest', [App\Http\Controllers\HrdController::class, 'psikotest']);
Route::get('/hrd/tambah-psikotest', [App\Http\Controllers\HrdController::class, 'addPsikotest']);
Route::get('/hrd/ubah-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'editPsikotest']);
Route::get('/hrd/hapus-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'deletePsikotest']);
Route::get('/hrd/kelola-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'questionPsikotest']);
Route::get('/hrd/tambah-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'addQuestionPsikotest']);
Route::get('/hrd/ubah-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'editQuestionPsikotest']);
Route::get('/hrd/hapus-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'deleteQuestionPsikotest']);



Route::post('/hrd/tambah-lowongan-pekerjaan', [App\Http\Controllers\HrdController::class, 'addJobVacancy']);
Route::post('/hrd/ubah-lowongan-pekerjaan/{id}', [App\Http\Controllers\HrdController::class, 'editJobVacancy']);
Route::post('/hrd/lanjut-wawancara-satu/{id}', [App\Http\Controllers\HrdController::class, 'processToFirstInterview']);
Route::post('/hrd/lanjut-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'processToPsikotest']);
Route::post('/hrd/lanjut-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'processToTest']);
Route::post('/hrd/lanjut-wawancara-dua/{id}', [App\Http\Controllers\HrdController::class, 'processToSecondInterview']);
Route::post('/hrd/tambah-uji-keterampilan', [App\Http\Controllers\HrdController::class, 'addSkillTest']);
Route::post('/hrd/ubah-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'editSkillTest']);
Route::post('/hrd/tambah-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'addQuestionSkillTest']);
Route::post('/hrd/ubah-soal-uji-keterampilan/{id}', [App\Http\Controllers\HrdController::class, 'editQuestionSkillTest']);
Route::post('/hrd/tambah-psikotest', [App\Http\Controllers\HrdController::class, 'addPsikotest']);
Route::post('/hrd/ubah-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'editPsikotest']);
Route::post('/hrd/tambah-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'addQuestionPsikotest']);
Route::post('/hrd/ubah-soal-psikotest/{id}', [App\Http\Controllers\HrdController::class, 'editQuestionPsikotest']);
