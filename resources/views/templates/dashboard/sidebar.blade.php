@if(Session::has('hrd_id'))
<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile not-navigation-link">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{ url(Session::get('profile_picture')) }}">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name">{{Session::get('name')}}</p>
                        <div class="dropdown" data-display="static">
                            <a href="#" class="nav-link d-flex user-switch-dropdown-toggler" id="UsersettingsDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                                <small class="designation text-muted">HRD</small>
                                <span class="status-indicator online"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="UsersettingsDropdown">
                                <a href="/ganti-password" class="dropdown-item mt-2"> Ganti Password </a>
                                <a href="/logout" class="dropdown-item"> Keluar </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('/hrd/dashboard') }}">
                <i class="menu-icon mdi mdi-television"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/hrd/kelola-lowongan-pekerjaan') }}">
                <i class="menu-icon mdi mdi-clipboard"></i>
                <span class="menu-title">Lowongan Pekerjaan</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui" aria-controls="basic-ui">
                <i class="menu-icon mdi mdi-account"></i>
                <span class="menu-title">Pelamar</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse " id="basic-ui">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/hrd/kelola-pelamar-baru') }}">Pelamar Baru</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/hrd/kelola-pelamar') }}">Pelamar Aktif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/hrd/lihat-pelamar-diterima') }}">Pelamar Diterima</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/hrd/lihat-pelamar-ditolak') }}">Pelamar Ditolak</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/hrd/kelola-psikotest') }}">
                <i class="menu-icon mdi mdi-hexagon-multiple"></i>
                <span class="menu-title">Soal Psikotest</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/hrd/kelola-uji-keterampilan') }}">
                <i class="menu-icon mdi mdi-tie"></i>
                <span class="menu-title">Soal Uji Keterampilan</span>
            </a>
        </li>
    </ul>
</nav>
@else
<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile not-navigation-link">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{ url(Auth::user()->profile_picture) }}">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name">{{Auth::user()->name}}</p>
                        <div class="dropdown" data-display="static">
                            <a href="#" class="nav-link d-flex user-switch-dropdown-toggler" id="UsersettingsDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                                <small class="designation text-muted">Pelamar</small>
                                <span class="status-indicator online"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="UsersettingsDropdown">
                                <a href="pelamar/ganti-password" class="dropdown-item mt-2"> Ganti Password </a>
                                <a href="/logout" class="dropdown-item"> Keluar </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('/my-dashboard') }}">
                <i class="menu-icon mdi mdi-view-dashboard"></i>
                <span class="menu-title">Beranda</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('/lamaranku') }}">
                <i class="menu-icon mdi mdi-briefcase"></i>
                <span class="menu-title">Lamaranku</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('/profilku') }}">
                <i class="menu-icon mdi mdi-worker"></i>
                <span class="menu-title">Profilku</span>
            </a>
        </li>
    </ul>
</nav>
@endif