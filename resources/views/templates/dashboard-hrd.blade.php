<!DOCTYPE html>
<html>

<head>
    <title>Simenka</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="_token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

    <!-- plugin css -->
    {!! Html::style('dashboard/assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
    {!! Html::style('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
    <!-- end plugin css -->

    @stack('plugin-styles')

    <!-- common css -->
    {!! Html::style('dashboard/css/app.css') !!}
    <!-- end common css -->

    @stack('style')
</head>

<body data-base-url="{{url('/')}}">

    <div class="container-scroller" id="app">
        @include('templates.dashboard.header')
        <div class="container-fluid page-body-wrapper">
            @include('templates.dashboard.sidebar')
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content')
                </div>
                @include('templates.dashboard.footer')
            </div>
        </div>
    </div>

    <!-- base js -->
    {!! Html::script('dashboard/js/app.js') !!}
    {!! Html::script('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
    <!-- end base js -->

    <!-- plugin js -->
    @stack('plugin-scripts')
    <!-- end plugin js -->

    <!-- common js -->
    {!! Html::script('dashboard/assets/js/off-canvas.js') !!}
    {!! Html::script('dashboard/assets/js/hoverable-collapse.js') !!}
    {!! Html::script('dashboard/assets/js/misc.js') !!}
    {!! Html::script('dashboard/assets/js/settings.js') !!}
    {!! Html::script('dashboard/assets/js/todolist.js') !!}
    <!-- end common js -->

    @stack('custom-scripts')
</body>

</html>