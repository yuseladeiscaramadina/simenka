<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>SIMENKA</title>

    <link href="{{ asset('beranda/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('beranda/assets/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('beranda/assets/css/templatemo-space-dynamic.css') }}">
    <link rel="stylesheet" href="{{ asset('beranda/assets/css/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('beranda/assets/css/owl.css') }}">

    <style>
        .introduce-mka {
            color: #fff;
            font-size: 50px;
            font-weight: 700;
            margin-bottom: 12px;
        }

        /* For Phone */
        @media only screen and (max-width: 600px) {
            .introduce-mka {
                color: #fff;
                font-size: 30px;
                font-weight: 700;
                margin-bottom: 12px;
            }
        }
    </style>

</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="js-preloader" class="js-preloader">
        <div class="preloader-inner">
            <span class="dot"></span>
            <div class="dots">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="/" class="logo">
                            <h4>SI<span>-</span>MENKA</h4>
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#tentang-kami">Tentang Kami</a></li>
                            <li class="scroll-to-section"><a href="#lowongan-pekerjaan">Lowongan Pekerjaan</a></li>
                            @if(!Auth::guest())
                            <li class="scroll-to-section">
                                <a href="/my-dashboard">Profilku</a>
                            </li>
                            <li class="scroll-to-section">
                                <a href="/logout">Keluar</a>
                            </li>
                            @else
                            <li class="scroll-to-section">
                                <div class="main-white-button" onMouseOver="this.style.color='#fff'" onMouseOut="this.style.color='#fff'"><a href="/masuk" style="display: inline-block;font-size: 15px;font-weight: 400;color: #fff;background-color:#c6c3c3;text-transform: capitalize;padding: 0px 20px;border-radius: 23px;letter-spacing: 0.25px;">Kuy Login</a></div>
                            </li>
                            @endif
                            <li class="scroll-to-section">
                            </li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    @yield('content')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{asset('beranda/assets/js/owl-carousel.js')}}"></script>
    <script src="{{asset('beranda/assets/js/animation.js')}}"></script>
    <script src="{{asset('beranda/assets/js/imagesloaded.js')}}"></script>
    <script src="{{asset('beranda/assets/js/templatemo-custom.js')}}"></script>

    @yield('js')

</body>

</html>