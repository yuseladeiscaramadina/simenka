@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
@endpush

@php
function tgl_indo($tanggal){
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);
$pecahkan = explode('-', $tanggal);
return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
@endphp

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h4 style="margin-bottom: 30px;">Kelola Data Pelamar</h4>
                    </div>
                </div>
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <table id="applicant-table">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Nama Pelamar </th>
                            <th> Posisi </th>
                            <th> Pendidikan Terakhir </th>
                            <th> Aksi </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($datas) == 0)
                        <tr>
                        <td colspan="5" class="text-center">Tidak ada data</td>
                        </tr>
                        @else
                        @php
                        $no = 1;
                        @endphp
                        @foreach($datas as $data)
                        <tr>
                        <td>{{$no++}}</td>
                        <td>
                            <p>{{$data->applicant->name}}</p>
                        </td>
                        <td>
                            <p>{{$data->job_vacancy->title}}</p>
                        </td>
                        <td>
                            <p>{{$data->applicant->last_education}}</p>
                        </td>
                        <td>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#profilModal{{$data->id}}">Lihat Profil</button>
                            <button class="btn btn-success" data-toggle="modal" data-target="#wawancaraSatuModal{{$data->id}}">Lanjut ke Wawancara 1</button>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                        </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@foreach($datas as $data)
<div class="modal fade" id="profilModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="profilModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 95%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="profilModalLabel">Detail Profil <b>{{$data->applicant->name}}</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img class="rounded-circle" style="width: 200px;" src="{{asset($data->applicant->profile_picture);}}" alt="{{$data->applicant->name}}">
                </div>
                <hr>
                <h4>Data Diri</h4>
                <div class="row mb-2">
                    <div class="col-4">Nama </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7"><b>{{$data->applicant->name}}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. KTP </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->id_card_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. KK </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->family_card_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. Telepon </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->phone_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Alamat KTP </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->ktp_address}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Alamat Saat Ini </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->current_address}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Agama </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->religion}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Usia </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->age}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Jenis Kelamin </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{ucfirst($data->applicant->gender)}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Tempat, Tanggal Lahir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->place_of_birth.', '.tgl_indo($data->applicant->date_of_birth)}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Pendidikan Terakhir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->last_education}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Jurusan </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->major}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Tempat Pendidikan Terakhir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->last_place_education}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Nama Ibu </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->mother_name}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. Rekening </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->account_number}} ({{$data->applicant->bank}})</div>
                </div>

                <hr>
                <h4>Dokumen</h4>
                <div class="row text-center">
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_id_card) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>KTP</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_family_card) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>KK</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_cv) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>CV</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_certificate_of_education) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>Ijazah</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_transcript) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>Transkrip</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_taxpayer) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>NPWP</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_bpjs) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>BPJS</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="deleteModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menolak lamaran ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/tolak-lamaran/{{$data->id}}"><button class="btn btn-danger">Ya, Tolak</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="wawancaraSatuModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="wawancaraSatuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/hrd/lanjut-wawancara-satu/{{$data->id}}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="wawancaraSatuModalLabel">Form Lanjut ke Wawancara pertama</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Form Wawancara 1</h4>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-4">
                                <label>Waktu</label>
                            </div>
                            <div class="col-8">
                                <input type="datetime-local" class="form-control" name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Link Zoom</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="link">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>ID Room</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="room_id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Password Room</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="room_password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <input type="submit" class="btn btn-success" value="Ya, Lanjutkan">
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#applicant-table').DataTable();
    });
</script>
@endpush