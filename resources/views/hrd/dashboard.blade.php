@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
                    <div class="float-left">
                        <i class="mdi mdi-file text-primary icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Total Pendaftar Aktif</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0">{{$pendaftarAktif}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
                    <div class="float-left">
                        <i class="mdi mdi-account-check text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Total Diterima</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0">{{$diterima}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
                    <div class="float-left">
                        <i class="mdi mdi-account-remove text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Total Ditolak</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0">{{$ditolak}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center mb-4">
                    <h2 class="card-title mb-0">Grafik Penerimaan Pegawai Tahun <b>{{date('Y')}}</b></h2>
                    <div class="wrapper d-flex">
                        <div class="d-flex align-items-center mr-3">
                            <span class="dot-indicator bg-success"></span>
                            <p class="mb-0 ml-2 text-muted">Diterima</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <span class="dot-indicator bg-primary"></span>
                            <p class="mb-0 ml-2 text-muted">Tidak Diterima</p>
                        </div>
                    </div>
                </div>
                <div class="chart-container">
                    <canvas id="dashboard-area-chart" height="80"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

<script>
    if ($("#dashboard-area-chart").length) {
        var lineChartCanvas = $("#dashboard-area-chart")
            .get(0)
            .getContext("2d");
        var data = {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
            datasets: [{
                    label: "Diterima",
                    data: [<?php foreach ($dataDiterima as $diterima) {
                                echo $diterima->total . ',';
                            } ?>],
                    backgroundColor: "#19d895",
                    borderColor: "#15b67d",
                    borderWidth: 1,
                    fill: true
                },
                {
                    label: "Ditolak",
                    data: [<?php foreach ($dataDitolak as $ditolak) {
                                echo $ditolak->total . ',';
                            } ?>],
                    backgroundColor: "#2196f3",
                    borderColor: "#0c83e2",
                    borderWidth: 1,
                    fill: true
                }
            ]
        };
        var options = {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                yAxes: [{
                    gridLines: {
                        color: "#F2F6F9"
                    },
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 20,
                        stepSize: 10
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: "#F2F6F9"
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            legend: {
                display: false
            },
            elements: {
                point: {
                    radius: 2
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            stepsize: 1
        };
        var lineChart = new Chart(lineChartCanvas, {
            type: "line",
            data: data,
            options: options
        });
    }
</script>
@endpush