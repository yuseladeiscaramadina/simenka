@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 style="margin-bottom: 30px;">Kelola Data Lowongan Pekerjaan</h4>
                    </div>
                    <div class="col-2">
                        <a href="/hrd/tambah-lowongan-pekerjaan">
                            <button class="btn btn-primary" style="float: right;"><i class="menu-icon mdi mdi-plus"></i> Tambah</button>
                        </a>
                    </div>
                </div>
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Posisi </th>
                                <th> Deskripsi </th>
                                <th> Kebutuhan </th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($datas) == 0)
                            <tr>
                                <td colspan="5" class="text-center">Tidak ada data</td>
                            </tr>
                            @else
                            @php
                            $no = 1;
                            @endphp
                            @foreach($datas as $data)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>
                                    <p>{{$data->title}}</p>
                                </td>
                                @php
                                $description = $data->description;
                                $requirement = $data->requirement;
                                if(strlen($data->description) > 30) {
                                $description = substr($data->description, 0, 29) . "...";
                                }
                                if(strlen($data->requirement) > 30) {
                                $requirement = substr($data->requirement, 0, 29) . "...";
                                }
                                @endphp
                                <td data-toggle="modal" data-target="#descriptionModal{{$data->id}}">{!!$description!!}</td>
                                <td data-toggle="modal" data-target="#requirementModal{{$data->id}}">{!!$requirement!!}</td>
                                <td>
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#imageModal{{$data->id}}">Lihat Gambar</button>
                                    <a href="/hrd/ubah-lowongan-pekerjaan/{{$data->id}}"><button class="btn btn-warning"> Ubah</button></a>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}"> Hapus</button>
                                    @if($data->is_active)
                                    <button class="btn btn-dark" data-toggle="modal" data-target="#hideModal{{$data->id}}"> Sembunyikan</button>
                                    @else
                                    <button class="btn btn-dark" data-toggle="modal" data-target="#showModal{{$data->id}}"> Tampilkan</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($datas as $data)
<div class="modal fade" id="imageModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Lihat Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($data->image == null)
                <p class="text-center">Gambar Tidak Ditemukan</p>
                @else
                <img src="{{url($data->image)}}" class="img-fluid">
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="descriptionModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="descriptionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="descriptionModalLabel">Detail Deskripsi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="text-align: justify;text-justify: inter-word;">{!!$data->description!!}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="requirementModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="requirementModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requirementModalLabel">Detail Kebutuhan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="text-align: justify;text-justify: inter-word;">{!!$data->requirement!!}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="deleteModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah yakin ingin menghapus data? sekali terhapus maka data tidak akan bisa dikembalikan.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/hapus-lowongan-pekerjaan/{{$data->id}}"><button class="btn btn-danger">Ya, Hapus</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="hideModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="hideModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hideModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menyembunyikan lowongan ini? jika disembunyikan maka calon pelamar tidak dapat melihat informasi lowongan pekerjaan ini.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/status-lowongan-pekerjaan/{{$data->id}}"><button class="btn btn-warning">Ya, Sembunyikan</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="showModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="showModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menampilkan lowongan ini? jika ditampilkan maka calon pelamar dapat melihat informasi lowongan pekerjaan ini.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/status-lowongan-pekerjaan/{{$data->id}}"><button class="btn btn-success">Ya, Tampilkan</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

@endpush