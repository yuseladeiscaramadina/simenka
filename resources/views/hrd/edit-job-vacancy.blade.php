@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 style="margin-bottom: 30px;">Ubah Data Lowongan Pekerjaan <b>{{$datas->title}}</b></h4>

                        <form action="/hrd/ubah-lowongan-pekerjaan/{{$datas->id}}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Posisi Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="title" id="title" value="{{$datas->title}}" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Deskripsi Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <textarea name="description" id="description" cols="30" rows="10" required>{{$datas->description}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Kebutuhan Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <textarea name="requirement" id="requirement" cols="30" rows="10" required>{{$datas->requirement}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Gambar</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="file" name="image" id="image" accept="image/*" class="form-control" style="height: 3rem;">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

<script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>

<script>
    const ckDescription = ClassicEditor
        .create(document.querySelector('#description'), {
            removePlugins: ['Heading'],
            toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote']
        })
        .catch(error => {
            console.error(error);
        });

    ckDescription.on('required', function(evt) {
        ckDescription.showNotification('This field is required.', 'warning');
        evt.cancel();
    });

    const ckRequirement = ClassicEditor
        .create(document.querySelector('#requirement'), {
            removePlugins: ['Heading'],
            toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote']
        })
        .catch(error => {
            console.error(error);
        });

    ckRequirement.on('required', function(evt) {
        ckRequirement.showNotification('This field is required.', 'warning');
        evt.cancel();
    });
</script>
@endpush