@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 style="margin-bottom: 30px;">Tambah Data Tes Uji Keterampilan</h4>

                        <form action="/hrd/ubah-uji-keterampilan/{{$test->id}}" method="POST">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Nama Uji Tes Keterampilan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="name" id="name" value="{{$test->name}}" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Nilai KKM</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="kkm" id="kkm" value="{{$test->kkm}}" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Waktu Pengerjaan (Menit)</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="number" value="{{$test->time}}" name="time" id="time" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
@endpush