@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
@endpush

@php
function tgl_indo($tanggal){
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);
$pecahkan = explode('-', $tanggal);
return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
@endphp

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-10">
            <h4 style="margin-bottom: 30px;">Kelola Psikotest <b>{{$psikotest->name}}</b></h4>
          </div>
          <div class="col-2">
            <a href="/hrd/tambah-soal-psikotest/{{$psikotest->id}}">
              <button class="btn btn-primary" style="float: right;"><i class="menu-icon mdi mdi-plus"></i> Tambah</button>
            </a>
          </div>
        </div>
        @if(Session::get('msg'))
        <div class="alert alert-{!!session::get('type')!!}" role="alert">
          {!!session('msg')!!}
        </div>
        @endif
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th> # </th>
                <th> Pertanyaan </th>
                <th> Pilihan A </th>
                <th> Pilihan B </th>
                <th> Pilihan C </th>
                <th> Pilihan D </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @php
              $no = 1;
              @endphp
              @if(count($datas) == 0)
              <tr>
              <td colspan="7" class="text-center">Tidak ada data</td>
              </tr>
              @else
              @foreach($datas as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->question}}</td>
                <td>{{$data->option_a}} ~ <b>(P: {{$data->option_type_a_me}}, K: {{$data->option_type_a_not_me}})</b></td>
                <td>{{$data->option_b}} ~ <b>(P: {{$data->option_type_b_me}}, K: {{$data->option_type_a_not_me}})</b></td>
                <td>{{$data->option_c}} ~ <b>(P: {{$data->option_type_c_me}}, K: {{$data->option_type_a_not_me}})</b></td>
                <td>{{$data->option_d}} ~ <b>(P: {{$data->option_type_d_me}}, K: {{$data->option_type_a_not_me}})</b></td>
                <td>
                  <a href="/hrd/ubah-soal-psikotest/{{$data->id}}" class="btn btn-warning">Ubah</a>
                  <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Hapus</button>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


@foreach($datas as $data)
<div class="modal fade" id="deleteModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah yakin ingin menghapus data? sekali terhapus maka data tidak akan bisa dikembalikan.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="/hrd/hapus-soal-psikotest/{{$data->id}}"><button class="btn btn-danger">Ya, Hapus</button></a>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

@endpush