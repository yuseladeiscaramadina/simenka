@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 style="margin-bottom: 30px;">Ubah Soal <b>{{$test->name}}</b></h4>

                        <form action="/hrd/ubah-soal-uji-keterampilan/{{$data->id}}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Soal</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="question" id="question" class="form-control" value="{{$data->question}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Pilihan A</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="option_a" id="option_a" class="form-control" value="{{$data->option_a}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Pilihan B</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="option_b" id="option_b" class="form-control" value="{{$data->option_b}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Pilihan C</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="option_c" id="option_c" class="form-control" value="{{$data->option_c}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Pilihan D</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="option_d" id="option_d" class="form-control" value="{{$data->option_d}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Jawaban</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <select name="answer" class="form-control" style="height: 3rem;">
                                            @if($data->answer == 'a')
                                            <option value="a" selected>A</option>
                                            @else
                                            <option value="a">A</option>
                                            @endif
                                            @if($data->answer == 'b')
                                            <option value="b" selected>B</option>
                                            @else
                                            <option value="b">B</option>
                                            @endif
                                            @if($data->answer == 'c')
                                            <option value="c" selected>C</option>
                                            @else
                                            <option value="c">C</option>
                                            @endif
                                            @if($data->answer == 'd')
                                            <option value="d" selected>D</option>
                                            @else
                                            <option value="d">D</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Gambar</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="file" name="image" id="image" accept="image/*" class="form-control" style="height: 3rem;">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
@endpush