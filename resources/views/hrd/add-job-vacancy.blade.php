@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 style="margin-bottom: 30px;">Tambah Data Lowongan Pekerjaan</h4>

                        <form action="/hrd/tambah-lowongan-pekerjaan" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Posisi Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Deskripsi Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <textarea name="description" id="description" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Kebutuhan Lowongan Pekerjaan</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <textarea name="requirement" id="requirement" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <label>Gambar</label>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <input type="file" name="image" id="image" accept="image/*" class="form-control" style="height: 3rem;">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Simpan" class="btn btn-primary float-right" />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

<script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>

<script>
    ClassicEditor
        .create(document.querySelector('#description'), {
            removePlugins: ['Heading'],
            toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote']
        })
        .catch(error => {
            console.error(error);
        });

    ClassicEditor
        .create(document.querySelector('#requirement'), {
            removePlugins: ['Heading'],
            toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote']
        })
        .catch(error => {
            console.error(error);
        });
</script>
@endpush