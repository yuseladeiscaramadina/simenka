@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@php
function tgl_indo($tanggal){
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);
$pecahkan = explode('-', $tanggal);
return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
@endphp

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h4 style="margin-bottom: 30px;">Kelola Data Pelamar</h4>
                    </div>
                </div>
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Nama Pelamar </th>
                                <th> Posisi </th>
                                <th> Tahap </th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($datas) == 0)
                            <tr>
                            <td colspan="5" class="text-center">Tidak ada data</td>
                            </tr>
                            @else
                            @php
                            $no = 1;
                            @endphp
                            @foreach($datas as $data)
                            
                            <tr>
                            <td>{{$no++}}</td>
                            <td>
                                <p>{{$data->applicant->name}}</p>
                            </td>
                            <td>
                                <p>{{$data->job_vacancy->title}}</p>
                            </td>
                            <td>
                                @if($data->status == 'pendaftaran')
                                <div class="progress" data-toggle="tooltip" data-placement="bottom" title="Baru saja mendaftar">
                                    <div role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-dark" style="width: 20%;"></div>
                                </div>
                                @elseif($data->status == 'wawancara_1')
                                <div class="progress" data-toggle="tooltip" data-placement="bottom" title="Sedang dalam tahap wawancara pertama">
                                    <div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-info" style="width: 40%;"></div>
                                </div>
                                @elseif($data->status == 'psikotest')
                                <div class="progress" data-toggle="tooltip" data-placement="bottom" title="Sedang dalam tahap pengerjaan soal psikotest">
                                    <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-primary" style="width: 60%;"></div>
                                </div>
                                @elseif($data->status == 'keterampilan')
                                <div class="progress" data-toggle="tooltip" data-placement="bottom" title="Sedang dalam tahap uji keterampilan">
                                    <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-warning" style="width: 80%;"></div>
                                </div>
                                @elseif($data->status == 'wawancara_2')
                                <div class="progress" data-toggle="tooltip" data-placement="bottom" title="Sedang dalam tahap wawancara kedua">
                                    <div role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-success" style="width: 95%;"></div>
                                </div>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#profilModal{{$data->id}}">Lihat Profil</button>
                                @if($data->status == 'pendaftaran')
                                <button class="btn btn-success" data-toggle="modal" data-target="#wawancaraSatuModal{{$data->id}}">Lanjut ke Wawancara 1</button>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                                @elseif($data->status == 'wawancara_1')
                                <button class="btn btn-success" data-toggle="modal" data-target="#psikotestModal{{$data->id}}">Lanjut ke Psikotest</button>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                                @elseif($data->status == 'psikotest')
                                @if($data->applicant_psikotest->status == 'done')
                                <button class="btn btn-dark" data-toggle="modal" data-target="#lihatHasilPsikotestModal{{$data->applicant_psikotest->id}}">Lihat Hasil</button>
                                <button class="btn btn-success" data-toggle="modal" data-target="#ujiKeterampilanModal{{$data->applicant_psikotest->id}}">Lanjut ke Uji Keterampilan</button>
                                @else
                                <button class="btn btn-dark" data-toggle="modal" data-target="#lihatHasilPsikotestModal{{$data->applicant_psikotest->id}}">Lihat Hasil</button>
                                @endif
                                <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                                @elseif($data->status == 'keterampilan')
                                @if($data->applicant_test->status == 'done')
                                <button class="btn btn-success" data-toggle="modal" data-target="#wawancaraDuaModal{{$data->id}}">Lanjut ke Wawancara 2</button>
                                <button class="btn btn-dark" data-toggle="modal" data-target="#lihatHasilKeterampilanModal{{$data->applicant_test->id}}">Lihat Hasil</button>
                                @else
                                <button class="btn btn-dark" data-toggle="modal" data-target="#lihatHasilKeterampilanModal{{$data->applicant_test->id}}">Lihat Hasil</button>
                                @endif
                                <button class=" btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                                @elseif($data->status == 'wawancara_2')
                                <button class="btn btn-success" data-toggle="modal" data-target="#acceptModal{{$data->id}}">Terima</button>
                                <button class=" btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}">Tolak</button>
                                @endif
                            </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($datas as $data)
<div class="modal fade" id="profilModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="profilModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 95%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="profilModalLabel">Detail Profil <b>{{$data->applicant->name}}</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img class="rounded-circle" style="width: 200px;" src="{{asset($data->applicant->profile_picture);}}" alt="{{$data->applicant->name}}">
                </div>
                <hr>
                <h4>Data Diri</h4>
                <div class="row mb-2">
                    <div class="col-4">Nama </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7"><b>{{$data->applicant->name}}</b></div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. KTP </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->id_card_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. KK </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->family_card_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. Telepon </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->phone_number}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Alamat KTP </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->ktp_address}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Alamat Saat Ini </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->current_address}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Agama </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->religion}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Usia </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->age}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Jenis Kelamin </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{ucfirst($data->applicant->gender)}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Tempat, Tanggal Lahir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->place_of_birth.', '.tgl_indo($data->applicant->date_of_birth)}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Pendidikan Terakhir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->last_education}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Jurusan </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->major}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Tempat Pendidikan Terakhir </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->last_place_education}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">Nama Ibu </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->mother_name}}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-4">No. Rekening </div>
                    <div style="flex: 0 0 8.33333333%;max-width: 1.33333333%;">:</div>
                    <div class="col-7">{{$data->applicant->account_number}} ({{$data->applicant->bank}})</div>
                </div>

                <hr>
                <h4>Dokumen</h4>
                <div class="row text-center">
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_id_card) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>KTP</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_family_card) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>KK</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_cv) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>CV</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_certificate_of_education) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>Ijazah</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_transcript) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>Transkrip</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_taxpayer) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>NPWP</p>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="{{ url($data->applicant->doc_bpjs) }}" target="_blank">
                            <img src="{{ url('dashboard/assets/images/pdf.png') }}" style="width: 80px;">
                            <p>BPJS</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="deleteModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menolak lamaran ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/tolak-lamaran/{{$data->id}}"><button class="btn btn-danger">Ya, Tolak</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="acceptModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="acceptModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="acceptModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menerima lamaran ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/hrd/terima-lamaran/{{$data->id}}"><button class="btn btn-success">Ya, Terima</button></a>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="psikotestModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="psikotestModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/hrd/lanjut-psikotest/{{$data->id}}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="psikotestModalLabel">Form Lanjut ke Psikotest</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Form Psikotest</h4>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-5">
                                <label>Deadline Pengerjaan</label>
                            </div>
                            <div class="col-7">
                                <input type="datetime-local" class="form-control" name="date">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-5">
                                <label>Psikotest</label>
                            </div>
                            <div class="col-7">
                                <select name="psikotest" class="form-control" style="height:2.9rem">
                                    @foreach($psikotests as $psikotest)
                                    <option value="{{$psikotest->id}}">{{$psikotest->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <input type="submit" class="btn btn-success" value="Ya, Lanjutkan">
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
@if($data->applicant_psikotest != null)
<div class="modal fade" id="ujiKeterampilanModal{{$data->applicant_psikotest->id}}" tabindex="-1" role="dialog" aria-labelledby="ujiKeterampilanModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ujiKeterampilanModalLabel">Form Lanjut ke Uji Keterampilan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/hrd/lanjut-uji-keterampilan/{{$data->id}}" method="POST">
                @csrf
                <div class="modal-body">
                    <h4>Form Uji Keterampilan</h4>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-5">
                                <label>Deadline Pengerjaan</label>
                            </div>
                            <div class="col-7">
                                <input type="datetime-local" class="form-control" name="date">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-5">
                                <label>Uji Keterampilan</label>
                            </div>
                            <div class="col-7">
                                <select name="test" class="form-control" style="height:2.9rem">
                                    @foreach($tests as $test)
                                    <option value="{{$test->id}}">{{$test->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <input type="submit" value="Ya, Lanjutkan" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endforeach

@foreach($datas as $data)
<div class="modal fade" id="wawancaraDuaModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="wawancaraDuaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wawancaraDuaModalLabel">Form Lanjut ke Wawancara kedua</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/hrd/lanjut-wawancara-dua/{{$data->id}}" method="POST">
                @csrf
                <div class="modal-body">
                    <h4>Form Wawancara 2</h4>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-4">
                                <label>Waktu</label>
                            </div>
                            <div class="col-8">
                                <input type="datetime-local" class="form-control" name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Link Zoom</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="link">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>ID Room</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="room_id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Password Room</label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" name="room_password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <input type="submit" value="Ya, Lanjutkan" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach($datas as $data)
@if($data->status == 'psikotest')
<div class="modal fade" id="lihatHasilPsikotestModal{{$data->applicant_psikotest->id}}" tabindex="-1" role="dialog" aria-labelledby="wawancaraDuaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wawancaraDuaModalLabel">Hasil Psikotest <b>{{$data->applicant->name}}</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($data->applicant_psikotest->status == 'not_started')
                <p class="text-center">{{$data->applicant->name}} belum memulai psikotest!</p>
                @elseif($data->applicant_psikotest->status == 'started')
                <p class="text-center">{{$data->applicant->name}} sedang psikotest</p>
                @elseif($data->applicant_psikotest->status == 'fail')
                <p class="text-center">{{$data->applicant->name}} gagal dalam mengerjakan psikotest!</p>
                @elseif($data->applicant_psikotest->status == 'late')
                <p class="text-center">{{$data->applicant->name}} tidak mengerjakan psikotest pada waktu yang telah ditentukan.</p>
                @elseif($data->applicant_psikotest->status == 'done')
                <h4 class="font-weight-light">Mask Public Self (Dalam)</h4>
                @foreach($resultPsikotests as $resultPsikotest)
                @if($resultPsikotest->key == $data->applicant_psikotest->result_p)
                <p><b>{{$resultPsikotest->title}}</b></p>
                <ul>{!!$resultPsikotest->definition!!}</ul>
                @elseif($data->applicant_psikotest->result_p == null)
                <p class="text-center">Kepribadian tidak ada yang cocok dengan hasil</p>
                @php
                break;
                @endphp
                @endif
                @endforeach
                <h4 class="font-weight-light">Core Private Self (Luar)</h4>
                @foreach($resultPsikotests as $resultPsikotest)
                @if($resultPsikotest->key == $data->applicant_psikotest->result_k)
                <p><b>{{$resultPsikotest->title}}</b></p>
                <ul>{!!$resultPsikotest->definition!!}</ul>
                @elseif($data->applicant_psikotest->result_k == null)
                <p class="text-center">Kepribadian tidak ada yang cocok dengan hasil</p>
                @php
                break;
                @endphp
                @endif
                @endforeach
                <h4 class="font-weight-light">Mirror Perceived Self</h4>
                @foreach($resultPsikotests as $resultPsikotest)
                @if($resultPsikotest->key == $data->applicant_psikotest->result_pk)
                <p><b>{{$resultPsikotest->title}}</b></p>
                <ul>{!!$resultPsikotest->definition!!}</ul>
                @elseif($data->applicant_psikotest->result_pk == null)
                <p class="text-center">Kepribadian tidak ada yang cocok dengan hasil</p>
                @php
                break;
                @endphp
                @endif
                @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endif

@if($data->status == 'keterampilan')
<div class="modal fade" id="lihatHasilKeterampilanModal{{$data->applicant_test->id}}" tabindex="-1" role="dialog" aria-labelledby="wawancaraDuaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wawancaraDuaModalLabel">Hasil Keterampilan <b>{{$data->applicant->name}}</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($data->applicant_test->status == 'not_started')
                <p class="text-center">{{$data->applicant->name}} belum memulai Uji Keterampilan!</p>
                @elseif($data->applicant_test->status == 'started')
                <p class="text-center">{{$data->applicant->name}} sedang melaksanakan ujian</p>
                @elseif($data->applicant_test->status == 'fail')
                <p class="text-center">{{$data->applicant->name}} gagal dalam mengerjakan ujian keterampilan!</p>
                @elseif($data->applicant_test->status == 'late')
                <p class="text-center">{{$data->applicant->name}} tidak mengerjakan ujian keterampilan pada waktu yang telah ditentukan.</p>
                @elseif($data->applicant_test->status == 'done')
                <p class="text-center">{{$data->applicant->name}} berhasil mendapatkan score <b>{{$data->applicant_test->score}}</b>.</p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

@endpush