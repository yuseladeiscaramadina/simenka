@extends('templates.dashboard-hrd')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-10">
            <h4 style="margin-bottom: 30px;">Tambah Soal <b>{{$psikotest->name}}</b></h4>

            <form action="/hrd/tambah-soal-psikotest/{{$psikotest->id}}" method="POST">
              @csrf

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Soal</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <input type="text" name="question" id="question" class="form-control" placeholder="ex: Gambaran Diri" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Pilihan A</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <input type="text" name="option_a" id="option_a" class="form-control" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan A (P)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_a_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan A (K)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_a_not_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Pilihan B</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <input type="text" name="option_b" id="option_b" class="form-control" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan B (P)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_b_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan B (K)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_b_not_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Pilihan C</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <input type="text" name="option_c" id="option_c" class="form-control" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan C (P)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_c_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan C (K)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_c_not_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Pilihan D</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <input type="text" name="option_d" id="option_d" class="form-control" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan D (P)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_d_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <label>Jenis Pilihan D (K)</label>
                  </div>
                  <div class="col-lg-8 col-md-12">
                    <select name="question_type_d_not_me" class="form-control" style="height: 3rem;">
                      <option value="d">D</option>
                      <option value="i">I</option>
                      <option value="s">S</option>
                      <option value="c">C</option>
                      <option value="*">*</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary float-right">Simpan</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
@endpush