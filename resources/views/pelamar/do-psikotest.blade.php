<!DOCTYPE html>
<html>

<head>
  <title>Simenka</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="_token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

  <!-- plugin css -->
  {!! Html::style('dashboard/assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
  {!! Html::style('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
  <!-- end plugin css -->

  <!-- common css -->
  {!! Html::style('dashboard/css/app.css') !!}
  <!-- end common css -->
</head>

<body data-base-url="{{url('/')}}" id="psikotest-body" onclick="fullScreen()">

  <div class="container-scroller" id="app">
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo">
          <img src="{{ url('dashboard/assets/images/logo.svg') }}" alt="logo" /> </a>
        <a class="navbar-brand brand-logo-mini">
          <img src="{{ url('dashboard/assets/images/logo.svg') }}" style="width:80px;" alt="logo" /> </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span id="text-timer" class="profile-text" style="font-size: 20px;"><i class="menu-icon mdi mdi-timer"></i> 00:00:00</span>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <a href="/failed-psikotest" class="dropdown-item"> Keluar </a>
              </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu icon-menu"></span>
        </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile not-navigation-link">
            <div class="row mt-4 d-flex justify-content-center text-center">
              @php $no = 1; @endphp
              @foreach($psikotest->psikotestQuestion as $psikotestQuestion)
              @if($psikotestQuestion->id == $psikotestQuestionNow->id)
              <div class="col-3 bg-primary mr-1 mb-1 d-flex" style="width: 100%; height:60px;">
                <p class="mx-auto my-auto text-white">{{$no++}}</p>
              </div>
              @elseif($psikotestQuestion->id < $psikotestQuestionNow->id)
                <div class="col-3 bg-success mr-1 mb-1 d-flex" style="width: 100%; height:60px;">
                  <p class="mx-auto my-auto text-white">{{$no++}}</p>
                </div>
                @else
                <div class="col-3 bg-dark mr-1 mb-1 d-flex" style="width: 100%; height:60px;">
                  <p class="mx-auto my-auto text-white">{{$no++}}</p>
                </div>
                @endif
                @endforeach
            </div>
            <div class="nav-link">

              <div class="user-wrapper">

              </div>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-12">
                      <h2 style="margin-bottom: 30px;">{{$psikotestQuestionNow->question}}</h2>
                    </div>
                  </div>
                  @if($progressPK == 'p')
                  <h4>Manakah dari pilihan berikut yang paling <b>menggambarkan</b> diri anda?</h4>

                  <form id="formPsikotest" action="/ikuti-psikotest" method="POST">
                    @csrf
                    <input type="hidden" name="is_refreshed" id="refresh" value="{{$randString}}">
                    <div class="row">
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio1" value="option_a|{{$psikotestQuestionNow->option_type_a_me}}" required>
                          <label class="form-check-label" for="psikotestRadio1">
                            {{$psikotestQuestionNow->option_a}}
                          </label>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio2" value="option_b|{{$psikotestQuestionNow->option_type_b_me}}" required>
                          <label class="form-check-label" for="psikotestRadio2">
                            {{$psikotestQuestionNow->option_b}}
                          </label>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio3" value="option_c|{{$psikotestQuestionNow->option_type_c_me}}" required>
                          <label class="form-check-label" for="psikotestRadio3">
                            {{$psikotestQuestionNow->option_c}}
                          </label>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio4" value="option_d|{{$psikotestQuestionNow->option_type_d_me}}" required>
                          <label class="form-check-label" for="psikotestRadio4">
                            {{$psikotestQuestionNow->option_d}}
                          </label>
                        </div>
                      </div>
                      <div class="col-12">
                        <button class="btn btn-primary float-right" onclick="submitPsikotest();">Selanjutnya</button>
                      </div>
                    </div>
                  </form>
                  @elseif($progressPK == 'k')
                  <h4>Manakah dari pilihan berikut yang paling <b>tidak menggambarkan</b> diri anda?</h4>

                  <form id="formPsikotest" action="/ikuti-psikotest" method="POST">
                    @csrf
                    <input type="hidden" name="is_refreshed" id="refresh" value="{{$randString}}">
                    <div class="row">
                      @if($lastAnswer != 'option_a')
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio1" value="option_a|{{$psikotestQuestionNow->option_type_a_not_me}}" required>
                          <label class="form-check-label" for="psikotestRadio1">
                            {{$psikotestQuestionNow->option_a}}
                          </label>
                        </div>
                      </div>
                      @endif

                      @if($lastAnswer != 'option_b')
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio2" value="option_b|{{$psikotestQuestionNow->option_type_b_not_me}}" required>
                          <label class="form-check-label" for="psikotestRadio2">
                            {{$psikotestQuestionNow->option_b}}
                          </label>
                        </div>
                      </div>
                      @endif

                      @if($lastAnswer != 'option_c')
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio3" value="option_c|{{$psikotestQuestionNow->option_type_c_not_me}}" required>
                          <label class="form-check-label" for="psikotestRadio3">
                            {{$psikotestQuestionNow->option_c}}
                          </label>
                        </div>
                      </div>
                      @endif

                      @if($lastAnswer != 'option_d')
                      <div class="col-6">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="psikotestRadio" id="psikotestRadio4" value="option_d|{{$psikotestQuestionNow->option_type_d_not_me}}" required>
                          <label class="form-check-label" for="psikotestRadio4">
                            {{$psikotestQuestionNow->option_d}}
                          </label>
                        </div>
                      </div>
                      @endif
                      <div class="col-12">
                        <input type="submit" value="Selanjutnya" class="btn btn-primary float-right">
                      </div>
                    </div>
                  </form>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- base js -->
  {!! Html::script('dashboard/js/app.js') !!}
  {!! Html::script('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
  <!-- end base js -->

  <!-- plugin js -->
  @stack('plugin-scripts')
  <!-- end plugin js -->

  <!-- common js -->
  {!! Html::script('dashboard/assets/js/off-canvas.js') !!}
  {!! Html::script('dashboard/assets/js/hoverable-collapse.js') !!}
  {!! Html::script('dashboard/assets/js/misc.js') !!}
  {!! Html::script('dashboard/assets/js/settings.js') !!}
  {!! Html::script('dashboard/assets/js/todolist.js') !!}
  <!-- end common js -->

  {!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
  {!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}

  {!! Html::script('/dashboard/assets/js/dashboard.js') !!}

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script>

    $(document).on("keydown", function(e) {
      if (e.key == "F5" ||
        (e.ctrlKey == true && (e.key == 'r' || e.key == 'R')) ||
        e.keyCode == 116 || e.keyCode == 82) {

        e.preventDefault();
      }
    });

    $(document).ready(function() {
      if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
        window.location = "/failed-psikotest";
      }

      setInterval(function() {
        $.ajax({
          type: 'get',
          url: '/waktu-psikotest',
          data: {
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            if (data.time != 'undefined') {
              if (data.time != 'timeout') {
                document.getElementById('text-timer').innerHTML = `<i class="menu-icon mdi mdi-timer"></i> ${data.time}`;
              } else {
                window.location = "/failed-psikotest";
              }
            }
          }
        })
      }, 1000);
    })
  </script>
</body>

</html>