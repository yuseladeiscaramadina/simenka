<!DOCTYPE html>
<html>

<head>
  <title>Simenka</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="_token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

  <!-- plugin css -->
  {!! Html::style('dashboard/assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
  {!! Html::style('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
  <!-- end plugin css -->

  <!-- common css -->
  {!! Html::style('dashboard/css/app.css') !!}
  <!-- end common css -->
</head>

<body data-base-url="{{url('/')}}" id="psikotest-body" onclick="fullScreen()">

  <div class="container-scroller" id="app">
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo">
          <img src="{{ url('dashboard/assets/images/logo.svg') }}" alt="logo" /> </a>
        <a class="navbar-brand brand-logo-mini">
          <img src="{{ url('dashboard/assets/images/logo.svg') }}" style="width:80px;" alt="logo" /> </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span id="text-timer" class="profile-text" style="font-size: 20px;"><i class="menu-icon mdi mdi-timer"></i> 00:00:00</span>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <a href="/failed-test" class="dropdown-item"> Keluar </a>
              </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu icon-menu"></span>
        </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile not-navigation-link">
            <div class="row mt-4 d-flex justify-content-center text-center">
              @php $no = 1; @endphp
              @foreach($tests->testQuestion as $testQuestion)
              @if($testQuestion->id == $testQuestionNow->id)
              <div class="col-3 bg-primary mr-1 mb-1 d-flex" style="width: 100%; height:60px;">
                <p class="mx-auto my-auto text-white">{{$no++}}</p>
              </div>
              @elseif($testQuestion->id < $testQuestionNow->id)
                <div class="col-3 bg-success mr-1 mb-1 d-flex" style="width: 100%; height:60px;" onclick="changeQuestion({{$testQuestion->id}})">
                  <p class="mx-auto my-auto text-white">{{$no++}}</p>
                </div>
                @elseif(array_key_exists($testQuestion->id, session()->get('answer_test')))
                <div class="col-3 bg-success mr-1 mb-1 d-flex" style="width: 100%; height:60px;" onclick="changeQuestion({{$testQuestion->id}})">
                  <p class="mx-auto my-auto text-white">{{$no++}}</p>
                </div>
                @else
                <div class="col-3 bg-dark mr-1 mb-1 d-flex" style="width: 100%; height:60px;">
                  <p class="mx-auto my-auto text-white">{{$no++}}</p>
                </div>
                @endif
                @endforeach
            </div>
            <div class="nav-link">

              <div class="user-wrapper">

              </div>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-12">
                      @if($testQuestionNow->image != null)
                      <div class="text-center">
                        <img src="{{$testQuestionNow->image}}" class="mb-3" alt="Test Question" style="height: 400px;">
                      </div>
                      @endif
                      <h4 style="margin-bottom: 30px;">{{$testQuestionNow->question}}</h4>

                      <form action="/ikuti-uji-keterampilan" method="POST">
                        @csrf
                        <input type="hidden" value="{{$testQuestionNow->id}}" name="question_id">
                        <div class="row">
                          <div class="col-6">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="testRadio" id="testRadio1" value="a" required>
                              <label class="form-check-label" for="testRadio1">
                                {{$testQuestionNow->option_a}}
                              </label>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="testRadio" id="testRadio2" value="b" required>
                              <label class="form-check-label" for="testRadio2">
                                {{$testQuestionNow->option_b}}
                              </label>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="testRadio" id="testRadio3" value="c" required>
                              <label class="form-check-label" for="testRadio3">
                                {{$testQuestionNow->option_c}}
                              </label>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="testRadio" id="testRadio4" value="d" required>
                              <label class="form-check-label" for="testRadio4">
                                {{$testQuestionNow->option_d}}
                              </label>
                            </div>
                          </div>
                          <div class="col-12">
                            <input type="submit" value="Selanjutnya" class="btn btn-primary float-right">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <form action="/ikuti-uji-keterampilan" method="POST" id="changeQuestion" style="visibility: hidden;">
    @csrf
    <input type="hidden" value="true" name="change_question" />
    <input type="text" name="question_id" id="question">
  </form>

  <!-- base js -->
  {!! Html::script('dashboard/js/app.js') !!}
  {!! Html::script('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
  <!-- end base js -->

  <!-- plugin js -->
  @stack('plugin-scripts')
  <!-- end plugin js -->

  <!-- common js -->
  {!! Html::script('dashboard/assets/js/off-canvas.js') !!}
  {!! Html::script('dashboard/assets/js/hoverable-collapse.js') !!}
  {!! Html::script('dashboard/assets/js/misc.js') !!}
  {!! Html::script('dashboard/assets/js/settings.js') !!}
  {!! Html::script('dashboard/assets/js/todolist.js') !!}
  <!-- end common js -->

  {!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
  {!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}

  {!! Html::script('/dashboard/assets/js/dashboard.js') !!}

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script>
    function changeQuestion(question) {
      document.getElementById('question').value = question;
      $('#changeQuestion').submit();
    }


    $(document).on("keydown", function(e) {
      if (e.key == "F5" ||
        (e.ctrlKey == true && (e.key == 'r' || e.key == 'R')) ||
        e.keyCode == 116 || e.keyCode == 82) {

        e.preventDefault();
      }
    });

    $(document).ready(function() {
      setInterval(function() {
        $.ajax({
          type: 'get',
          url: '/waktu-test',
          data: {
            _token: "{{ csrf_token() }}",
          },
          success: function(data) {
            if (data.time != 'undefined') {
              if (data.time != 'timeout') {
                document.getElementById('text-timer').innerHTML = `<i class="menu-icon mdi mdi-timer"></i> ${data.time}`;
              } else {
                window.location = "/failed-test";
              }
            }
          }
        })
      }, 1000);
    })
  </script>
</body>

</html>