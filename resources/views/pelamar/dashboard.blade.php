@php
function changeFormatDateIndonesia($date)
{
$bulan = array(
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);
$exp = explode(' ', $date);
$resDate = $exp[0];

$expResDate = explode('/', $resDate);
return $expResDate[2] . ' ' . $bulan[(int)$expResDate[1]] . ' ' . $expResDate[0] . ', jam ' . $exp[1] . ' ' . $exp[2];
}
@endphp
@extends('templates.dashboard-pelamar')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center mb-4">
                    @if($application != null)
                    <h2 class="card-title mb-0">Lamaran Aktif - {{$application->job_vacancy->title}}
                        @if($application->status == 'pendaftaran')
                        <span class="badge badge-danger">Seleksi Berkas</span>
                        @elseif($application->status == 'wawancara_1')
                        <span class="badge badge-danger">Wawancara 1</span>
                        @elseif($application->status == 'psikotest')
                        <span class="badge badge-danger">Psikotest</span>
                        @elseif($application->status == 'keterampilan')
                        <span class="badge badge-danger">Uji Keterampilan</span>
                        @elseif($application->status == 'wawancara_2')
                        <span class="badge badge-danger">Wawancara 2</span>
                        @endif
                    </h2>
                    @else
                    <h2 class="card-title mb-0">Lamaran Aktif</h2>
                    @endif
                </div>
                @if($application == null)
                <div class="text-center font-weight-light">Anda tidak memiliki Lamaran yang sedang aktif</div>
                @elseif($application->status == 'pendaftaran')
                <div class="text-center font-weight-light">Anda sedang dalam tahap pendaftaran! Silahkan menunggu HRD menyeleksi berkas anda.</div>
                @elseif($application->status == 'wawancara_1')
                @foreach($application->interview as $interview)
                @if($interview->many_interview == '1')
                <div class="row">
                    <div class="col-2">
                        <img class="float-right" src="https://play-lh.googleusercontent.com/V1lb4J811SNh6hlGqNlDnGZXfdtI2i-1dC7_i9TnViyiboZ2RHp8xbhKbbECG-f26is" style="width: 100px;">
                    </div>
                    <div class="col-10">
                        @php
                        $date = date_create($interview->event_date);
                        $dateFormat = changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));
                        @endphp
                        <p>Selamat! Anda berhasil melewati seleksi berkas dan sekarang sedang dalam wawancara 1. Silahkan mengikuti wawancara pada tanggal <b>{{$dateFormat}}.</b> Silahkan mengikuti wawancara pada zoom berikut.</p>
                        <p class="mb-0">Link: <a href="{{$interview->link}}" target="__blank">{{$interview->link}}</a></p>
                        <p class="mb-0">Meeting ID: {{$interview->meeting_id}}</p>
                        <p class="mb-0">Password: {{$interview->password}}</p>
                    </div>
                </div>
                @endif
                @endforeach
                @elseif($application->status == 'psikotest')
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <img src="{{asset('beranda/assets/illustration/illust-do-psikotest.jpg')}}" style="width: 300px;">

                            @php
                            $date = date_create($application->deadline);
                            $dateFormat = changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));
                            @endphp
                            <p>Selamat! Anda berhasil melewati seleksi Wawancara dan sekarang sedang dalam tahap pengerjaan Psikotest. Silahkan mengikuti Psikotest dibawah ini sebelum tanggal <b>{{$dateFormat}}.</b></p>

                            @if($test != null)
                            @if($test->status == 'not_started')
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#psikotestModal">Ikuti Psikotest!</a>
                            @elseif($test->status == 'done')
                            <a href="#" class="btn btn-outline-primary">Anda telah mengikuti Psikotest</a>
                            @elseif($test->status == 'fail')
                            <a href="#" class="btn btn-outline-primary">Anda telah mengikuti Psikotest</a>
                            @elseif($test->status == 'late')
                            <a href="#" class="btn btn-outline-danger">Anda terlambat mengikuti Psikotest</a>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @elseif($application->status == 'keterampilan')
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <img src="{{asset('beranda/assets/illustration/illust-do-psikotest.jpg')}}" style="width: 300px;">

                            @php
                            $date = date_create($application->deadline);
                            $dateFormat = changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));
                            @endphp
                            <p>Selamat! Anda berhasil melewati seleksi Psikotest dan sekarang sedang dalam tahap pengerjaan Uji Keterampilan. Silahkan mengikuti Uji Keterampilan dibawah ini sebelum tanggal <b>{{$dateFormat}}.</b></p>

                            @if($test != null)
                            @if($test->status == 'not_started')
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#ujiKeterampilanModal">Ikuti Uji Keterampilan!</a>
                            @elseif($test->status == 'done')
                            <a href="#" class="btn btn-outline-primary">Anda telah mengikuti Uji Keterampilan</a>
                            @elseif($test->status == 'fail')
                            <a href="#" class="btn btn-outline-primary">Anda telah mengikuti Uji Keterampilan</a>
                            @elseif($test->status == 'late')
                            <a href="#" class="btn btn-outline-danger">Anda terlambat mengikuti Uji Keterampilan</a>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @elseif($application->status == 'wawancara_2')
                @foreach($application->interview as $interview)
                @if($interview->many_interview == '2')
                <div class="row">
                    <div class="col-2">
                        <img class="float-right" src="https://play-lh.googleusercontent.com/V1lb4J811SNh6hlGqNlDnGZXfdtI2i-1dC7_i9TnViyiboZ2RHp8xbhKbbECG-f26is" style="width: 100px;">
                    </div>
                    <div class="col-10">
                        @php
                        $date = date_create($interview->event_date);
                        $dateFormat = changeFormatDateIndonesia(date_format($date, "Y/m/d h:i A"));
                        @endphp
                        <p>Selamat! Anda berhasil melewati seleksi uji keterampilan dan sekarang sedang dalam wawancara akhir. Silahkan mengikuti wawancara pada tanggal <b>{{$dateFormat}}.</b> Silahkan mengikuti wawancara pada zoom berikut.</p>
                        <p class="mb-0">Link: <a href="{{$interview->link}}" target="__blank">{{$interview->link}}</a></p>
                        <p class="mb-0">Meeting ID: {{$interview->meeting_id}}</p>
                        <p class="mb-0">Password: {{$interview->password}}</p>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

@if($application!=null)
@if($application->status == 'psikotest')
<div class="modal fade" id="psikotestModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda akan langsung mengikuti ujian Psikotest selama <b>{{$test->psikotest->time}} Menit</b>. Selama mengikuti Psikotest dilarang keluar atau melakukan refresh halaman dari website sampai psikotest selesai atau waktu habis. Jika anda keluar atau melakukan refresh halaman sebelum waktu yang telah ditentukan, psikotest anda akan dianggap gagal.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/ikuti-psikotest"><button class="btn btn-danger">Ya, saya yakin</button></a>
            </div>
        </div>
    </div>
</div>
@endif

@if($application->status == 'keterampilan')
<div class="modal fade" id="ujiKeterampilanModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda akan langsung mengikuti uji keterampilan selama <b>600 Menit</b>. Selama mengikuti uji keterampilan dilarang keluar atau melakukan refresh halaman dari website sampai ujian selesai atau waktu habis. Jika anda keluar atau melakukan refresh halaman sebelum waktu yang telah ditentukan, uji keterampilan anda akan dianggap gagal.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="/ikuti-uji-keterampilan"><button class="btn btn-danger">Ya, saya yakin</button></a>
            </div>
        </div>
    </div>
</div>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'cancel-psikotest')
<div class="modal fade" id="psikotestCancelModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Selamat!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Terimakasih telah mengikuti Psikotest kami. Anda telah keluar atau waktu yang diberikan untuk Psikotest telah habis. Silahkan untuk menunggu kabar selanjutnya oleh HRD.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Siap</button>
            </div>
        </div>
    </div>
</div>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'cancel-test')
<div class="modal fade" id="testCancelModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Selamat!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Terimakasih telah mengikuti test uji keterampilan kami. Anda telah keluar atau waktu yang diberikan untuk ujian telah habis. Silahkan untuk menunggu kabar selanjutnya oleh HRD.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Siap</button>
            </div>
        </div>
    </div>
</div>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'done-psikotest')
<div class="modal fade" id="psikotestDoneModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Selamat!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Terimakasih telah mengikuti Psikotest kami. Anda telah berhasil menyelesaikan psikotest yang telah kami sediakan. Silahkan untuk menunggu kabar selanjutnya oleh HRD.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Siap</button>
            </div>
        </div>
    </div>
</div>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'done-test')
<div class="modal fade" id="testDoneModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark" style="color: white;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Selamat!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Terimakasih telah mengikuti Uji Keterampilan kami. Anda telah berhasil menyelesaikan uji keterampilan yang telah kami sediakan. Silahkan untuk menunggu kabar selanjutnya oleh HRD.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Siap</button>
            </div>
        </div>
    </div>
</div>
@endif
@endif

@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
@endpush

@section('extra-js')
@if(Session::get('msg'))
@if(Session::get('msg') == 'cancel-psikotest')
<script>
    $('#psikotestCancelModal').modal('show');
</script>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'cancel-test')
<script>
    $('#testCancelModal').modal('show');
</script>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'done-psikotest')
<script>
    $('#psikotestDoneModal').modal('show');
</script>
@endif
@endif

@if(Session::get('msg'))
@if(Session::get('msg') == 'done-test')
<script>
    $('#testDoneModal').modal('show');
</script>
@endif
@endif
@endsection