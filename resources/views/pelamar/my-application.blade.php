@extends('templates.dashboard-pelamar')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@php
function tgl_indo($tanggal){
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);
$pecahkan = explode('-', $tanggal);
return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
@endphp

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <h4 style="margin-bottom: 30px;">Riwayat Lamaran Anda</h4>
          </div>
        </div>
        @if(Session::get('msg'))
        <div class="alert alert-{!!session::get('type')!!}" role="alert">
          {!!session('msg')!!}
        </div>
        @endif
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th> # </th>
                <th> Nama Pelamar </th>
                <th> Posisi </th>
                <th> Status </th>
              </tr>
            </thead>
            <tbody>
              @if(count($datas) == 0)
              <td colspan="5" class="text-center">Tidak ada data</td>
              @else
              @php
              $no = 1;
              @endphp
              @foreach($datas as $data)
              <td>{{$no++}}</td>
              <td>
                <p>{{$data->applicant->name}}</p>
              </td>
              <td>
                <p>{{$data->job_vacancy->title}}</p>
              </td>
              <td>
                @if($data->status == 'diterima')
                <span class="badge badge-success">Diterima</span>
                @elseif($data->status == 'ditolak')
                <span class="badge badge-danger">Ditolak</span>
                @endif
              </td>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}

@endpush