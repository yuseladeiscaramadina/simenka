@extends('templates.dashboard-pelamar')

@push('plugin-styles')
<!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('extra-style')
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
<style>
  .profile-image {
    width: 10%;
  }

  .edit-image {
    width: 2%;
    position: absolute;
    left: 600px;
    top: 0px;
  }

  /* .dz-max-files-reached {
    pointer-events: none;
    cursor: default;
  } */

  .dropzone {
    border: 2px dashed #028AF4 !important;
    ;
  }

  @media only screen and (max-width: 600px) {
    .profile-image {
      width: 50%;
    }

    .edit-image {
      width: 10%;
      position: absolute;
      left: 100px;
      top: 0px;
    }
  }
</style>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <div id="change-picture">
              <img class="profile-image rounded-circle" src="{{asset(Auth::user()->profile_picture)}}" onclick="changePicture();">
              <img class="edit-image" src="{{asset('beranda/assets/illustration/illust-pencil.png')}}" onclick="changePicture();">
              <form method="POST">
                <input type="file" name="profile_picture" id="profile_picture" accept="image/*" style="display:none;">
              </form>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <h3 class="font-weight-light mb-3">Data Diri</h3>
            @if(Session::get('msg'))
            <div class="alert alert-{!!session::get('type')!!}" role="alert">
              {!!session('msg')!!}
            </div>
            @endif
            <form action="/profilku" method="POST">
              @csrf
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Nama</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="name" id="name" class="form-control" value="{{Auth::user()->name}}" readonly required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Email</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="email" id="email" class="form-control" value="{{Auth::user()->email}}" readonly required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>No. KTP</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="id_card_number" id="id_card_number" class="form-control" value="{{Auth::user()->id_card_number}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>No. KK</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="family_card_number" id="family_card_number" class="form-control" value="{{Auth::user()->family_card_number}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Nomor Handphone</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="phone_number" id="phone_number" class="form-control" value="{{Auth::user()->phone_number}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Alamat KTP</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="ktp_address" id="ktp_address" class="form-control" value="{{Auth::user()->ktp_address}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Alamat Sekarang</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="current_address" id="current_address" class="form-control" value="{{Auth::user()->current_address}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Agama</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <select name="religion" id="religion" class="form-control" style="height: 3rem;" required>
                      @if(Auth::user()->religion == null)
                      <option value="">-- Pilih Salah Satu --</option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Khonghucu">Khonghucu</option>
                      @elseif(Auth::user()->religion == 'Islam')
                      <option value="Islam" selected>Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Khonghucu">Khonghucu</option>
                      @elseif(Auth::user()->religion == 'Kristen Protestan')
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan" selected>Kristen Protestan</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Khonghucu">Khonghucu</option>
                      @elseif(Auth::user()->religion == 'Hindu')
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Hindu" selected>Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Khonghucu">Khonghucu</option>
                      @elseif(Auth::user()->religion == 'Budha')
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha" selected>Budha</option>
                      <option value="Khonghucu">Khonghucu</option>
                      @elseif(Auth::user()->religion == 'Khonghucu')
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Khonghucu" selected>Khonghucu</option>
                      @endif
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Umur</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="number" name="age" id="age" class="form-control" value="{{Auth::user()->age}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Jenis Kelamin</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <select name="gender" id="gender" class="form-control" style="height: 3rem;" required>
                      @if(Auth::user()->gender == null)
                      <option value="">-- Pilih Salah Satu --</option>
                      <option value="pria">Pria</option>
                      <option value="wanita">Wanita</option>
                      @elseif(Auth::user()->gender == 'pria')
                      <option value="pria" selected>Pria</option>
                      <option value="wanita">Wanita</option>
                      @elseif(Auth::user()->gender == 'wanita')
                      <option value="pria">Pria</option>
                      <option value="wanita" selected>Wanita</option>
                      @endif
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Tempat Lahir</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="place_of_birth" id="place_of_birth" class="form-control" value="{{Auth::user()->place_of_birth}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Tanggal Lahir</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="{{Auth::user()->date_of_birth}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Pendidikan Terakhir</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="last_education" id="last_education" class="form-control" value="{{Auth::user()->last_education}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Jurusan Pendidikan</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="major" id="major" class="form-control" value="{{Auth::user()->major}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Tempat Pendidikan Terakhir</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="last_place_education" id="last_place_education" class="form-control" value="{{Auth::user()->last_place_education}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Nama Ibu</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="mother_name" id="mother_name" class="form-control" value="{{Auth::user()->mother_name}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Nomor Rekening</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="account_number" id="account_number" class="form-control" value="{{Auth::user()->account_number}}" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2 col-md-12">
                    <label>Nama Bank</label>
                  </div>
                  <div class="col-lg-10 col-md-12">
                    <input type="text" name="bank" id="bank" class="form-control" value="{{Auth::user()->bank}}" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary float-right">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row mt-5">
  <div class="col-12">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <h3 class="font-weight-light mb-3">Kelengkapan Dokumen</h3>
            <div class="row">
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">KTP</h4>
                <form action="/doc/ktp" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-ktp">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF KTP Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">KK</h4>
                <form action="/doc/kk" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-kk">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF KK Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">CV</h4>
                <form action="/doc/cv" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-cv">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF CV Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">Ijazah</h4>
                <form action="/doc/ijazah" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-ijazah">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF Ijazah Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">Transkrip</h4>
                <form action="/doc/transkrip" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-transkrip">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF Transkrip Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">NPWP</h4>
                <form action="/doc/npwp" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-npwp">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF NPWP Anda disini</span>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 col-md-12 mb-2">
                <h4 class="font-weight-light">BPJS</h4>
                <form action="/doc/bpjs" class="dropzone dz-clickable dz-max-files-reached text-center" id="dropzone-bpjs">
                  @csrf
                  <div class="dz-default dz-message" data-dz-message>
                    <span>Unggah Dokumen PDF BPJS Anda disini</span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Crop Foto Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="img-container">
          <div class="row">
            <div class="col-12 text-center">
              <img src="" id="profile_picture_display" class="fluid" />
            </div>
            <div class="col-md-4">
              <div class="preview"></div>
            </div>
          </div>
        </div>
      </div>
      <div id="footer-crop" class="modal-footer">
        <button type="button" id="crop" class="btn btn-primary">Crop</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak Jadi</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="failedModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Duh~</h5>
        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="{{asset('beranda/assets/illustration/illust-warning.jpg')}}" style="width: 70%;">
        <p>Gagal dalam mengganti foto profil! Silahkan coba beberapa saat lagi.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Oke deh</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
{!! Html::script('/dashboard/assets/plugins/chartjs/chart.min.js') !!}
{!! Html::script('/dashboard/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
{!! Html::script('/dashboard/assets/js/dashboard.js') !!}
@endpush

@section('extra-js')
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script src="https://unpkg.com/cropperjs"></script>
<script src="{{asset('dashboard/assets/js/document-manager.js')}}"></script>

<script>
  function changePicture() {
    $('#profile_picture').trigger('click');
  }

  $(document).ready(function() {
    const $modal = $('#modal');
    const image = document.getElementById('profile_picture_display');
    const footer = document.getElementById('footer-crop');
    var cropper;

    $('#profile_picture').change(function(event) {
      const files = event.target.files;

      const done = function(url) {
        document.getElementById("profile_picture").value = null;
        image.src = url;
        $modal.modal({
          backdrop: 'static',
          keyboard: false
        });
      };

      if (files && files.length > 0) {
        reader = new FileReader();
        reader.onload = function(event) {
          done(reader.result);
        };
        reader.readAsDataURL(files[0]);
      }
    });

    $modal.on('shown.bs.modal', function() {
      cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 1,
        preview: '.preview',
        data: {
          width: 200,
          height: 200
        }
      });
    }).on('hidden.bs.modal', function() {
      cropper.destroy();
      cropper = null;
    });

    $('#crop').click(function() {
      canvas = cropper.getCroppedCanvas({
        width: 400,
        height: 400
      });

      canvas.toBlob(function(blob) {
        url = URL.createObjectURL(blob);
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function() {
          const base64data = reader.result;
          $.ajax({
            url: '/ubah-foto-profil',
            method: 'POST',
            data: {
              _token: "{{ csrf_token() }}",
              image: base64data
            },
            beforeSend: function(data) {
              footer.innerHTML = `
              <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              `;
            },
            success: function(data) {
              if (data.msg == 'success') {
                location.reload();
              } else {
                $modal.modal('hide');
                $('#failedModal').modal('show');
              }
            }
          });
        };
      });
    });
  });
</script>
@endsection