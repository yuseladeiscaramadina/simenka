<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1 style="font-weight: normal;">Halo {{$name}}!</h1>
    <br>
    Selamat! anda berhasil lolos tahap uji keterampilan, selanjutnya anda akan diundang untuk mengikuti Wawancara terakhir melalui platform Zoom dengan HRD dari PT. MKA!
    <br>
    Berikut detail informasi untuk undangan wawancara anda.
    <br>
    <br>
    Waktu : {{$eventDate}}
    <br>
    Link Zoom : <a href="{{$link}}">{{$link}}</a>
    <br>
    Room ID : {{$roomId}}
    <br>
    Password : {{$roomPassword}}
    <br>

    <br>
    <br>
    Sekian informasi yang dapat kami sampaikan, silahkan dipersiapkan untuk sesi wawancaranya dengan baik.

    <br>
    <br>
    Best Regards,
    <br>
    HRD PT.MKA
</body>

</html>