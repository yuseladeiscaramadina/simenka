<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1 style="font-weight: normal;">Halo {{$name}}!</h1>
    <br>
    Mohon maaf, anda belum dinyatakan lolos seleksi ini. Silahkan coba dikemudian hari.
    <br>
    <br>
    Best Regards,
    <br>
    HRD PT.MKA
</body>

</html>