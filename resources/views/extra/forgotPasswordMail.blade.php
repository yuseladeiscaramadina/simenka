<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1 style="font-weight: normal;">Halo {{$name}}!</h1>
    <br>
    <b>
        Anda telah meminta melakukan ubah password pada akun anda.
        <br>
        Jika itu memang anda silahkan klik Tombol Reset Password dibawah.
    </b>
    <br>
    <br>
    <div>
        <a style="background-color: #fe3f40; border: none; color: white; padding: 15px 32px; text-decoration: none; display: inline-block; font-size: 16px;" href="{{URL::to('/reset-password/'.$code);}}">Reset Password</a>

        <br>
        <br>
        <b>Atau</b>
        <br>
        <br>
        <a href="{{URL::to('/reset-password/'.$code);}}" style="color: blue;">{{URL::to('/reset-password/'.$code);}}</a>
    </div>
</body>

</html>