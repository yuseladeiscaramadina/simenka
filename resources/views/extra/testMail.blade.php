<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1 style="font-weight: normal;">Halo {{$name}}!</h1>
    <br>
    Selamat! anda berhasil lolos tahap Psikotest, selanjutnya anda akan diundang untuk mengikuti Tes Keterampilan yang akan dilaksanakan pada Website Simenka!
    <br>
    Silahkan kerjakan Tes Keterampilan anda sebelum <b>{{$eventDate}}</b>, jika anda tidak mengerjakan pada batas waktu yang telah ditentukan, maka secara otomatis anda akan gugur pada seleksi lowongan pekerjaan bersangkutan. Mohon diperhatikan juga dengan jaringan anda, jika ditengah test anda keluar, maka otomatis test anda akan dianggap gagal.
    <br>
    <br>

    Sekian informasi yang dapat kami sampaikan, silahkan dipersiapkan untuk sesi tes keterampilan dengan baik.

    <br>
    <br>
    Best Regards,
    <br>
    HRD PT.MKA
</body>

</html>