@extends('templates.homepage')
@section('content')


<div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                        <div class="left-content header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                            <h2>Bangun Karirmu di PT. <span>Media Kreasi Abadi</span></h2>
                            <p>Website ini dibangun untuk mempermudah teman - teman dalam mendaftar Lowongan yang ada di MKA.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="{{asset('beranda/assets/images/banner-right-image.png')}}" alt="team meeting">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="" class="about-us section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="left-image wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <img src="{{asset('beranda/assets/images/about-left-image.png')}}" alt="person graphic">
                </div>
            </div>
            <div class="col-lg-8 align-self-center">
                <div class="services">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-10">
                            <div class="right-text">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                    <h1 class="introduce-mka">Kuy Kenalan sama MKA!</h1>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                    <div class="icon">
                                        <img src="{{asset('beranda/assets/images/service-icon-01.png')}}" alt="reporting">
                                    </div>
                                    <div class="right-text">
                                        <h4>Data Analysis</h4>
                                        <p>Lorem ipsum dolor sit amet, ctetur aoi adipiscing eliter</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                                    <div class="icon">
                                        <img src="{{asset('beranda/assets/images/service-icon-02.png')}}" alt="">
                                    </div>
                                    <div class="right-text">
                                        <h4>Data Reporting</h4>
                                        <p>Lorem ipsum dolor sit amet, ctetur aoi adipiscing eliter</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                                    <div class="icon">
                                        <img src="{{asset('beranda/assets/images/service-icon-03.png')}}" alt="">
                                    </div>
                                    <div class="right-text">
                                        <h4>Web Analytics</h4>
                                        <p>Lorem ipsum dolor sit amet, ctetur aoi adipiscing eliter</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                                    <div class="icon">
                                        <img src="{{asset('beranda/assets/images/service-icon-04.png')}}" alt="">
                                    </div>
                                    <div class="right-text">
                                        <h4>SEO Suggestions</h4>
                                        <p>Lorem ipsum dolor sit amet, ctetur aoi adipiscing eliter</p>
                                    </div>
                                </div>
                            </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tentang-kami" class="our-services section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center  wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="left-image">
                    <img src="{{asset('beranda/assets/images/logo.png')}}" alt="">
                </div>
            </div>
            <div class="col-lg-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="section-heading">
                    <h2>Cerita <em>MKA</em></h2>
                    <p>PT. Media Kreasi Abadi adalah perusahaan yang menyediakan berbagai layanan jasa yang melayani perusahaan mulai dari perusahaan menengah hingga perusahaan besar bergitu pula perusahaan swasta maupun pemerintahan.
                        Kami menyediakan solusi bisnis yang inovatif dengan mengutamakan mutu serta kepercayaan demi kelangsungan bisnis yang harmonis dan berkelanjutan
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="portfolio" class="our-portfolio section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading  wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <h2>Yuk kenalan dengan Jasa di <span>MKA</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 mb-5">
                <a href="#">
                    <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="hidden-content">
                            <h4>Konsultan IT</h4>
                            <p>Jasa Konsultan IT (Information & Technology) dan Telekomunikasi Bersertifikat Internasional</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/konsultan-it.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6 mb-5">
                <a href="#">
                    <div class="item wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.4s">
                        <div class="hidden-content">
                            <h4>Cleaning Service</h4>
                            <p>Jasa Pelayanan Kebersihan Rumah, Kantor, Gedung dll yang bersertifikasi APKLINDO KALTIM</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/cleaning-service.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6 mb-5">
                <a href="#">
                    <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <div class="hidden-content">
                            <h4>Courier Service</h4>
                            <p>Jasa Ekspedisi Pengiriman Barang Berupa Dokumen dan Paket Besar Untuk Seluruh Wilayah Indonesia</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/courier-service.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6 mb-5">
                <a href="#">
                    <div class="item wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="hidden-content">
                            <h4>Desain Interior & Renovasi</h4>
                            <p>Jasa Desain Interior dan Furniture Custom & Kontraktor Renovasi Bangunan</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/desain-interior.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6 mb-5">
                <a href="#">
                    <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="hidden-content">
                            <h4>Training Center</h4>
                            <p>Jasa Konsultan Training Untuk Peningkatan Mutu SDM Berskala Internasional</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/training-center.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="#">
                    <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="hidden-content">
                            <h4>General Supplier Pengadaan Barang</h4>
                            <p>Jasa Pengadaan Barang Berupa Alat Tulis Kantor dll</p>
                        </div>
                        <div class="showed-content">
                            <img src="{{asset('beranda/assets/images/general-supplier.png')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@if($newestJob != null)
<div id="lowongan-pekerjaan" class="our-blog section" style="margin-bottom: 60px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="section-heading">
                    <h2>Minat kerja di <em>MKA?</em> <br>yuk coba daftar <span>Lowongan</span></h2>
                </div>
            </div>
            <div class="col-lg-6 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="top-dec">
                    <img src="{{asset('beranda/assets/images/blog-dec.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="left-image">
                    <a href="/lihat-lowongan/{{$newestJob->id}}"><img src="{{asset($newestJob->image)}}" alt="Workspace Desktop"></a>
                    <div class="info">
                        <div class="inner-content">
                            <ul>
                                <li><i class="fa fa-calendar"></i> {{date("d M Y", strtotime($newestJob->created_at))}} </li>
                                <li><i class="fa fa-users"></i> MKA</li>
                            </ul>
                            <a href="/lihat-lowongan/{{$newestJob->id}}">
                                <h4>{{$newestJob->title}}</h4>
                            </a>
                            <p>{!!substr($newestJob->description, 0, 100) . "..."!!}</p>
                            <div class="main-blue-button">
                                <a href="/lihat-lowongan">Lihat Semua Lowongan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="right-list">
                    <ul>
                        @foreach($jobs as $job)
                        <li>
                            <div class="left-content align-self-center">
                                <span><i class="fa fa-calendar"></i> {{date("d M Y", strtotime($job->created_at))}}</span>
                                <a href="/lihat-lowongan/{{$job->id}}">
                                    <h4>{{$job->title}}</h4>
                                </a>
                                <p>{!! (count_chars($job->description) > 100) ? substr($job->description, 0, 100) . "..." : $job->description !!}</p>
                            </div>
                            <div class="right-image">
                                <a href="/lihat-lowongan/{{$job->id}}"><img src="{{asset($job->image)}}" alt=""></a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div id="lowongan-pekerjaan" class="our-blog section" style="margin-bottom: 60px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="section-heading">
                    <h2>Minat kerja di <em>MKA?</em> <br>untuk sekarang belum ada <span>Lowongan</span></h2>
                </div>
            </div>
            <div class="col-lg-6 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s">
                <div class="top-dec">
                    <img src="{{asset('beranda/assets/images/blog-dec.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection