@extends('templates.homepage')
@section('content')
<div class="container" style="height: 80px;"></div>
<div class="container mt-5">
  <h2 class="mb-2 font-weight-light wow fadeInDown">Semua Lowongan Pekerjaan</h2>
  @foreach($jobs as $job)
  <div class="row mb-3 wow fadeInUp">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-4 col-md-12">
            <img src="{{asset($job->image)}}" alt="">
          </div>
          <div class="col-lg-8 col-md-12">
            <span style="font-size: 19px; color: #afafaf; font-weight: 300;"><i class="fa fa-calendar" style="color: #ff4d61; font-size: 20px;margin-right: 8px;"></i> {{date("d M Y", strtotime($job->created_at))}}</span>

            <a href="/lihat-lowongan/{{$job->id}}">
              <h4 class="mt-3" style="font-size: 24px;font-weight: 700;color: #2a2a2a;margin: 20px 0px 15px 0px;">{{$job->title}}</h4>
            </a>
            <p class="mt-1">
            <p>{!! (strlen($job->description) > 300) ? substr($job->description, 0, 300) . "..." : $job->description; !!}</p>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

@endsection