@extends('templates.homepage')
@section('content')
<div class="container" style="height: 80px;"></div>
<div class="container mt-5">
  <div class="card mb-3">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-4 col-md-12">
          <div class="wow fadeInUp">
            <img src="{{asset($job->image)}}" alt="">
          </div>
        </div>
        <div class="col-lg-8 col-md-12">
          <div class="wow fadeInUp">
            <span style="font-size: 19px; color: #afafaf; font-weight: 300;"><i class="fa fa-calendar" style="color: #ff4d61; font-size: 20px;margin-right: 8px;"></i> {{date("d M Y", strtotime($job->created_at))}}</span>

            <h4 class="mt-3" style="font-size: 24px;font-weight: 700;color: #2a2a2a;margin: 20px 0px 15px 0px;">{{$job->title}}</h4>

            <p class="font-weight-bold mb-3">PT. MKA</p>
          </div>

          <div class="wow fadeInDown" style="float: right;">
            @if($isApplied)
            <button type="button" class="btn btn-outline-dark">Sudah Mendaftar!</button>
            @else
            <a href="/daftar-lowongan/{{$job->id}}" class="btn btn-primary btn-lg">Daftar</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <div class="wow fadeInUp">
            <p style="font-weight: 700;" class="mb-1">Deskripsi Pekerjaan</p>
            <p>{!! $job->description; !!}</p>
          </div>

          <div class="wow fadeInUp">
            <p style="font-weight: 700;" class="mb-1 mt-2">Kebutuhan Pekerjaan</p>
            <p>{!! $job->requirement; !!}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Duh~</h5>
        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="{{asset('beranda/assets/illustration/illust-warning.jpg')}}" style="width: 70%;">
        <p>Anda belum Login, Silahkan melakukan Login terlebih dahulu...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ga Dulu...</button>
        <a href="/masuk" class="btn btn-danger">Gas</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="completeProfileModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Duh~</h5>
        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="{{asset('beranda/assets/illustration/illust-complete-profile.jpg')}}" style="width: 70%;">
        <p>Terdapat beberapa data anda yang belum lengkap. Silahkan lengkapi data anda terlebih dahulu sebelum melamar</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ga Dulu...</button>
        <a href="/profilku" class="btn btn-danger">Oh Iyaa</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="failedModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Duh~</h5>
        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="{{asset('beranda/assets/illustration/illust-failed-apply.jpg')}}" style="width: 70%;">
        @if(Session::get('job_active') != null)
        <p>Lamaran anda pada lowongan <b>{{Session::get('job_active')->title}}</b> sedang dalam proses. Silahkan menyelesaikan lowongan tersebut sebelum daftar pada lowongan yang baru</p>
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Siap!</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Mantap!</h5>
        <button type="button" class="close btn btn-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="{{asset('beranda/assets/illustration/illust-success-apply.jpg')}}" style="width: 70%;">
        <p>Lamaran anda berhasil dimasukkan! Silahkan periksa website atau email anda secara berkala untuk info lebih lanjut!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Siap!</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')

@if(Session::get('msg') == 'noauth')
<script>
  $('#loginModal').modal('show');
</script>
@endif

@if(Session::get('msg') == 'nodata')
<script>
  $('#completeProfileModal').modal('show');
</script>
@endif

@if(Session::get('msg') == 'hasactive')
<script>
  $('#failedModal').modal('show');
</script>
@endif

@if(Session::get('msg') == 'success')
<script>
  $('#successModal').modal('show');
</script>
@endif

@endsection