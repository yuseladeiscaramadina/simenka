@extends('templates.auth')

@section('content')
<div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('dashboard/assets/images/auth/register.jpg') }}); background-size: cover;">
    <div class="row w-100">
        <div class="col-lg-4 mx-auto">
            <h2 class="text-center mb-4">Daftar</h2>
            <div class="auto-form-wrapper">
                <div id="message"></div>
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <form action="/daftar" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <input name="name" type="text" class="form-control" placeholder="Nama" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="email" type="email" class="form-control" placeholder="Email" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="password" id="password" type="password" class="form-control" placeholder="Password" onkeyup="checkPassword();" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="confirm-password" type="password" class="form-control" placeholder="Ulangi Password" onkeyup="checkPassword();" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button id="btn-submit" type="submit" class="btn btn-primary submit-btn btn-block">Daftar</button>
                    </div>
                    <div class="text-block text-center my-3">
                        <span class="text-small font-weight-semibold">Sudah punya akun ?</span>
                        <a href="{{ url('/masuk') }}" class="text-black text-small">Masuk</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>
    function checkPassword() {
        let password = document.getElementById('password').value;
        let confirmPassword = document.getElementById('confirm-password').value;
        let btnSubmit = document.getElementById('btn-submit');
        let message = document.getElementById('message');

        if (password != confirmPassword || password.length <= 8) {
            message.innerHTML = `<div class="alert alert-danger" role="alert">
                    Pastikan Password sama dan jumlah karakternya lebih dari 8 sebelum mendaftar 
                </div>`;
            btnSubmit.disabled = true;
        } else {
            message.innerHTML = ``;
            btnSubmit.disabled = false;
        }
    }
</script>
@endsection