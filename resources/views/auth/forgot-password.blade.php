@extends('templates.auth')
@section('content')

<div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('dashboard/assets/images/auth/register.jpg') }}); background-size: cover;">
    <div class="row w-100">
        <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <form action="/lupa-password" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="label">Email</label>
                        <div class="input-group">
                            <input name="email" type="text" class="form-control"  placeholder="email" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary submit-btn btn-block">Ubah Password saya</button>
                    </div>
                    <div class="text-block text-center my-3">
                        <span class="text-small font-weight-semibold">Sudah ingat lagi ?</span>
                        <a href="{{ url('/masuk') }}" class="text-black text-small">Yuk login lagi</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection