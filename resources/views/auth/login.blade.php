@extends('templates.auth')
@section('content')

<div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('dashboard/assets/images/auth/register.jpg') }}); background-size: cover;">
    <div class="row w-100">
        <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <form action="/masuk" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="label">Email</label>
                        <div class="input-group">
                            <input name="email" type="text" class="form-control"  placeholder="email" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="label">Password</label>
                        <div class="input-group">
                            <input name="password" type="password" class="form-control" placeholder="*********" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary submit-btn btn-block">Masuk</button>
                    </div>
                    <div class="form-group d-flex justify-content-between">
                        <div class="form-check form-check-flat mt-0">
                        </div>
                        <a href="/lupa-password" class="text-small forgot-password text-black">Lupa Password</a>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-block g-login" onclick="location.href='/auth/redirect';">
                            <img class="mr-3" src="{{ url('dashboard/assets/images/file-icons/icon-google.svg') }}" alt="">Log in with Google</button>
                    </div>
                    <div class="text-block text-center my-3">
                        <span class="text-small font-weight-semibold">Belum punya akun ?</span>
                        <a href="{{ url('/daftar') }}" class="text-black text-small">Buat Akun Baru</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection