@extends('templates.auth')

@section('content')
<div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('dashboard/assets/images/auth/register.jpg') }}); background-size: cover;">
    <div class="row w-100">
        <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
                <div id="message"></div>
                @if(Session::get('msg'))
                <div class="alert alert-{!!session::get('type')!!}" role="alert">
                    {!!session('msg')!!}
                </div>
                @endif
                <form action="/pelamar/ganti-password" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <input name="old_password" type="password" class="form-control" placeholder="Password Lama" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="new_password" id="password" type="password" class="form-control" placeholder="Password Baru" onkeyup="checkPassword();" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="confirm-password" type="password" class="form-control" placeholder="Ulangi Password" onkeyup="checkPassword();" required>
                            <div class="input-group-append">
                                <span class="input-group-text" style="background-color: white;">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button id="btn-submit" type="submit" class="btn btn-primary submit-btn btn-block">Ubah Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>
    function checkPassword() {
        let password = document.getElementById('password').value;
        let confirmPassword = document.getElementById('confirm-password').value;
        let btnSubmit = document.getElementById('btn-submit');
        let message = document.getElementById('message');

        if (password != confirmPassword || password.length <= 8) {
            message.innerHTML = `<div class="alert alert-danger" role="alert">
                    Pastikan Password sama dan jumlah karakternya lebih dari 8 sebelum menekan tombol reset password
                </div>`;
            btnSubmit.disabled = true;
        } else {
            message.innerHTML = ``;
            btnSubmit.disabled = false;
        }
    }
</script>
@endsection